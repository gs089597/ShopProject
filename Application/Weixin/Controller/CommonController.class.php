<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Weixin\Controller;

use Think\Controller;

class CommonController extends Controller {
	public function _initialize() {
		$this->config = $this->configArr ();
		$weObj = new \NewsLib\Wechat ( $this->config ['siteConfig'] ['wechat'] );
		$this->WeiObj = $weObj;
	}
	// 初始化
	protected function configArr() {
		$site = M ( 'System' )->select ();
		foreach ( $site as $k => $v ) {
			$siteConfig [$v ['name']] = json_decode ( $v ['data'], true );
		}
		$array ['siteConfig'] = $siteConfig;
		return $array;
	}
}