<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Weixin\Controller;
use Think\Controller;
class IndexController extends CommonController {
    public function index(){
		$this->WeiObj->valid();
		$type = $this->WeiObj->getRev()->getRevType();
		$this->Wuser = $this->WeiObj->_receive['FromUserName'];
		$key = $this->WeiObj->getRevContent();
		switch($type) {
			case \NewsLib\Wechat::MSGTYPE_TEXT: //文本信息
				$this->aReply($key);
				break;
			case \NewsLib\Wechat::MSGTYPE_EVENT: //事件
				$event = $this->WeiObj->getRev()->getRevEvent();
				break;
			case \NewsLib\Wechat::MSGTYPE_IMAGE: //图片
				break;
			default: //默认
				$this->defaultReply($key);
		}
		switch($event['event']) {
			case \NewsLib\Wechat::EVENT_MENU_VIEW:
				M('User')->where(array('wx_openid'=>$this->Wuser,'is_subscribe'=>0))->save(array('is_subscribe'=>1));
				break;
			case \NewsLib\Wechat::EVENT_MENU_CLICK:
				$this->aReply($event['key']);
				break;
			case \NewsLib\Wechat::EVENT_SUBSCRIBE: //关注
				if(strstr($event['key'],"usert") || strstr($event['key'],"qrscene_usert")){
					$this->aReply($event['key']);
				}				
				$this->aReply('关注');
				break;
			case \NewsLib\Wechat::EVENT_UNSUBSCRIBE: //取消关注
				M('User')->where(array('wx_openid'=>$this->Wuser,'is_subscribe'=>1))->save(array('is_subscribe'=>0));
				$this->WeiObj->text("谢谢关注我们")->reply();
				break;
			case \NewsLib\Wechat::EVENT_SCAN: //扫描带参数二维码
				$this->aReply($event['key']);
				break;
			default:
				$this->defaultReply($event['key']);
		}
	}
	public function defaultReply($key){
		$this->aReply("默认");
		/*
		$M = D('Goods');
		$map = $map_1 = array(
			'is_putaway' => 1,
			'is_recycle' => 0,
		);
		$map_1['title'] = array('like','%'.$key.'%');
		$arr = $M->where($map_1)->limit(10)->order("id DESC")->relation(true)->select();
		if($arr == ''){
			$arr = $M->where($map)->limit(10)->order("salesnum DESC")->relation(true)->select();
		}
		$content = array();
		foreach($arr as $key=>$val){
			$content[$key]['Title'] = $val['title'];
			$content[$key]['Description'] = $val['GoodsA']['description'];
			$content[$key]['PicUrl'] = "http://" . $_SERVER['HTTP_HOST'] . $val['img'];
			$content[$key]['Url'] = "http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Goods/index',array('id'=>$val['id']));
		}
		$this->WeiObj->news($content)->reply();
		exit();
		*/
	}
	public function aReply($key){	
		$user = M('User')->where(array('wx_openid'=>$this->Wuser))->find();
		$user_1 = M('User')->where(array('wx_openid'=>$this->Wuser))->count();
		if($user_1 < 1){	
			//$this->WeiObj->text("回复: ".$this->Wuser)->reply();exit;
			if(strstr($key,"usert")){
				if(strstr($key,"qrscene_usert")){
					$userid = str_replace("qrscene_usert",'',$key);
					$this->weiRegiste($userid);
				}else{
					$userid = str_replace("usert",'',$key);
					$this->weiRegiste($userid);
				}
			}else{
				$this->weiRegiste(0);
			}
		}else{
			M('User')->where(array('wx_openid'=>$this->Wuser,'is_subscribe'=>0))->save(array('is_subscribe'=>1));
		}
      	if($user_1 >= 1){	
            if(strstr($key,'客服')){
                $this->WeiObj->transfer_customer_service()->reply();
                exit();
            }	

            if(strstr($key,'海报')){
              	$img = R('Wap/Fenxiao/getWeixinQRCodeWei', array('uid' => $user['id']));
              	if($img){
                  	$filepath = WEB_ROOT . 'Uploads/usertg/'.$user['id'].'/haibao.jpg';
                    if (class_exists ( '\CURLFile' )) {//关键是判断curlfile,官网推荐php5.5或更高的版本使用curlfile来实例文件  
                        $filedata = array (  
                            'media' => new \CURLFile ( realpath ( $filepath ), 'image/jpeg' )   
                        );  
                    } else {  
                        $filedata = array (  
                            'media' => '@' . realpath ( $filepath )   
                        );  
                    }  
                	$json = $this->WeiObj->uploadMedia($filedata, 'image');
                  	if($json['media_id'])
              			$this->WeiObj->image($json['media_id'])->reply();
                }
                exit();
            }
        }
		$M = M('WeixReply');
		$arr = $M->where(array('key'=>array('like','%'.$key.'%')))->order("id DESC")->find();
		$content = json_decode($arr['data'],true);
		if($arr <> ''){
			if($arr['type'] == 'text'){
				foreach($content as $key=>$val){
					$this->WeiObj->text($val['content'])->reply();
					exit();
				}
			}
			if($arr['type'] == 'news'){
				foreach($content as $key=>$val){
					$content[$key]['PicUrl'] = "http://" . $_SERVER['HTTP_HOST'] . $val['PicUrl'];
				}
				$this->WeiObj->news($content)->reply();
				exit();
			}
		}else{
			$this->defaultReply($key);
		}
	}
	public function weiRegiste($tj_u) {
		$user = M( "User" )->order("id DESC")->find ();
		if($user['username'] == ''){
			$userid_a = 10000;
		}else{
			$userid_a = $user['username'];
		}
		$wuser = $this->WeiObj->getUserInfo($this->Wuser);
		$user = M ( "User" )->where ( array ("wx_openid" => $this->Wuser ) )->count ();
		if ($user > 0) {
			exit();
		}
		$userid = $userid_a + 1;
		$password = rand(100000,999999);
		$datas = array(
				'sex' => $wuser['sex'],
				'portrait' => $wuser['headimgurl'],
				'nickname' => $wuser['nickname'],
				'wx_openid' => $this->Wuser,
				'username' => $userid,
				'password' => $password,
				'referrer_u' => $tj_u,
				'is_subscribe' => 1
		);
		$info = $datas['nickname']."，恭喜您成功关注成为第". $userid ."位会员，加入商城，享更多优惠";
		$arr = D ( "Public" )->addUser ($datas);
		$this->WeiObj->text($info)->reply();
		exit();
	}
}