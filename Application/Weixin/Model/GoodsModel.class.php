<?php
namespace Weixin\Model;
use Think\Model\RelationModel;
class GoodsModel extends RelationModel {
	protected $_link = array(        
		'GoodsA'=>array(            
			'mapping_type'      => self::HAS_ONE,            
			'class_name'        => 'GoodsA', 
			'foreign_key'       => 'id'    
	    ), 
		'GoodsAttr'=>array(
			'mapping_type'      => self::HAS_MANY,            
			'class_name'        => 'GoodsAttr', 
			'foreign_key'       => 'goods_id'  
		)   
	 );
}

?>
