<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Weixin\Model;
use Think\Model;
class PublicModel extends Model {
	public function adminMd5($password){
		return md5(substr(md5(substr(md5($password),5,20).C("PSD_VERIFY")),2,28).C("PS_ENTERPRISE"));
	}
	public function payMd5($password){
		return md5(substr(md5(substr(md5($password),6,25).C("PSD_VERIFY")),8,28).C("PS_ENTERPRISE"));
	}
	public $referrer_u;
	public function addUser($datasa = "") {		
       	$datas = $datasa;
        $M = M("User");
        $datas['username'] = trim($datas['username']);
		$datas['portrait'] = $datas['portrait'] <> '' ? $datas['portrait'] : '/Public/Images/big_user_0.jpg';
		$datas['salt'] = randCode(5);
		$password = $datas['password'];
        $datas['password'] = $this->adminMd5($password.$datas['salt']);
		$datas['password_pay'] = $this->payMd5($password.$datas['salt']);
		$datas['regdate'] = time();
		$datas['status'] = 1;

        if($datas['referrer_u'] > 0) {
            $referrer_u = $M->where(array('id' => $datas['referrer_u']))->find();
            /*if ($referrer_u['user_rank'] <= 0) {
                unset($datas['referrer_u']);
            } else {*/
                $this->referrer_u = $referrer_u['id'];
                $M->where(array('id' => $this->referrer_u))->setInc("sub_num_z", 1);
                for ($i = 0; $i < 3; $i++) {
                    if($this->referrer_u > 0){
                        $M->where(array('id' => $this->referrer_u))->setInc("sub_num", 1);
                        $refe = $M->where(array('id' => $this->referrer_u))->find();
                        if($refe['id'] > 0){
                            $this->referrer_u = $refe['referrer_u'];
                        }else{
                            $this->referrer_u = 0;
                        }
                    }
                }
            //}
        }

        if ($id = $M->add($datas)) {			
			//$count = $M->count ();
			if($datas['referrer_u'] > 0){
				$info_r = $M->where(array('id'=>$datas['referrer_u']))->find();
				$msarr = '您的好友 ['.$datasa['nickname']."] 已经成功注册会员，他的消费您有提成哦 \n <a href=\"http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Fenxiao/myfriend').'">查看详情</a>';
				setUserWeixinMessage($info_r['id'],$msarr);
			}			
            return array('status' => 1, 'info' => "注册成功");
        } else {
            return array('status' => 0, 'info' => "注册失败失败，请重试");
        }
    }
}

?>
