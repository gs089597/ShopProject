<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Model;
use Think\Model;
class GoodsModel extends CommonModel {
	protected $_link = array(        
		'GoodsA'=>array(            
			'mapping_type'      => self::HAS_ONE,            
			'class_name'        => 'GoodsA', 
			'foreign_key'       => 'id'    
	    ), 
		'GoodsAttr'=>array(
			'mapping_type'      => self::HAS_MANY,            
			'class_name'        => 'GoodsAttr', 
			'foreign_key'       => 'goods_id'  
		)   
	 );
}

