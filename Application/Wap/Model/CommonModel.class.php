<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Model;
use Think\Model;
use Think\Model\RelationModel;
class CommonModel extends RelationModel {
	public function _initialize() {
		$this->uid = session('W_uid');
	}
	public function adminMd5($password){
		return md5(substr(md5(substr(md5($password),5,20).C("PSD_VERIFY")),2,28).C("PS_ENTERPRISE"));
	}
	public function payMd5($password){
		return md5(substr(md5(substr(md5($password),6,25).C("PSD_VERIFY")),8,28).C("PS_ENTERPRISE"));
	}
}

?>
