<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Model;
use Think\Model;
class PublicModel extends Model {
	public function adminMd5($password){
		return md5(substr(md5(substr(md5($password),5,20).C("PSD_VERIFY")),2,28).C("PS_ENTERPRISE"));
	}
	public function payMd5($password){
		return md5(substr(md5(substr(md5($password),6,25).C("PSD_VERIFY")),8,28).C("PS_ENTERPRISE"));
	}
	private $referrer_u;

	public function getConfig(){
        $site = M('System')->select();
        foreach ($site as $k => $v) {
            $siteConfig[$v['name']] = json_decode($v['data'], true);
        }
        $array['siteConfig'] = $siteConfig;
        $this->config = $array;
	}

	public function addUser($datasa = "", $is_weix = false) {
		$this->getConfig();

        $datas = $_POST;
        if($datasa != ""){
        	$datas = $datasa;
        }

        if(!$is_weix){
            if (!$this->check_verify($datas['verify'], 'regcode')) {
                return array('status' => 0, 'info' => "验证码错误！");
            }

            if(session('phoneVerify') <> md5( $datas['phone_verify'] . $datas['phone'])){
                return array('status' => 0, 'info' => "短信验证码错误");
            }else{
                unset($datas['phone_verify']);
            }
            $datas['is_edit_pwd'] = 1;
		}
        unset($datas['phone_verify']);

        $M = M("User");
        if(isset($datas['referrer_mobile']) && $datas['referrer_mobile'] > 0){
            $ru = $M->where(array('phone'=>$datas['referrer_mobile']))->find();
            $datas['referrer_u'] = $ru['id'];
        }
		unset($datas['referrer_mobile']);

        if($datas['referrer_u'] > 0) {
            $referrer_u = $M->where(array('id' => $datas['referrer_u']))->find();
            /*if ($referrer_u['user_rank'] <= 0) {
                unset($datas['referrer_u']);
            } else {*/
                $this->referrer_u = $referrer_u['id'];
                $M->where(array('id' => $this->referrer_u))->setInc("sub_num_z", 1);
                for ($i = 0; $i < 3; $i++) {
                    if($this->referrer_u > 0){
                        $M->where(array('id' => $this->referrer_u))->setInc("sub_num", 1);
                        $refe = $M->where(array('id' => $this->referrer_u))->find();
                        if($refe['id'] > 0){
                            $this->referrer_u = $refe['referrer_u'];
                        }else{
                            $this->referrer_u = 0;
                        }
                    }
                }
            //}
        }


        $user = D ( "User" )->order("id DESC")->find ();
        
        if($user['username'] == ''){
        	$userid_a = 10000;
        }else{
        	$userid_a = $user['username'];
        }
        $datas['username'] = $userid_a + 1;
		if($M->where(array("wx_openid"=>$datas['wx_openid']))->count() > 0){
			return array('status' => 0, 'info' => "亲！你的微信已经绑定过账号了，将自动登陆","url"=>U("Public/login"));
		};		
		if ($M->where(array('qq_openid'=>$datas['qq_openid']))->count() > 0) {
			return array('status' => 0, 'info' => "这个QQ已经绑定过账户了！","url"=>U("Public/login"));
		}
		if ($M->where(array('sina_openid'=>$datas['sina_openid']))->count() > 0) {
			return array('status' => 0, 'info' => "这个新浪账户已经绑定过账户了！","url"=>U("Public/login"));
		}
		if ($M->where(array('phone'=>$datas['phone']))->count() > 0) {
			return array('status' => 0, 'info' => "这个手机已经绑定过本站账号！","url"=>U("Public/login"));
		}		      
		if ($M->where(array('username'=>$datas['username']))->count() > 0) {
            return array('status' => 0, 'info' => "该用户名已经存在");
        }		
		if ($datas['password'] <> $datas['password_1']) {
            return array('status' => 0, 'info' => "两次密码不一致");
        }
		unset($datas['password_1']);
		unset($datas['verify']);
		unset($datas['__hash__']);
		$datas['portrait'] = $datas['portrait'] <> '' ? $datas['portrait'] : '/Public/Images/big_user_0.jpg';
		$datas['salt'] = randCode(5);
		$password = $datas['password'];
        $datas['password'] = $this->adminMd5($password.$datas['salt']);
		$datas['password_pay'] = $this->payMd5($password.$datas['salt']);
        $datas['regdate'] = time();
		$datas['status'] = 1;
        /*$u_rank = $this->config['siteConfig']['system_basis']['sys_mr_rank'];
		if($u_rank > 0){
            $user_rank = M("UserRank")->where(array('rank'=> $u_rank))->find();
            $datas['user_rank'] = $user_rank['id'];
		}*/
        if ($id = $M->add($datas)) {
        	$info = $M->where(array('id'=>$id))->find();
        	$session = array(
        			'uid' => $info['id'],
        			'username' => $info['username'],
        			'usershell' => md5(md5($info['id'].$info['username']).$info['password'].$info['salt']),
        	);
        	session("W_uid",$session['uid']);
        	session("W_username",$session['username']);
        	session("W_usershell",$session['usershell']);
        	$url = cookie('jump_url');
        	if($url == null){
        		$url = U("Index/index");
        	}
			if($datas['referrer_u']){
				$info_r = $M->where(array('id'=>$datas['referrer_u']))->find();

				$msarr = array(
						array(
								'title' => "好友注册提醒",
								'description' => '您的好友 ['.$datas['nickname']."] 已经成功注册会员，他的消费您有提成哦 \n <a href=\"http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Fenxiao/myfriend').'">查看详情</a>',
								'url' => "http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Home/myfriend'),
								'picurl' => "http://" . $_SERVER['HTTP_HOST'].$this->config['siteConfig']['system_basis']['sys_weix_img2'],								
						)
				);
				setUserWeixinMessage($info_r['id'],$msarr[0]['description']);
			}
        	
        	
            return array('status' => 1, 'info' => "注册成功", 'url' => $url);
        } else {
            return array('status' => 0, 'info' => "注册失败失败，请重试");
        }
    }
	
	public function login(){
		$datas = $_POST;
		if (!$this->check_verify($datas['verify'])) {
			return array('status' => 0, 'info' => "验证码错误！");
        }
		$M = M("User");
		$M->create($datas);
        if ($M->where(array('phone'=>$datas['username']))->count() >= 1) {
            $info = $M->where(array('phone'=>$datas['username']))->find();
            if ($info['status'] == 0) {
                return array('status' => 0, 'info' => "你的账号被禁用，如有疑问联系客服");
            }
            if ($info['password'] == $this->adminMd5($datas['password'].$info['salt'])) {
				$session = array(
					'uid' => $info['id'],
					'username' => $info['username'],
					'usershell' => md5(md5($info['id'].$info['username']).$info['password'].$info['salt']),
				);
				session("W_uid",$session['uid']);
				session("W_username",$session['username']);
				session("W_usershell",$session['usershell']);
				$url = cookie('jump_url');
				if($url == null){
					$url = U("Index/index");
				}
				M ( "User" )->where ( array ("username" => $info['username'] ) )->save (array("last_login"=>time()));
                return array('status' => 1, 'info' => "登录成功", 'url' => $url);
            } else {
                return array('status' => 0, 'info' => "账号或密码错误");
            }
        } else {
            return array('status' => 0, 'info' => "不存在用户名为：</br>" . $datas["username"] . '的账号！');
        }
	}
	//API登陆
	public function loginApiA($str,$openid){
		$M = M("User");
        if ($M->where(array($str=>$openid))->count() >= 1) {
            $info = $M->where(array($str=>$openid))->find();
            if ($info['status'] == 0) {
                return array('status' => 0, 'info' => "你的账号被禁用，如有疑问联系客服");
            }
			$session = array(
				'uid' => $info['id'],
				'username' => $info['username'],
				'usershell' => md5(md5($info['id'].$info['username']).$info['password'].$info['salt']),
			);
			session("W_uid",$session['uid']);
			session("W_username",$session['username']);
			session("W_usershell",$session['usershell']);
			$url = cookie('jump_url');
			if($url == null){
				$url = U("Index/index");
			}
			M ( "User" )->where ( array ("username" => $info['username'] ) )->save (array("last_login"=>time()));
			return array('status' => 1, 'info' => "登录成功", 'url' => $url);
        } else {
            return array('status' => 0, 'info' => "非法操作");
        }	
	} 
	//API绑定且登陆
	public function loginApiB($str){
		$datas = $_POST;
        //if(!$is_weix){
            if (!$this->check_verify($datas['verify'], 'regcode')) {
                return array('status' => 0, 'info' => "验证码错误！");
            }

            if(session('phoneVerify') <> md5( $datas['phone_verify'] . $datas['phone'])){
                return array('status' => 0, 'info' => "短信验证码错误");
            }else{
                unset($datas['phone_verify']);
            }
            $datas['is_edit_pwd'] = 1;
        //}
		$M = M("User");
		$M->create($datas);
        if ($M->where("`phone`='" . $datas['phone'] . "'")->count() >= 1) {
            $info = $M->where("`phone`='" . $datas["phone"] . "'")->find();
            if ($info['status'] == 0) {
                return array('status' => 0, 'info' => "你的账号被禁用，如有疑问联系客服");
            }
            if ($info['password'] == $this->adminMd5($datas['password'].$info['salt'])) {
				$session = array(
					'uid' => $info['id'],
					'username' => $info['username'],
					'usershell' => md5(md5($info['id'].$info['username']).$info['password'].$info['salt']),
				);
				$array = array(
					$str =>$datas[$str],
					'sex' =>$datas['sex'],
					'nickname' =>$datas['nickname'],
				);
				$datas['portrait'] <> '' ? $array['portrait'] = $datas['portrait'] : '';
				$M->where(array('id'=>$info['id']))->save($array);
				session("W_uid",$session['uid']);
				session("W_username",$session['username']);
				session("W_usershell",$session['usershell']);
				$url = cookie('jump_url');
				if($url == null){
					$url = U("Index/index");
				}
				M ( "User" )->where ( array ("username" => $info['username'] ) )->save (array("last_login"=>time()));
                return array('status' => 1, 'info' => "登录成功", 'url' => $url);
            } else {
                return array('status' => 0, 'info' => "账号或密码错误");
            }
        } else {
            return array('status' => 0, 'info' => "不存在用户名为：</br>" . $datas["username"] . '的账号！');
        }	
	} 
	public function check_verify($code, $id = ''){    
		$verify = new \Think\Verify();    
		return $verify->check($code, $id);
	}
}

?>
