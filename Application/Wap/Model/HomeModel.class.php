<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Model;
use Think\Model;
class HomeModel extends CommonModel {
	//修改手机
	public function editPhone(){
		$datas = $_POST;
        $M = M("User");
        $uid = $this->uid;
        $user = D("User")->where(array('id'=>$uid))->find();
        if($user['phone'] <> ""){
        	$y = session('phoneVerify');
        	$ve = md5($datas['verify'].$user['phone']);
        	if($ve <> $y){
        		return array('status' => 0, 'info' => "验证码错误，请重新输入");
        	}
        }
        $count = D("User")->where(array('phone'=>$datas['phone']))->count();
        if($count > 0){
        	return array('status' => 0, 'info' => "这个手机号已经被使用");
        }
		$M->create($datas);
		if($_GET['type'] == "drawings"){
			$url = U("Home/drawings");
		}elseif($_GET['type'] == "payment"){
			$url = U("/Wap/Payment/index",array("order"=>$_GET['order'],"payId"=>$_GET['payId']));
		}elseif($_GET['type'] == "editPhone"){
			$url = U("Home/editPhone");
		}elseif($_GET['type'] == "editPayPwd"){
			$url = U("Home/editPayPwd");
		}else{
			$url = U("Home/index");
		}
        if ($M->where(array('id'=>$uid))->save()) {
            return array('status' => 1, 'info' => "修改成功", 'url' => $url);
        } else {
            return array('status' => 0, 'info' => "修改失败，请重试");
        }	
	}
	
	//修改信息
	public function editMyInfo(){
		$datas = $_POST;
        $M = M("User");
		$M->create($datas);
        if ($M->where(array('id'=>$this->uid))->save()) {
            return array('status' => 1, 'info' => "修改成功", 'url' => U("Home/index"));
        } else {
            return array('status' => 0, 'info' => "修改失败，请重试");
        }	
	}
	//修改支付密码
	public function editPayPwd(){
		$datas = $_POST;
        $M = M("User");
         $uid = $this->uid;
        $user = D("User")->where(array('id'=>$uid))->find();
		$y = session('phoneVerify');
		$ve = md5($datas['verify'].$user['phone']);
		if($ve <> $y){
			return array('status' => 0, 'info' => "验证码错误，请重新输入");
		}
		if ($datas['password'] <> $datas['password_1']) {
            return array('status' => 0, 'info' => "两次密码不一致");
        }
		$info = $M->where(array('id'=>$this->uid))->find();
		
        $datas['password_pay'] = $this->payMd5($datas['password'].$info['salt']);
		unset($datas['password']);
		$M->create($datas);
		if($_GET['type'] == "payment"){
			$url = U("/Wap/Payment/index",array("order"=>$_GET['order'],"payId"=>$_GET['payId']));
		}else{
			$url = U("Home/index");
		}
        if ($M->where(array('id'=>$this->uid))->save()) {
            return array('status' => 1, 'info' => "支付密码修改成功", 'url' => $url);
        } else {
            return array('status' => 0, 'info' => "修改失败，请重试");
        }	
	}
	//修改登陆密码
	public function editPwd(){
		$datas = $_POST;
        $M = M("User");
		$info = $M->where(array('id'=>$this->uid))->find();
		$y = session('phoneVerify');
		$ve = md5($datas['verify'].$datas['phone']);
		if($ve <> $y){
            return array('status' => 0, 'info' => "短信验证码不正确");
		}
		if ($datas['password'] <> $datas['password_1']) {
            return array('status' => 0, 'info' => "两次密码不一致");
        }
        if ($M->where(array('id'=> array('neq', $this->uid), 'phone'=>$datas['phone']))->count() > 0) {
            return array('status' => 0, 'info' => "这个手机已经绑定过其他账号！");
        }
        /*if($info['password'] <> $this->adminMd5($datas['password_old'].$info['salt'])){
            return array('status' => 0, 'info' => "旧密码输入有误");
        }*/
        $datas['password'] = $this->adminMd5($datas['password'].$info['salt']);
		$datas['is_edit_pwd'] = 1;
		$M->create($datas);
        if ($M->where(array('id'=>$this->uid))->save()) {
            return array('status' => 1, 'info' => "登陆密码修改成功", 'url' => U("Home/index"));
        } else {
            return array('status' => 0, 'info' => "修改失败，请重试");
        }	
	}
	//添加收货地址
	public function addUserAddress() {
        $M = M("UserAddress");
		$_POST['user_id'] = $this->uid;
		$M->create($_POST);
        return $M->add() ? array('status' => 1, 'info' => '添加成功', 'url' => U('Home/address')) : array('status' => 0, 'info' => '添加失败');
    }
	public function editUserAddress() {
        $M = M("UserAddress");
		$M->create($_POST);
        return $M->where(array('user_id'=>$this->uid,'id'=>$_POST['id']))->save() ? array('status' => 1, 'info' => '编辑成功', 'url' => U('Home/address')) : array('status' => 0, 'info' => '编辑失败');
    }
	public function drawings_a(){
		$datas['bank'] = json_encode($_POST['data']);
        $M = M("User");
		//$M->create($datas);
        if ($M->where(array('id'=>$this->uid))->save($datas)) {
            return array('status' => 1, 'info' => "操作成功");
        } else {
            return array('status' => 0, 'info' => "操作失败，请重试");
        }		
	}
}

?>
