<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class ArticleController extends CommonController {
     public function index(){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
		C('TOKEN_ON',false);
		$M = M('NewsCategory');
		$Category = $this->getCategory($id = (int)$_GET['id']);
		$this->assign('CategoryList',$Category);
		$this->assign('navt',"新闻帮助中心");
		$this->display();
    }
	public function nlist(){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
		C('TOKEN_ON',false);
		$M = M('News');
		$map = array(
			'status' => 1,
            'type' => 0
		);
		if($_GET['category']){
            $map['category'] = $_GET['category'];
        }
		//获取分页
		$pageconfig['count'] = $M->where($map)->count();
		$pageconfig['pagesize'] = 15;
		$Page = new  \NewsLib\Page($pageconfig['count'], $pageconfig['pagesize']);
		
		$list = $M->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
		if(IS_AJAX){
			$this->ajaxReturn($list);
		}else{
			$this->assign('pageconfig',$pageconfig);
			$this->assign('list',$list);
			$this->assign('navt',"文章列表");
			$this->display();
		}
	}
	public function content(){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
		$M = M('News');
		$map = array(
			'status' => 1,
			'id' => $_GET['id']
		);
		$info = $M->where($map)->find();
		$info['content'] = M('NewsA')->where(array('id'=>$info['id']))->find();
		if($info['link_url']){
			header('Location: ' . $info['link_url']);
		}
		$this->assign('navt',$info['title']);
		$this->assign('info',$info);
		$this->display();
	}
	protected function getCategory($id = 0){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
		$M = M('NewsCategory');
		$array = $M->where(array('pid'=>$id, 'type' => 0))->order('sort ASC')->select();
		if($array){
			foreach($array as $key => $val){
				$val['list'] = $aaaa = $this->getCategory($val['id']);
				$arr[] = $val; 
			}
			return $arr; 
		}
	}
}