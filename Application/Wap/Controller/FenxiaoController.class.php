<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;

use Think\Controller;

class FenxiaoController extends CommonController
{
    public function index()
    {
        $this->authentication();
        $uid = $this->uid;
        $user = M("User")->where(array('id' => $uid))->find();
        /*if ($user['user_rank'] <= 0) {
            $this->redirect('Fenxiao/nofenxiao');
        }*/
        $is_sc = 0;
        if($user['weixin_qrcode_type'] == 1 && $user['weixin_qrcode_endtime'] < time()){
            $is_sc = 1;
        }
        if($user['weixin_qrcode'] == ''){
            $is_sc = 1;
        }
        $this->assign("is_sc", $is_sc);
        $this->assign("user", $user);
        $this->assign('navt', "分享推广");
        $this->display();
    }

    public function share()
    {
        $uid = (int)$_GET['tuid'];
        if ($uid) {
            $user = M("User")->where(array('id' => $uid))->find();
            $this->assign("user", $user);
        }
        $this->assign('navt', $user['nickname'] ? $user['nickname'] : $user['username'] . "推荐二维码");
        $this->display();
    }

    public function nofenxiao()
    {
        if ($this->is_weixin()) {
            $this->authentication();
        };
        $this->assign('navt', "购买会员");
        $this->display();
    }

    /*
     * 推广提成提现到余额
     *  */
    /*public function referrerToMoney()
    {
        $this->authentication();
        $uid = $this->uid;
        $userinfo = M("User")->where(array('id' => $uid))->find();
        if (IS_AJAX) {
            $data = $_POST;

            $y = session('TxVerify');
            $ve = md5($data['verify'] . $userinfo['phone']);
            //if($ve <> $y)$this->error('验证码错误，请重新输入');
            $password_pay = D("Home")->payMd5($data['password'] . $userinfo['salt']);
            //if($password_pay <> $userinfo['password_pay'])$this->error("支付密码不正确");
            if ($data['money'] < 1) $this->error('最低提现金额 1元');
            if ($data['money'] > $userinfo['referrer_money']) $this->error('你输入的金额大于你的可用余额！请重新输入');

            if ($userinfo['referrer_money'] > 10000) {
                $gyj = $data['money'] * 1 / 100;
                $money = $data['money'] - $gyj;
                $tm = "，用户已经累计提成超过1万，扣除1%公益基金";
                $arr = array(
                    'money' => $gyj,
                    'uid' => $uid,
                    'time' => time(),
                );
                M("SsLog")->add($arr);
            } else {
                $money = $data['money'];
            }
            $scoreA['user_money'] = $userinfo['user_money'] + $money;
            $scoreA['referrer_money'] = $userinfo['referrer_money'] - $data['money'];
            M("User")->where(array('id' => $userinfo['id']))->save($scoreA);
            addUserAccount($userinfo['username'], $money, 1, '分销提成提现到余额' . $tm, '站内充值', time());

            $array = array(
                'user_id' => $userinfo['username'],
                'money' => $money,
                'user_note' => $data['user_note'],
                'add_time' => time(),
                'type' => 2,
                'payment' => $data['payment'],
                'data' => json_encode($data['data']),
            );

            if ($orderid = M('UserAccount')->add($array)) {
                D('User')->where(array('id' => $uid))->setInc('frozen_money', $data['money']);//冻结
                $this->success("申请提交成功，请等待管理员审核", U("Fenxiao/referrerToMoney", array('type' => 2)), 3);
            } else {
                $this->error('订单生成失败', U("Fenxiao/referrerToMoney"), 3);
            };
        } else {
            $this->assign("user", $userinfo);
            $this->assign('navt', "推广提成提现");
            $this->display();
        }
    }*/

    public function getQRCode()
    {
        $uid = $this->uid;
        $root = str_replace('\\', '/', realpath(dirname(__FILE__) . '/../../../'));
        $path = "/Uploads/usertg/{$uid}/";
        $dir = makeDir($root.$path);
        $outUrl = "http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Index/popularize', array('type' => 'u', 'user' => $uid));
        $img = new \NewsLib\myQRCode($path, 30);
        $url = $img->getUrl("qrcode",$outUrl);
        D('User')->where(array('id' => $uid))->save(array('weixin_qrcode' => $img, 'weixin_qrcode_type'=> 2));

        $this->getImg();
        $this->success("成功生成", U("/Wap/Fenxiao/index"));
    }

    public function getWeixinQRCode()
    {
        $this->authentication();
        $uid = $this->uid;
        $weixconfig = $this->config['siteConfig']['wechat'];
        $weObj = new  \NewsLib\Wechat($weixconfig);
        $key = 'usert' . $uid;
        $expire_seconds = 2592000;
        $arr = $weObj->getQRCode($key, 1, $expire_seconds);
        $img = $weObj->getQRUrl($arr['ticket']);
        D('User')->where(array('id' => $uid))->save(array('weixin_qrcode' => $img, 'weixin_qrcode_type'=> 1, 'weixin_qrcode_endtime' => time() + 2592000 - 300));
        $this->getImg();
        $this->success("成功生成", U("/Wap/Fenxiao/index"));
    }
	
  	public function getWeixinQRCodeWei($uid = 0)
    {
      
      	if($uid <= 0 || !$uid) return false;
      	$us = D('User')->where(array('id' => $uid, 'weixin_qrcode_endtime' => array('gt', time())))->find();
      	if(!empty($us) && $us['weixin_qrcode_endtime'] > time() && $us['tg_img'] && file_exists(WEB_ROOT . "Uploads/usertg/{$uid}/haibao.jpg")){
        	return $us['tg_img'];
        }
        $weixconfig = $this->config['siteConfig']['wechat'];
        $weObj = new  \NewsLib\Wechat($weixconfig);
        $key = 'usert' . $uid;
        $expire_seconds = 2592000;
        $arr = $weObj->getQRCode($key, 1, $expire_seconds);
        $img = $weObj->getQRUrl($arr['ticket']);
        D('User')->where(array('id' => $uid))->save(array('weixin_qrcode' => $img, 'weixin_qrcode_type'=> 1, 'weixin_qrcode_endtime' => time() + 2592000 - 300));
        $haibimg = $this->getImg($uid);
      	return $img;
    }
    public function getImg($uid = 0)
    {	
      	if($uid <= 0)
        	$uid = $this->uid;
        $user = D('User')->field("id,username,nickname,portrait,weixin_qrcode,tg_img,phone")->where(array('id' => $uid))->find();
        $image = new \Think\Image();
        $Http = new \Org\Net\Http();
		
        $path = WEB_ROOT . "Uploads/usertg/{$user['id']}";
        $dir = makeDir($path);
        $imgfile_qrcode = $path . "/qrcode.png";
        $imageinfo_qrcode = $Http->curlDownload($user['weixin_qrcode'], $imgfile_qrcode);
        $image->open($imgfile_qrcode)->thumb(172, 172)->save($path . "/qrcode_thumb.png");
		
        if(strpos($user['portrait'], "http") !==false ){
            $user_portrait = $user['portrait'];
        }else{
            $user_portrait = 'https://'.$_SERVER['SERVER_NAME'].$user['portrait'];
        }
        $imgfile_portrait = $path . "/portrait.jpg";
        $imageinfo_portrait = $Http->curlDownload($user_portrait, $imgfile_portrait);
        $image->open($imgfile_portrait)->thumb(170, 170, 6)->save($path . "/portrait_thumb.jpg");
		
        $bg_img = WEB_ROOT . "Public/Images/code.png";
        $imgfile_qrcode_thumb = $path . "/qrcode_thumb.png";
        $imgfile_portrait_thumb = $path . "/portrait_thumb.jpg";
        $imgfile_portrait_thumb_y = $path . "/portrait_thumb_y.png";

        $imgg = $this->yuan_img($imgfile_portrait_thumb);
        imagepng($imgg, $imgfile_portrait_thumb_y);
        imagedestroy($imgg);

        //$image->open($imgfile_portrait)->thumb(120, 120)->save($path . "/portrait_thumb_y.jpg");
        $image->open($bg_img)->water($imgfile_qrcode_thumb, array(400, 360), 100)->save($path . "/haibao.jpg");
        $bg_img = $path . "/haibao.jpg";
        $image->open($bg_img)->water($imgfile_portrait_thumb_y, array(140, 360), 100)->save($path . "/haibao.jpg");
//        $image->open($bg_img)->text($user['nickname'], WEB_ROOT . "Public/Fonts/AdobeHeitiStd-Regular.otf", 23, '#fbe362', array(180, 110))->save($path . "/haibao.jpg");
//        $image->open($bg_img)->text($user['username'], WEB_ROOT . "Public/Fonts/AdobeHeitiStd-Regular.otf", 23, '#fbe362', array(180, 110))->save($path . "/haibao.jpg");
        D('User')->where(array('id' => $uid))->save(array('tg_img' => "/Uploads/usertg/{$user['id']}/haibao.jpg"));
        return "/Uploads/usertg/{$user['id']}/haibao.jpg";
    }

    function yuan_img($imgpath = '') {
        $ext     = pathinfo($imgpath);
        $src_img = null;
        switch ($ext['extension']) {
            case 'jpg':
                $src_img = imagecreatefromjpeg($imgpath);
                break;
            case 'png':
                $src_img = imagecreatefrompng($imgpath);
                break;
        }
        $wh  = getimagesize($imgpath);
        $w   = $wh[0];
        $h   = $wh[1];
        $w   = min($w, $h);
        $h   = $w;
        $img = imagecreatetruecolor($w, $h);
        //这一句一定要有
        imagesavealpha($img, true);
        //拾取一个完全透明的颜色,最后一个参数127为全透明
        $bg = imagecolorallocatealpha($img, 255, 255, 255, 127);
        imagefill($img, 0, 0, $bg);
        $r   = $w / 2; //圆半径
        $y_x = $r; //圆心X坐标
        $y_y = $r; //圆心Y坐标
        for ($x = 0; $x < $w; $x++) {
            for ($y = 0; $y < $h; $y++) {
                $rgbColor = imagecolorat($src_img, $x, $y);
                if (((($x - $r) * ($x - $r) + ($y - $r) * ($y - $r)) < ($r * $r))) {
                    imagesetpixel($img, $x, $y, $rgbColor);
                }
            }
        }
        return $img;
    }

    public function myfriend()
    {
        $this->authentication();
        $uid = $this->uid;
        $U = D("User");
        $usera = $U->where(array('id' => $this->config['User']['referrer_u']))->field("id,username,phone,nickname,name,portrait")->find();
        $scaleArr = $this->config['siteConfig']['popularize']['scale'];
        $i = 0;
        foreach ($scaleArr as $k => $v) {
            if ($i == 0) {
                $this->ids = array($uid);
            }
            $ids = array();
            foreach ($this->usersss as $key => $val) {
                $ids[] = $val['id'];
            }
            if ($i > 0) {
                $this->ids = $ids;
            }
            if ($_GET['key']) {
                $map_1['username'] = array('like', '%' . $_GET['key'] . '%');
                $map_1['nickname'] = array('like', '%' . $_GET['key'] . '%');
                $map_1['phone'] = array('like', '%' . $_GET['key'] . '%');
                $map_1['_logic'] = 'or';
                $where['_complex'] = $map_1;
            }
            $this->usersss = $user[$k] = $this->GetUserR($this->ids, $where);
            $i++;
        }
        foreach ($user as $key => $val) {
            foreach ($val as $k => $v) {
                $userAll[] = $v;
            }
        }
        $this->assign("userR", $scaleArr);
        $this->assign('navt', "我的团队");
        $this->assign("userAllCount", count($userAll));
        $this->assign("userAll", $userAll);
        $this->assign("userS", $user);
        $this->assign("usera", $usera);
        $this->display();
    }

    public function GetUserR($uids, $where)
    {
        $U = D("User");
        $where['referrer_u'] = array('in', $uids);
        $user = $U->where($where)->field("id,username,phone,nickname,name,portrait,user_rank")->select();
        $uid = $this->uid;
        foreach ($user as $key => $val) {
            $user[$key]['Rank'] = M('UserRank')->where(array('id' => $val['user_rank']))->find();
            $user[$key]['money'] = M('ReferrerLog')->where(array('uid' => $uid, 'auid' => $val['id']))->sum('money');
            $user[$key]['order_num'] = M('Orders')->where(array('user_id' => $val['id']))->count();
        }
        return $user;
    }

    public function myProfit()
    {
        $this->authentication();
        $uid = $this->uid;
        $M = M("ReferrerLog");
        $map = array(
            'uid' => $uid,
        );
        $_GET['fid'] <> '' ? $map['auid'] = (int)$_GET['fid'] : '';
        $status = $_GET['status'];
        if ($status != 0) {
            $map['status'] = $status;
        }
        $count = $M->where($map)->count();
        $pagesize = 10;
        $Page = new  \NewsLib\wapPage($count, $pagesize);
        $show = $Page->show();
        $tabs = $M->where($map)->limit($Page->firstRow . ',' . $Page->listRows)->order('time DESC')->select();
        foreach ($tabs as $key => $val) {
            $tabs[$key]['Order'] = M('Orders')->where(array('id' => $val['oid']))->find();
            $tabs[$key]['gUser'] = M('User')->where(array('id' => $val['auid']))->find();
        }
        $this->assign('page', $show);
        $this->assign('navt', "个人中心");
        $this->assign("list", $tabs);
        $this->display();
    }
}