<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class AdController extends CommonController {
    public function index(){
		$id = $_GET['wid'];
		$AD = M('Ad');
		$adposition = $AD->where(array('id'=>$id))->find();
		$map  = array(
			'adpositionid' => array('eq',$id),
			'startdate' => array('elt',time()),
			'enddate' => array('egt',time()),
			'status' => 1
		);
		$AdContent = M('AdContent');
		$adposition['content'] = $AdContent->where($map)->order("sort ASC")->limit($num)->select();
		$adStatistics = M('AdStatistics');	
		$date = strtotime(date("Y-m-d",time()));
		foreach($adposition['content'] as $k => $v){
			$sta = $adStatistics->where(array('time'=>$date,'ad_content_id'=>$v['id']))->find();
			if($sta <> ''){
				$adStatistics->where(array('time'=>$date,'ad_content_id'=>$v['id']))->setInc('shownum');
			}else{
				$data = array(
					'shownum' => 1,
					'ad_content_id' => $v['id'],
					'time' => $date
				);
				$adStatistics->add($data);	
			}
		}
		$this->getStr($adposition);
    }
	public function content(){
		$id = $_GET['id'];
		$AdContent = M('AdContent');
		$adStatistics = M('AdStatistics');
		$date = strtotime(date("Y-m-d",time()));
		$adStatistics->where(array('time'=>$date,'ad_content_id'=>$id))->setInc('click');
		$content = $AdContent->where(array('id'=>$id))->find();
		header("Location:{$content['url']}"); 
		exit;
    }
	
	public function getStr($arr){
		$str = 'var str=\'';
		//矩形横幅
		if($arr['type'] == 1){
			foreach($arr['content'] as $k => $v){
				$str .= '<a href="'.U('Ad/content',array('id'=>$v['id'])).'" target="_blank">';
				$str .= '<img style="width:'.$arr['width'].'px; height:'.$arr['height'].'px;" src="'.__ROOT__.$v['img_content'] .'" alt="'.$v['img_p'].'"/></a>';
			}
		}
		//图片列表
		if($arr['type'] == 2){
			$str .= "<ul>";
			foreach($arr['content'] as $k => $v){
				$str .= '<li><a href="'.U('Ad/content',array('id'=>$v['id'])).'" target="_blank">';
				$str .= '<img style="width:'.$arr['width'].'px; height:'.$arr['height'].'px;" src="'.__ROOT__.$v['img_content'] .'" alt="'.$v['img_p'].'"/></a></li>';
			}
			$str .= "</ul>";
		}
		//文字广告
		if($arr['type'] == 4){
			$str .= '<ul style="width:'.$arr['width'].'px; height:'.$arr['height'].'px;">';
			foreach($arr['content'] as $k => $v){
				$str .= '<li><a href="'.U('Ad/content',array('id'=>$v['id'])).'" target="_blank">'.$v['text_content'].'</a></li>';
			}
			$str .= "</ul>";
		}
		//滚动图片
		if($arr['type'] == 3){
			echo '$(document).ready(function(){
					$("#myController_'.$arr['id'].'").jFlow({
						controller: ".control_'.$arr['id'].'", 
						slideWrapper : "#jFlowSlider_'.$arr['id'].'", 
						slides: "#slider_'.$arr['id'].'", 
						selectedWrapper: "jFlowSelected", 
						width: "'.$arr['width'].'px",  
						height: "'.$arr['height'].'px", 
						duration: 400, 
						prev: ".slidprev_'.$arr['id'].'", 
						next: ".slidnext_'.$arr['id'].'",
						auto: true
				});
			  });';
			$str .= '<div class="slider" style="width:'.$arr['width'].'px; height:'.$arr['height'].'px;"><div class="slidprev slidprev_'.$arr['id'].'"><span>Prev</span></div><div class="slidnext slidnext_'.$arr['id'].'"><span>Next</span></div><div id="slider_'.$arr['id'].'">';
			foreach($arr['content'] as $k => $v){
				$str .= '<div> <a href="'.U('Ad/content',array('id'=>$v['id'])).'" target="_blank"><img style="width:'.$arr['width'].'px; height:'.$arr['height'].'px;" src="'.__ROOT__.$v['img_content'] .'" alt="'.$v['img_p'].'"/></a></div>';
			}
			$str .= '</div><div class="myController" id="myController_'.$arr['id'].'">';
						foreach($arr['content'] as $k => $v){
							$str .= '<div class="control control_'.$arr['id'].'"><span>'. $k  .'</span></div>';
						}
		$str .= '</div></div>';
		}
		
		$str .= '\';document.write(str);';
		echo $str;
	}
}