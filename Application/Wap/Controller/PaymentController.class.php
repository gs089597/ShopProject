<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class PaymentController extends CommonController {
	public function payMd5($password){
		return md5(substr(md5(substr(md5($password),6,25).C("PSD_VERIFY")),8,28).C("PS_ENTERPRISE"));
	}
	
	public function index(){
		header("Content-Type:text/html; charset=utf-8");
		$this->authentication();
		$uid = $this->uid;
		$order = $_GET['order'];	
		$payId = $_GET['payId'];
		$order = D('Orders')->where(array('id'=>$order,'user_id'=>$uid))->find();
		$user = M("User")->where(array('id'=>$uid))->find();
        $payGoods = D('OrderGoods')->where(array('order_id'=>$order['id']))->select();
		foreach ($payGoods as $key => $val){
            $Goods = M("Goods")->where(array('id'=>$val['goodsId']))->find();
            $number = $this->getgoodsnums($Goods, $val['number']);
            if($number <= 0){
                M('Orders')->where(array('id'=>$order['id'],'user_id'=>$uid))->save(array('order_status' => 2));
                $this->error('当前订单中，有限购商品，数量已经超过，当前订单不可以支付，系统已经为您取消',U('Home/index'),3);
            }
        }
        if($order['integral_size'] > $user['score']){
            $this->error('您的积分不够支付',U('Home/index'),3);
        }
		if($order['pay_status'] == 1){
			$this->error('订单已经支付过了！亲',U('Home/index'),3);
		}
      	if($order['ordersize'] == 0){
            $this->ordersave($order['ordernum'],$order['ordernum'],time());
            $this->error('支付成功',U("Order/all_order",array('all_order'=>1)),3);
            exit;
        }
		$payment = D('Payment')->where(array('id'=>$order['pay_type']))->find();
		$config = json_decode($payment['config'],true);
		$this->assign('order',$order);
		$this->$payment['name']($config,$order);
	}
	
	//支付宝支付	
	public function alipayConfig(){
		return array(
			'private_key_path' => getcwd()."/ThinkPHP/Library/NewsLib/wapAlipay/rsa_private_key.pem",
			'ali_public_key_path' => getcwd()."/ThinkPHP/Library/NewsLib/wapAlipay/alipay_public_key.pem",
			'sign_type' => 'MD5',
			'input_charset' => 'utf-8',
			'cacert' => getcwd()."/ThinkPHP/Library/NewsLib/wapAlipay/cacert.pem",
			'transport' => 'http',
		);
	}
	public function alipayNotify(){
		header("Content-Type:text/html; charset=utf-8");
		$payment = D('Payment')->where(array('name'=>'alipay'))->find();
		$config = json_decode($payment['config'],true);
		$alipay_config = $this->alipayConfig();
		$alipay_config['partner']		= $config['partner'];
		$alipay_config['key']			= $config['key'];
		$alipayNotify = new \NewsLib\wapAlipay\AlipayNotify($alipay_config);
		$verify_result = $alipayNotify->verifyNotify();
		
		if($verify_result) {//验证成功
			$time = $_POST['gmt_payment'];				
			$out_trade_no = $_POST['out_trade_no'];
			$trade_no = $_POST['trade_no'];
			$trade_status = $_POST['trade_status'];
			$aaa = $this->ordersave($out_trade_no,$trade_no,$time);
			if(!$aaa){
				echo "fail";
			}else{
				echo "success";
			}								
		}else {
			echo "fail";
		}
	}
	public function alipayReturn(){
		header("Content-Type:text/html; charset=utf-8");	
		$payment = D('Payment')->where(array('name'=>'alipay'))->find();
		$config = json_decode($payment['config'],true);
		$alipay_config = $this->alipayConfig();
		$alipay_config['partner']		= $config['partner'];
		$alipay_config['key']			= $config['key'];
		$alipayNotify = new \NewsLib\wapAlipay\AlipayNotify($alipay_config);
		$verify_result = $alipayNotify->verifyReturn();
		if($verify_result) {
			$this->success('支付成功',U('Home/index'),3);
		}else{
			$this->error('支付失败',U('Home/index'),3);	
		}
	}
	protected function alipay($config,$order){
		C('TOKEN_ON',false);
		$alipay_config = $this->alipayConfig();
		$alipay_config['partner']		= $config['partner'];
		$alipay_config['key']			= $config['key'];
		$notify_url = "http://".$_SERVER['HTTP_HOST'].U('Payment/alipayNotify');//服务器异步通知页面路径
		$return_url = "http://".$_SERVER['HTTP_HOST'].U('Payment/alipayReturn');//页面跳转同步通知页面路径
		$seller_email = $config['email'];//卖家支付宝帐户
		$out_trade_no = $order['ordernum'];//商户订单号
		$subject = "订单支付".$order['ordernum'];//订单名称
		$total_fee = $order['ordersize'];//付款金额
		$body = $order['ordernum'];
		$payment_type = "1";
		$parameter = array(
				"service" => "alipay.wap.create.direct.pay.by.user",
				"partner" => trim($alipay_config['partner']),
				"seller_id" => trim($alipay_config['partner']),
				"payment_type"	=> $payment_type,
				"notify_url"	=> $notify_url,
				"return_url"	=> $return_url,
				"out_trade_no"	=> $out_trade_no,
				"subject"	=> $subject,
				"total_fee"	=> $total_fee,
				"_input_charset"	=> trim(strtolower($alipay_config['input_charset']))
		);
		//建立请求
		$alipaySubmit = new \NewsLib\wapAlipay\AlipaySubmit($alipay_config);
		$html_text = $alipaySubmit->buildRequestForm($parameter, 'get', '确认');
	
		$this->assign('html_text',$html_text);
		$this->display('alipay');
	}
	//微信支付
	public function wechatConfig($config){
		return array(
			'APPID' => $config['appid'],
			'MCHID' => $config['mch_id'],
			'KEY' => $config['key'],//商户支付密钥Key。审核通过后，在微信发送的邮件中查看（新版本中需要自己填写）
			'APPSECRET' => $config['appsecret'],//JSAPI接口中获取openid，审核后在公众平台开启开发模式后可查看
			//'JS_API_CALL_URL' => "http://".$_SERVER['HTTP_HOST'].U('Payment/index',array('order'=>$_GET['order'],'payId'=>$_GET['payId'])),//获取access_token过程中的跳转uri，通过跳转将code传入jsapi支付页面
			'JS_API_CALL_URL' => "http://".$_SERVER['HTTP_HOST'].U('Payment/index')."?order={$_GET['order']}&payId={$_GET['payId']}",
			'NOTIFY_URL' => "http://".$_SERVER['HTTP_HOST'].U('Payment/wxPayNotify'),//异步通知url，商户根据实际开发过程设定
			'CURL_TIMEOUT' => 30,//本例程通过curl使用HTTP POST方法，此处可修改其超时时间，默认为30秒
		);
	}
	public function wechat($config,$order) {
		$wechatConfig = $this->wechatConfig($config);
        $jsApi = new \NewsLib\Wechat\Wxjspay($wechatConfig);
	    $unifiedOrder = new \NewsLib\Wechat\UnifiedOrder($wechatConfig);
		
        if (!isset($_GET['code'])){
            $url = $jsApi->createOauthUrlForCode($wechatConfig['JS_API_CALL_URL']);
            header("Location: $url");
        }else{
            $code = $_GET['code'];
            $jsApi->setCode($code);
            $openid = $jsApi->getOpenId();
        }
       /*-----------------------------必填--------------------------*/ 
        $unifiedOrder->setParameter("body","订单支付".$order['ordernum']);//商品描述微支付平台
        $unifiedOrder->setParameter("out_trade_no",$order['ordernum']);//商户订单号
        $unifiedOrder->setParameter("total_fee",$order['ordersize'] * 100);//总金额（微信支付以人民币“分”为单位）
        $unifiedOrder->setParameter("openid",$openid);//获取到的OPENID
        $unifiedOrder->setParameter("notify_url",$wechatConfig['NOTIFY_URL']);//通知地址
        $unifiedOrder->setParameter("trade_type","JSAPI");//交易类型
		//print_r($order);
        $prepay_id = $unifiedOrder->getPrepayId(); 
        $jsApi->setPrepayId($prepay_id);
        $this->jsApiParameters = $jsApiParameters = $jsApi->getParameters();
		$this->assign('jsApiParameters',$jsApiParameters);
        $this->display('wechat');
    }
    /**
     * 微信支付异步通知
    */
    public function wxPayNotify(){
		header("Content-Type:text/html; charset=utf-8");
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
        $array = $this->xmlToArray($postStr);//获取回调参数，orderno
		$order = D('Orders')->where(array('ordernum'=>$array['out_trade_no'],'pay_status'=>0))->find();
		$payment = D('Payment')->where(array('id'=>$order['pay_type']))->find();
		$config = json_decode($payment['config'],true);
		$wechatConfig = $this->wechatConfig($config);
		$notify = new \NewsLib\Wechat\Notify($wechatConfig);
		$xml = $GLOBALS['HTTP_RAW_POST_DATA'];	
		$notify->saveData($xml);
		if($notify->checkSign() == FALSE){
			$notify->setReturnParameter("return_code","FAIL");//返回状态码
			$notify->setReturnParameter("return_msg","签名失败");//返回信息
		}else{
			$aaa = $this->ordersave($array['out_trade_no'],$array['transaction_id'],$array['time_end']);
			if($aaa){
				$notify->setReturnParameter("return_code","SUCCESS");//设置返回码
			}
		}
		$returnXml = $notify->returnXml();
		echo $returnXml;
    }
    public function xmlToArray($xml){       
        $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);      
        return $array_data;
    }
	
	
	public function bPayConfig(){
		return array(
			'KEY' => 'jIGklrg8765VTYljierbiFDjnxcuD7Y78',
		);
	}
	//余额支付
	public function balancepay($config,$order){
		C('TOKEN_ON',false);
		$config = $this->bPayConfig();
		$config['URL'] = U('Payment/balancepay_p');
		$parameter = array(
			'ordernum' => $order['ordernum'],
			'ordersize' => $order['ordersize']
		);
		$bPay = new \NewsLib\bPay($config);
		$html_text = $bPay->buildRequestForm($parameter, 'post', '马上支付');
		$this->assign('html_text',$html_text);
		$this->display('balancepay');
	}
	
	public function balancepay_p(){
		$M = M('User');
		$config = $this->bPayConfig();
		$bPay = new \NewsLib\bPay($config);	
		//验证提交
		$datas = $array = $_POST;
		$order = D('Orders')->where(array('ordernum'=>$datas['ordernum'],'user_id'=>$this->uid))->find();
		if($order['pay_status'] == 1){
			$this->error('订单已经支付过了！亲',U('Home/index'),3);
		}
		$info =  $M->where(array('id'=>$this->uid))->find();
// 		if($this->payMd5($datas['password_pay'].$info['salt']) <> $info['password_pay']){
// 			$this->error('支付密码错误，请重新输入');
// 		}
		unset($array['password_pay']);
		unset($array['sign']);
		$sign = $bPay->getSign($array);
		if($datas['sign'] <> $sign){
			$this->error('订单验证失败！！');
		}
		/*if($info['score'] < $datas['ordersize']){
			$this->error('可用积分不足，请充值');
		}*/

        if($info['user_money'] < $datas['ordersize']){
            $this->error('可用余额不足，请充值');
        }
        if($info['user_money'] - $info['frozen_money'] < $datas['ordersize']){
            $this->error('可用余额不足，请充值');
        }
        $user_money = $info['user_money'] - $datas['ordersize'];
        if($M->where(array('id'=>$this->uid))->save(array('user_money'=>$user_money))){
            $this->ordersave($datas['ordernum'],$datas['ordernum'],date('Y-m-d H:i:s',time()));
            $this->success('支付成功',U('Home/index'),3);
        }
	}

	public function offline(){
        $this->success('订单已经提交',U('Order/all_order'),3);
    }



    public function withdraw($id, $paytype = "wechat"){
        $order = M("UserAccount")->where(array('id'=>$id))->find();
        if($order['is_paid'] == 1){
            return array('status' => 0, 'info' => "这个提现操作已经审核过了");
        }
        $payment = D('Payment')->where(array('name'=>$paytype))->find();
        $config = json_decode($payment['config'],true);
        $user = D("User")->where(array('username'=>$order['user_id']))->find();
        if($user['wx_openid'] == ''){
            return array('status' => 0, 'info' => "当前用户 没有绑定微信，无法完成付款");
        }
        $array = array (
            'partner_trade_no' => $order['id'],
            'openid' => $user['wx_openid'],
            'check_name' => 'NO_CHECK',
            'amount' => $order ['money'] * 100,
            'desc' => "会员提现",
            're_user_name' => $user['nickname']
        );
        return $this->withdrawWechat($config,$array);
    }
    //微信支付
    public function withdrawWechatConfig($config){
        return array(
            'APPID' => $config['appid'],
            'MCHID' => $config['mch_id'],
            'KEY' => $config['key'],//商户支付密钥Key。审核通过后，在微信发送的邮件中查看（新版本中需要自己填写）
            'APPSECRET' => $config['appsecret'],//JSAPI接口中获取openid，审核后在公众平台开启开发模式后可查看
            'CURL_TIMEOUT' => 30,//本例程通过curl使用HTTP POST方法，此处可修改其超时时间，默认为30秒
            //=======【证书路径设置】=====================================
            'SSLCERT_PATH' => __ROOT__.'ThinkPHP/Library/NewsLib/Wechat/cacert/apiclient_cert.pem',
            'SSLKEY_PATH' => __ROOT__.'ThinkPHP/Library/NewsLib/Wechat/cacert/apiclient_key.pem',
            'ROORCA' => __ROOT__.'ThinkPHP/Library/NewsLib/Wechat/cacert/rootca.pem',
        );
    }
    public function withdrawWechat($config,$data) {
        $wechatConfig = $this->withdrawWechatConfig($config);
        $unifiedOrder = new \NewsLib\Wechat\Qyfk($wechatConfig);
        $return = $unifiedOrder->QfykPay($data);
        if($return['return_code'] == "SUCCESS"){
            if($return['result_code'] == "SUCCESS"){
                $this->upOrder($return);
                return array('status' => 1, 'info' => "提现成功");
            }
        }
        return array('status' => 0, 'info' => "提现失败，请联系管理员处理");;
    }
    public function upOrder($arr){
        $account = M("UserAccount")->where(array('id'=>$arr['partner_trade_no']))->find();
        $ss = M('User')->where(array('username'=>$account['user_id']))->setDec('user_money',$account['money']);
        M('User')->where(array('username'=>$account['user_id']))->setDec('frozen_money',$account['money']);
        $admin_note = $account['admin_note']." 微信支付信息：【微信订单号：{$arr['payment_no']}，微信支付时间：{$arr['payment_time']}】";
        $array = array(
            'admin_user' => 'system',
            'admin_note'=>$admin_note,
            'is_paid' => 1
        );
        M("UserAccount")->where(array('id'=>$arr['partner_trade_no']))->save($array);
        $user = D("User")->where(array('username'=>$account['user_id']))->find();
        $str = '尊敬的'.$user['nickname'].'，您的提现'.$account['money'].'元操作，已经支付到您的微信余额，请注意查收';
        if($user['phone'] <> "" && $account['type'] == 2){
            //send_sms($user['phone'],$str); //发送提现短信通知
        }
        setUserWeixinMessage($user['id'],$str);
        setUserMessage($user['id'],$str);
    }
}