<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;

use Think\Controller;
use NewsLib\ThinkSDK\ThinkOauth;

class PublicController extends CommonController
{
    public function login()
    {
        if ($this->is_weixin()) {
            echo '<script>location.href="' . U("Public/weix") . '"</script>';
        }
        if (IS_AJAX) {
            $arr = D("Public")->login();
            if ($this->is_weixin()) {
                redirect($arr['url']);
            } else {
                $this->ajaxReturn($arr);
            }
        } else {
            $this->assign('navt', "用户登陆");
            $this->display();
        }
    }

    public function weix() {
        if ($_GET ['state'] == 'wxbase') {
            echo '<script>location.href="' . U ( "Public/weix" ) . '"</script>';
        }
        $weixUserInfo = session ( "W_wx_userinfo" );
        if (empty ( $weixUserInfo )) {
            $options = $this->config ['siteConfig']['wechat'];
            $this->wxAuth ( $options );
        }
        $user = M ( "User" )->where ( array ("wx_openid" => $weixUserInfo ['open_id'] ) )->count ();
        if ($user > 0) {
            $arr = D ( "Public" )->loginApiA ( 'wx_openid', $weixUserInfo ['open_id'] );
            $arr ['status'] == 1 ? redirect ( $arr ['url'] ) : $this->error ( $arr ['info'] );
            exit ();
        } else {
            $tj_u = cookie ( 'popularize_u' );
            $tj_u = empty($tj_u) ? 0 : $tj_u;
            $password = rand(100000,999999);

            $user = D ( "User" )->order("id DESC")->find ();
            if($user['username'] == ''){
                $userid_a = 10000;
            }else{
                $userid_a = $user['username'];
            }

            $datas = array(
                    'wx_openid' => $weixUserInfo['open_id'],
                    'username' => $userid_a,
                    'sex' => $weixUserInfo['sex'],
                    'portrait' => $weixUserInfo['avatar'],
                    'nickname' => $weixUserInfo['nickname'],
                    'password' => $password,
                    'referrer_u' => $tj_u,
                    'password_1' => $password
            );
            $arr = D ( "Public" )->addUser ($datas, true);

            header("Content-Type:text/html; charset=utf-8");
            M ( "User" )->where ( array ("username" => $userid_a ) )->save (array("last_login"=>time()));
            if($arr['status'] == 1){
                redirect($arr['url']);
            }
        }
    }

    /*public function weix()
    {
        if ($_GET['state'] == 'wxbase') {
            echo '<script>location.href="' . U("Public/weix") . '"</script>';
        }
        $weixUserInfo = session("W_wx_userinfo");
        if (empty($weixUserInfo)) {
            $options = $this->config['siteConfig']['wechat'];
            $this->wxAuth($options);
        }
        $user = M("User")->where(array("wx_openid" => $weixUserInfo['open_id']))->count();
        if ($user > 0) {
            $arr = D("Public")->loginApiA('wx_openid', $weixUserInfo['open_id']);
            $arr['status'] == 1 ? redirect($arr['url']) : $this->error($arr['info']);
            exit();
        } else {
            $this->assign('weixUserInfo', $weixUserInfo);
        }
        if (IS_AJAX) {
            $arr = D("Public")->loginApiB('wx_openid');
            $this->ajaxReturn($arr);
        } else {
            $this->display();
        }
    }*/

    // 新浪微博登陆
    public function loginSina()
    {
        $sdk = ThinkOauth::getInstance('sina');
        redirect($sdk->getRequestCodeURL());
    }

    public function sina()
    {
        if (!IS_AJAX) {
            $code = $_GET ['code'];
            $type = 'sina';
            $sns = ThinkOauth::getInstance($type);
            $extend = null;
            if ($type == 'tencent') {
                $extend = array(
                    'openid' => $this->_get('openid'),
                    'openkey' => $this->_get('openkey')
                );
            }
            $token = $sns->getAccessToken($code, $extend);
            $openid = $token ['openid'];
            if (is_array($token)) {
                $data = $sns->call('user/get_user_info');
                if ($data ['ret'] == 0) {
                    $session = array(
                        'nickname' => $data ['nickname']
                    );
                    $session ['open_id'] = $openid;
                    session("W_sina_userinfo", $session);
                } else {
                    E('获取用户信息失败 : ' . $data ['msg']);
                }
            } else {
                E('获取用户信息失败');
            }
        }
        $userinfo = session("W_sina_userinfo");
        $user = M("User")->where(array(
            "sina_openid" => $userinfo ['open_id']
        ))->count();
        if ($user > 0) {
            $arr = D("Public")->loginApiA('sina_openid', $userinfo ['open_id']);
            $arr ['status'] == 1 ? redirect($arr ['url']) : $this->error($arr ['info']);
            exit ();
        } else {
            $this->assign('sinaUserInfo', $userinfo);
        }
        if (IS_AJAX) {
            $arr = D("Public")->loginApiB('sina_openid');
            $this->ajaxReturn($arr);
        } else {
            $this->assign('navt', "用户登陆");
            $this->display();
        }
    }

    // qq登陆
    public function loginQQ()
    {
        $sdk = ThinkOauth::getInstance('qq');
        redirect($sdk->getRequestCodeURL());
    }

    public function qq()
    {
        if (!IS_AJAX) {
            $code = $_GET ['code'];
            $type = 'QQ';
            $sns = ThinkOauth::getInstance($type);
            $extend = null;
            if ($type == 'tencent') {
                $extend = array(
                    'openid' => $this->_get('openid'),
                    'openkey' => $this->_get('openkey')
                );
            }
            $token = $sns->getAccessToken($code, $extend);
            $openid = $token ['openid'];
            if (is_array($token)) {
                $data = $sns->call('user/get_user_info');
                if ($data ['ret'] == 0) {
                    $session = array(
                        'nickname' => $data ['nickname']
                    );
                    $session ['open_id'] = $openid;
                    session("W_qq_userinfo", $session);
                } else {
                    E('获取腾讯QQ用户信息失败 : ' . $data ['msg']);
                }
            } else {
                E('获取腾讯QQ用户信息失败');
            }
        }
        $userinfo = session("W_qq_userinfo");
        $user = M("User")->where(array(
            "qq_openid" => $userinfo ['open_id']
        ))->count();
        if ($user > 0) {
            $arr = D("Public")->loginApiA('qq_openid', $userinfo ['open_id']);
            $arr ['status'] == 1 ? redirect($arr ['url']) : $this->error($arr ['info']);
            exit ();
        } else {
            $this->assign('qqUserInfo', $userinfo);
        }
        if (IS_AJAX) {
            $arr = D("Public")->loginApiB('qq_openid');
            $this->ajaxReturn($arr);
        } else {
            $this->assign('navt', "用户登陆");
            $this->display();
        }
    }

    // qq登陆
    public function loginBaidu()
    {
        $sdk = ThinkOauth::getInstance('baidu');
        redirect($sdk->getRequestCodeURL());
    }

    public function baidu()
    {
        if (!IS_AJAX) {
            $code = $_GET ['code'];
            $type = 'baidu';
            $sns = ThinkOauth::getInstance($type);
            $extend = null;
            if ($type == 'tencent') {
                $extend = array(
                    'openid' => $this->_get('openid'),
                    'openkey' => $this->_get('openkey')
                );
            }
            $token = $sns->getAccessToken($code, $extend);
            $openid = $token ['openid'];
            if (is_array($token)) {
                $data = $sns->call('user/get_user_info');
                if ($data ['ret'] == 0) {
                    $session = array(
                        'nickname' => $data ['nickname']
                    );
                    $session ['open_id'] = $openid;
                    session("W_qq_userinfo", $session);
                } else {
                    E('获取用户信息失败 : ' . $data ['msg']);
                }
            } else {
                E('获取用户信息失败');
            }
        }
        $userinfo = session("W_qq_userinfo");
        $user = M("User")->where(array(
            "qq_openid" => $userinfo ['open_id']
        ))->count();
        if ($user > 0) {
            $arr = D("Public")->loginApiA('qq_openid', $userinfo ['open_id']);
            $arr ['status'] == 1 ? redirect($arr ['url']) : $this->error($arr ['info']);
            exit ();
        } else {
            $this->assign('qqUserInfo', $userinfo);
        }
        if (IS_AJAX) {
            $arr = D("Public")->loginApiB('qq_openid');
            $this->ajaxReturn($arr);
        } else {
            $this->assign('navt', "用户登陆");
            $this->display();
        }
    }

    public function register()
    {
        if ($this->is_weixin()) {
            if ($_GET ['state'] == 'wxbase') {
                echo '<script>location.href="' . U("Public/register") . '"</script>';
            }
            $weixUserInfo = session("W_wx_userinfo");
            if (empty ($weixUserInfo)) {
                $options = $this->config ['siteConfig'] ['wechat'];
                $this->wxAuth($options);
            }
            $this->assign('weixUserInfo', $weixUserInfo);
        }
        $tj_u = cookie('popularize_u');
        $tj_s = cookie('popularize_s');
        if ($tj_u) {
            $referrer_u = D("User")->where(array('id' => $tj_u))->find();
            $this->assign('referrer_u', $referrer_u);
        } elseif ($tj_s) {
            $referrer_s = D("Supplier")->where(array('id' => $tj_s))->find();
            $this->assign('referrer_s', $referrer_s);
        }
        $qqUserInfo = session("W_qq_userinfo");
        $this->assign('qqUserInfo', $qqUserInfo);

        $sinaUserInfo = session("W_sina_userinfo");
        $this->assign('sinaUserInfo', $sinaUserInfo);

        if (IS_AJAX) {
            $arr = D("Public")->addUser();
            $this->ajaxReturn($arr);
        } else {
            $this->assign('navt', "用户注册");
            $this->display();
        }
    }

    public function verify_code()
    {
        $code = I("get.code_id");
        $array = array(
            'imageW' => 150,
            'imageH' => 50,
            'length' => 4,
            'fontSize' => 18
        );
        $Verify = new \Think\Verify ($array);
        $Verify->codeSet = '0123456789';
        $Verify->entry($code);
    }

    public function logout()
    {
        session('W_uid', null);
        session('W_username', null);
        session('W_usershell', null);
        cookie(null);
        session(null);
        $this->success('成功退出！！', U('Index/index'), 3);
    }

    // 微信网页授权
    public function wxAuth($options)
    {
        $scope = 'snsapi_userinfo';
        $code = isset ($_GET ['code']) ? $_GET ['code'] : '';
        $we_obj = new \NewsLib\Wechat ($options);
        $W_wx_openid = session("W_wx_openid");
        if ($W_wx_openid) {
            return false;
        }
        if ($code) {
            $json = $we_obj->getOauthAccessToken();
            if (!$json) {
                die ('获取用户授权失败，请重新确认');
            }
            $userinfo = $we_obj->getOauthUserinfo($json ['access_token'], $json ["openid"]);
            $weixuser = array(
                'open_id' => $json ["openid"],
                'nickname' => $userinfo ['nickname'],
                'sex' => intval($userinfo ['sex']),
                'location' => $userinfo ['province'] . '-' . $userinfo ['city'],
                'avatar' => $userinfo ['headimgurl']
            );
            session("W_wx_userinfo", $weixuser);
        } else {
            $url = 'http://' . $_SERVER ['HTTP_HOST'] . $_SERVER ['REQUEST_URI'];
            $oauth_url = $we_obj->getOauthRedirect($url, "wxbase", $scope);
            header('Location: ' . $oauth_url);
        }
    }


    public function check_verify($code, $id = '', $config)
    {
        $verify = new \Think\Verify($config);
        return $verify->check($code, $id);
    }

    public function sendVerify()
    {
        $datas = I('post.');
        $mobile = $datas['mobile'];
        $is_bind = $datas['is_bind'];
        $M = M("User");
        if ($M->where(array("wx_openid" => $datas['wx_openid']))->count() > 0) {
            $this->ajaxReturn(array('status' => 0, 'msg' => "当前用户已经注册，请直接登录", "url" => U("Public/login")));
        };
        if (!$is_bind && $M->where(array('phone' => $mobile))->count() > 0) {
            $this->ajaxReturn(array('status' => 0, 'msg' => "当前用户已经注册，请直接登录", "url" => U("Public/login")));
        }

        $code = $datas['verify'];
        if (empty($code)) {
            $this->ajaxReturn(array('status' => 0, 'msg' => "验证码不可以为空", 'key' => 'code'));
        }
        $config = array(
            'reset' => false
        );
        if (!$this->check_verify($code, 'regcode', $config)) {
            $this->ajaxReturn(array('status' => 0, 'msg' => "验证码错误", 'key' => 'code'));
        }

        session(array('name' => 'phoneVerify', 'expire' => 600));
        $v = rand(1000, 9999);
        $ve = md5($v . $mobile);
        session('phoneVerify', $ve);
//        $str = '您正在进行修改操作，您的验证码是:'.$v.",十分钟内有效";
		send_sms($mobile, $v, '', 'SMS_170156204');
        $this->ajaxReturn(array('status' => 1, 'msg' => "发送成功", 'key' => 'vcode'));
    }

}