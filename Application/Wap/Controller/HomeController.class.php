<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class HomeController extends CommonController {
  	/*public function xffxlog(){
        cookie('jump_url',__SELF__);
        $this->authentication();
        $ML = M("ReferrerLog");
        $map = array(
            'uid' => $this->uid,
            'type' => 2,
          	'status' => 3
        );

        $count = $ML->where($map)->count();
        $pagesize = 20;
        $Page = new  \NewsLib\wapPage($count, $pagesize);
        $show       = $Page->show();
        $tabs = $ML->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('time DESC')->select();

        $this->assign("page", $show);
        $this->assign("list", $tabs);
        $this->display();
    }*/
  	public function earnings(){
        cookie('jump_url',__SELF__);
        $this->authentication();
        $ML = M("UserScoreLog");


        $map = array(
            'user_id' => $this->uid
        );
        if(I("get.typec") > 0){
            $map['type'] = I("get.typec");
        }

        if(I("get.type") <> 'a' || !I("get.type")){
            $count = $ML->where($map)->count();
            $pagesize = 20;
            $Page = new  \NewsLib\wapPage($count, $pagesize);
            $show       = $Page->show();
            $tabs = $ML->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('time DESC')->select();
            $nums = array(
                $ML->where(array('user_id' => $this->uid,'type' =>1))->sum("num"),
                $ML->where(array('user_id' => $this->uid,'type' =>2))->sum("num"),
                $ML->where(array('user_id' => $this->uid,'type' =>3))->sum("num"),
            );
        }else{
            $str=date("Y-m-d",strtotime("-1 day"))." 0:0:0";
            $star = strtotime($str);
            $str=date("Y-m-d",strtotime("-1 day"))." 24:00:00";
            $end = strtotime($str);

            $map['time'] = array(array('egt', $star),array('lt',$end));
            $count = $ML->where($map)->count();
            $pagesize = 20;
            $Page = new  \NewsLib\wapPage($count, $pagesize);
            $show       = $Page->show();
            $tabs = $ML->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('time DESC')->select();
            $nums = array(
                $ML->where(array('user_id' => $this->uid,'type' =>1,'time' => array(array('egt', $star),array('lt',$end))))->sum("num"),
                $ML->where(array('user_id' => $this->uid,'type' =>2,'time' => array(array('egt', $star),array('lt',$end))))->sum("num"),
                $ML->where(array('user_id' => $this->uid,'type' =>3,'time' => array(array('egt', $star),array('lt',$end))))->sum("num"),
            );
        }
        $this->assign("nums", $nums);
        $this->assign("page", $show);
        $this->assign("list", $tabs);

        $this->display();
    }
    public function signin(){
        $this->authentication();
        $uid = $this->uid;
        $username = session('W_username');
        $M = M("UserSigninLog");
        $time = strtotime(date("Y-m-d 0:0:0"));
        $info = $M->where(array('user_id'=>$uid, 'add_time' => $time))->find();
        if(!empty($info)){
            $this->error("亲！您今天已经签过到了！");
        }
        $score = $this->config['siteConfig']['system_basis']['sys_signin_score'];
        if($M->add(array('user_id'=>$uid, 'add_time' => strtotime(date("Y-m-d 0:0:0")), 'score' => $score))){
            M("User")->where(array('id'=> $uid))->setInc("score",$score);
            M("User")->where(array('id'=> $uid))->setInc("score_l",$score);
            M("UserScoreLog")->add(array(
                'user_id' => $uid,
                'type' => 3,
                'num' => $score,
                'time' => time()
            ));
            $this->success("成功签到");
        }else{
            $this->error("签到失败");
        }
    }
	public function myMessage(){
		$this->authentication();
		$M = M ( 'UserMessage' );
		$uid = $this->uid;
		$map = array(
				'to_user' => $uid,
		);
		$count = $M->where($map)->count();
		$pagesize = 10;
		$Page = new  \NewsLib\wapPage($count, $pagesize);
		$show       = $Page->show();
		$tabs = $M->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('time DESC,is_look')->select();
		$map['is_look'] = 0;
		$M->where($map)->save(array('is_look'=>1));
		$this->assign("page", $show);
		$this->assign("list", $tabs);
		$this->assign('navt',"个人中心");
		$this->display();
	}
	
	public function myFundLog(){
		$this->authentication();
		$uid = $this->uid;
		$M = M("UserAccount");
		$username = session('W_username');
		$map = array(
			'user_id' => $username,
		);
		$_GET['type'] ? $map['type'] = $_GET['type'] : '';
		$count = $M->where($map)->count();
		$pagesize = 10;
		$Page = new  \NewsLib\wapPage($count, $pagesize);
		$show       = $Page->show();
		
		$tabs = $M->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('add_time DESC')->select();
		//print_r($tabs);
		$this->assign('page',$show );
		$this->assign("list", $tabs);
		$this->assign('navt',"个人中心");
		$this->display();	
	}
	public function drawings(){
		$this->authentication();
		$uid = $this->uid;
		$userinfo = D('User')->where(array('id'=>$uid))->find();
		if(IS_POST){
			$data = $_POST;
			$y = session('TxVerify');
			$ve = md5($data['verify'].$userinfo['phone']);
			//if($ve <> $y)$this->error('验证码错误，请重新输入');
			$password_pay = D("Home")->payMd5($data['password'].$userinfo['salt']);
			//if($password_pay <> $userinfo['password_pay'])$this->error("支付密码不正确");
			if($data['money']  < 1)$this->error('最低提现金额 1元');
//			if(is_float($data['money'] / 100))$this->error('提现金额必须为100的倍数');
			if($data['money'] > $userinfo['user_money'] - $userinfo['frozen_money'])$this->error('你输入的金额大于你的可用余额！请重新输入');
            $array = array(
                'user_id' => $userinfo['username'],
                'money' => $data['money'],
                'user_note' => $data['user_note'],
                'add_time' => time(),
                'type' => 2,
                'payment' => $data['payment'],
                'data' => json_encode($data['data']),
                'admin_note' => ""
            );

			if($orderid = M('UserAccount')->add($array)){
                D('User')->where(array('id'=>$uid))->setInc('frozen_money',$data['money']);//冻结
                $arr = A('Payment');
                $return = $arr->withdraw($orderid);
                if($return['status']){
                    $this->success($return['info'],U("Home/myFundLog",array('type'=>2)),3);
                }else{
                    $this->error($return['info'],U("Home/myFundLog",array('type'=>2)),3);
                }
                $this->success("申请提交成功，请等待管理员审核",U("Home/myFundLog",array('type'=>2)),3);
			}else{
				$this->error('订单生成失败',U("Home/recharge"),3);	
			};
		}else{
			$this->assign('userinfo',$userinfo);
			$drawings_a = json_decode($userinfo['bank'],true);
			$this->assign('drawings_a',$drawings_a);
			$this->assign('navt',"个人中心");
			$this->display();
		}
	}
	//修改提现账号
	public function drawingsA(){
		$this->authentication();
		$uid = $this->uid;		
		if (IS_POST) {
			$arr = D("Home")->drawings_a();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
		} else {
			$user = D("User")->where(array('id'=>$uid))->find();
			$drawings_a = json_decode($user['bank'],true);
			$this->assign('drawings_a',$drawings_a);
			$this->assign('navt',"个人中心");
			$this->display();
		}
	}
	public function recharge(){
		$this->authentication();
		$uid = $this->uid;
		$userinfo = D('User')->where(array('id'=>$uid))->find();
		if(IS_POST){
			$data = $_POST;
			$rand = rand(1000,9999);
			$ordernum = 'R'.date("YmdHis").$rand;
			$payment = M('Payment')->where(array('name'=>$data['pay_type']))->find();
			$array = array(
				'user_id' => $uid,
				'type' => 2,
				'source' => 'Wap',
				'ordernum' => $ordernum,	
				'addtime' => time(),
				'pay_type' => $payment['id'],
				'ordersize' => $data['ordersize'],
				'goods_amount' => $data['ordersize'],
			);
			if($array['ordersize']  <= 0){
				$this->error('请正确输入充值金额');
			}
			if($orderid = D('Orders')->add($array)){
				//redirect();
				$this->success('马上去支付',U("Payment/index",array('order'=>$orderid,'payId'=>$array['pay_type'])),3);
			}else{
				$this->error('订单生成失败',U("Home/recharge"),3);
			}
		}else{
			$Payment = D('Payment')->select();
			$this->assign('list',$Payment);
			$this->assign('navt',"个人中心");
			$this->display();
		}
	}
	public function drawingsList(){
		
	}
	public function delDrawings(){
		$this->authentication();
		$uid = $this->uid;
		$username = session('W_username');
		$M = M("UserAccount");
		$info = $M->where(array('id'=>(int)$_GET['id']))->find();
		if($M->where(array('user_id'=>$username,'is_paid'=>0,'id'=>$_GET['id']))->save(array('is_paid'=>2))){
			M('User')->where(array('username'=>$info['user_id']))->setDec('frozen_money',$info['money']);
			$this->success("成功取消");
		}else{
			$this->error("取消失败");
		}
	}
	public function rechargeList(){
		$this->authentication();
		$uid = $this->uid;
		$M = M("UserAccount");
		$username = session('W_username');
		$count = $M->where(array('user_id'=>$username))->count();
		$pagesize = 10;
		$Page = new  \NewsLib\wapPage($count, $pagesize);
		$show       = $Page->show();
		
		$tabs = $M->where(array('user_id'=>$username))->limit($Page->firstRow.','.$Page->listRows)->order('add_time DESC')->select();
		$this->assign('page',$show );
		$this->assign("list", $tabs);
		$this->assign('navt',"个人中心");
		$this->display();	
	}
	public function wallet(){
		$this->authentication();
		$uid = $this->uid;
		$user = D("User")->where(array('id'=>$uid))->find();
		$this->assign("user", $user);
		$this->assign('navt',"个人中心");
		$this->display();	
	}
	//添加收藏
	public function addCollect(){
		$this->authentication();
		if(IS_AJAX){
			$arr = array(
				'goodsid' => $_POST['goodsid'],
				'user_id' => $this->uid,
			);
			if(M("UserCollect")->where($arr)->count()){
				if(M("UserCollect")->where($arr)->delete()){
					M("Goods")->where(array('id'=>$arr['goodsid']))->setDec('collect',1);
					$return = array('status' => 1, 'info' => "成功取消收藏");
				}else{
					$return = array('status' => 0, 'info' => "取消收藏失败");
				};
			}else{
				$arr['time'] = time();
				if(M("UserCollect")->add($arr)){
					M("Goods")->where(array('id'=>$arr['goodsid']))->setInc('collect',1);
					$return = array('status' => 1, 'info' => "收藏成功");
				}else{
					$return = array('status' => 0, 'info' => "收藏失败");
				};
			}
			$this->ajaxReturn($return);
		}else{
			$this->error("非法操作");
		}	
	}
	//删除收藏
	public function delcollect(){
		$this->authentication();
		$uid = $this->uid;
		$C = D('UserCollect');	
		if($C->where(array('user_id'=>$uid,'id'=>$_POST['id']))->delete()){
			$return = array('status' => 1, 'info' => "删除成功");
		}else{
			$return = array('status' => 0, 'info' => "删除失败");	
		}
		$this->ajaxReturn($return);
	}
	public function collect(){
		$this->authentication();
		$uid = $this->uid;
		$C = D('UserCollect');
		$map = array('user_id'=>$uid);
		$count = $C->where($map)->count();
		$pagesize = 10;
		$Page = new  \NewsLib\wapPage($count, $pagesize);
		$show       = $Page->show();
		$orders = $C->where($map)->order('time DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
		foreach($orders as $key=>$val){
			$orders[$key]['Goods'] = M('Goods')->where(array('id'=>$val['goodsid']))->find();
		}
		$this->assign('list',$orders);
		$this->assign('page',$show );
		$this->assign('navt',"个人中心");
		$this->display();	
	}
    public function index(){
		cookie('jump_url',__SELF__);
		$this->authentication();
		$uid = $this->uid;

		$user = $this->config['User'];
//		$orderNoPay = M("Orders")->where(array('user_id'=>$uid,'pay_status'=>0,'order_status'=>array('neq','2')))->count();
//		$orderNoD = M("Orders")->where(array('user_id'=>$uid,'shipping_status'=>1))->count();
		$rank = M('UserRank')->where(array('id'=>$user['user_rank']))->find();
		$userNoM = M("UserMessage")->where(array('to_user'=>$uid,'is_look'=>0))->count();
		$upUser = M("User")->where(array('id'=>$user['referrer_u']))->find();

		$this->assign("collect", M("UserCollect")->where(array('id'=>$user['id']))->count());
//		$this->assign("xffx", M("ReferrerLog")->where(array('uid' => $this->uid,/*'type' => 2, */'status'=> 3))->sum('money'));
		$this->assign("friendNum", $this->friendNum());
		$this->assign("upuser", $upUser);
		$this->assign("rank", $rank);
		$this->assign("user", $this->config['User']);
		$this->assign("userNoM", $userNoM);
//		$this->assign("orderNoD", $orderNoD);
//		$this->assign("orderNoPay", $orderNoPay);
		$this->assign('navt',"个人中心");
		$this->display();
    }
	

    public function friendNum(){
    	$this->authentication();
    	$uid = $this->uid;
    	$U = D("User");
    	$usera = $U->where(array('id'=>$this->config['User']['referrer_u']))->field("id")->find();
    	$scaleArr = $this->config['siteConfig']['popularize']['scale'];
    	$i = 0;
    	foreach ($scaleArr as $k=>$v){
    		if($i == 0){
    			$this->ids = array($uid);
    		}
    		$ids = array();
    		foreach ($this->usersss as $key=>$val){
    			$ids[] = $val['id'];
    		}
    		if($i > 0){
    			$this->ids = $ids;
    		}
    		$this->usersss = $user[$k] = $this->GetUserR($this->ids,array('user_rank'=>array('gt',0)));
    		$i++;
    	}
    	foreach ($user as $key => $val){
    		foreach ($val as $k => $v){
    			$userAll[] = $v;
    		}
    	}
    	return count($userAll);
    }
    public function GetUserR($uids,$where){
    	$U = D("User");
    	$where['referrer_u'] = array('in',$uids);
    	$user = $U->where($where)->field("id")->select();
    	$uid = $this->uid;
    	return $user;
    }
    
	public function info_nav(){
		$this->authentication();
		$this->assign('navt',"个人中心");
		$this->display();	
	}
	//修改信息
	public function editMyInfo(){
		$this->authentication();
		if (IS_POST) {
            $arr = D("Home")->editMyInfo();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
			$info = D('User')->where(array('id'=>$this->uid))->find(); 
			$this->assign("info", $info);
			$this->assign('navt',"个人中心");
            $this->display();
        }	
	}
	//修改登陆密码
	public function editPwd(){
		$this->authentication();
		if (IS_POST) {
            $arr = D("Home")->editPwd();
          	if(I('get.order') == 1){
            	$arr['url'] = U('Order/index');
            }
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
        	$uid = $this->uid;
        	$user = D("User")->where(array('id'=>$uid))->field("phone")->find();
        	$phone = $user['phone'];
        	$this->assign('phone',$phone);
        	$this->assign('navt',"个人中心");
            $this->display();
        }	
	}
	//修改支付密码
	public function editPayPwd(){
		$this->authentication();
		if (IS_POST) {
            $arr = D("Home")->editPayPwd();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
        	$uid = $this->uid;
        	$user = D("User")->where(array('id'=>$uid))->field("phone")->find();
        	$phone = $user['phone'];
        	$this->assign('phone',$phone);
        	$this->assign('navt',"个人中心");
            $this->display();
        }	
	}
	//修改手机号码
	public function editPhone(){
		$this->authentication();
		if (IS_POST) {
            $arr = D("Home")->editPhone();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
        	$uid = $this->uid;
        	$user = D("User")->where(array('id'=>$uid))->field("phone")->find();
        	$phone = $user['phone'];
        	$this->assign('phone',$phone);
        	$this->assign('navt',"个人中心");
            $this->display();
        }	
	}
	//收货地址
	public function address(){
		$this->authentication();
        $list = D('UserAddress')->where(array('user_id'=>$this->uid))->select(); 
		foreach($list as $key=>$val){
			$list[$key]['province'] = D('City')->where(array('var'=>$val['province']))->find();
			$list[$key]['city'] = D('City')->where(array('var'=>$val['city']))->find();
			$list[$key]['district'] = D('City')->where(array('var'=>$val['district']))->find();
		}
		$this->assign('navt',"个人中心");
		$this->assign("list", $list);
        $this->display();
	}
	//添加收货地址
	public function addAddress(){
		$this->authentication();
		if (IS_POST) {
            $arr = D("Home")->addUserAddress();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
			$info = array();
			$info['province'] = $this->getPid(array('mid'=>0));
			$this->assign("info", $info);
			$this->assign('navt',"个人中心");
            $this->display();
        }
	}
	//编辑收货地址
	public function editAddress(){
		$this->authentication();
		if (IS_POST) {
            $arr = D("Home")->editUserAddress();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
			$info = array();
			$info = D('UserAddress')->where(array('id'=>(int)$_GET['id'],'user_id'=>$this->uid))->find();
			$info['district'] = $this->getPid(array('mid'=>$info['city']),$info['district']);
			$info['city'] = $this->getPid(array('mid'=>$info['province']),$info['city']);
			$info['province'] = $this->getPid(array('mid'=>0),$info['province']);
			$this->assign("info", $info);
			$this->assign('navt',"个人中心");
            $this->display('addAddress');
        }
	}
	//删除发货地址
	public function delAddress(){
		$this->authentication();
		if(M('UserAddress')->where(array('id'=>(int)$_GET['id'],'user_id'=>$this->uid))->delete()){
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}	
	}
	private function getPid($where,$mid) {
        $list = D('City')->where($where)->select();
		$option.='<option value="0">请选择</option>';
        foreach ($list as $k => $v) {
            $selected = $v['var'] != $mid ? "" : ' selected="selected"';
            $option.='<option value="' . $v['var'] . '"' . $selected . '>' . $v['city'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }
	//提现时验证码
	public function sendTxVerify() {
        $this->authentication();
		$uid = $this->uid;
		$user = D("User")->where(array('id'=>$uid))->find();
		session(array('name'=>'TxVerify','expire'=>600));
		$v = rand(1000,9999);
		$ve = md5($v.$user['phone']);
		session('TxVerify',$ve);
		$str = '您正在进行提现操作，您的验证码是:'.$v.",十分钟内有效";
		send_sms($user['phone'],$str);
    }
	/*public function sendVerify() {
        $this->authentication();
		$uid = $this->uid;
		$user = D("User")->where(array('id'=>$uid))->find();
		session(array('name'=>'phoneVerify','expire'=>600));
		$v = rand(1000,9999);
		$ve = md5($v.$user['phone']);
		session('phoneVerify',$ve);
//		$str = '您正在进行修改操作，您的验证码是:'.$v.",十分钟内有效";
		send_sms($user['phone'],"#code#=".$v, '', 116365);
    }*/

    public function sendVerify() {
        $this->authentication();
        $uid = $this->uid;
//        $user = D("User")->where(array('id'=>$uid))->find();
        $post = I('post.');
        if($post['is_bind'] == 1){
            if (M("User")->where(array('id'=> array('neq', $this->uid), 'phone'=> $post['phone']))->count() > 0) {
                $this->ajaxReturn(array('status' => 0, 'msg' => "这个手机已经绑定过本站账号"));
            }
        }
        session(array('name'=>'phoneVerify','expire'=>600));
        $v = rand(1000,9999);
        $ve = md5($v.$post['mobile']);
        session('phoneVerify',$ve);
//		$str = '您正在进行修改操作，您的验证码是:'.$v.",十分钟内有效";
        send_sms($post['mobile'], $v, '', 'SMS_170156204');
        $this->ajaxReturn(array('status' => 1, 'msg' => "发送成功", 'key' => 'vcode'));
    }

	public function ajaxGetCity(){
		$where = array('mid'=>$_GET['id']);
		$data = $this->getPid($where);
        $this->ajaxReturn($data);
	}
}