<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class CategoryController extends CommonController {
    public function index(){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
		C('TOKEN_ON',false);
		$M = M('GoodsCategory');
		$Category = $this->getCategory($id = (int)$_GET['id']);
		$this->assign('navt',"商品分类");
		$this->assign('CategoryList',$Category);
		$this->display();
    }
	//获取下级所有包含的分类
	protected function getCategory($id = 0){
		$M = M('GoodsCategory');
		$array = $M->where(array('pid'=>$id))->order('sort ASC')->select();
		if($array){
			foreach($array as $key => $val){
				$val['list'] = $aaaa = $this->getCategory($val['id']);
				$arr[] = $val; 
			}
			return $arr; 
		}
	}
}