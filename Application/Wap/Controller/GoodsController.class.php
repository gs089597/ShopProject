<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class GoodsController extends CommonController {
    public function index(){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
		$info = D("Goods")->where(array('id'=>$_GET['id']))->find();
		if($this->uid){	
			$map = array(
					'goodsid' => $info['id'],
					'user_id' => $this->uid,
			);
			if(M("UserCollect")->where($map)->count()>0){
				$this->assign('is_col',1);
			};
		}

		$goodsType = D("GoodsType")->where(array('pid'=>$info['goods_type'],'attr_type'=>array('neq',0)))->select();
		foreach($goodsType as $key => $val){
			$goodsType[$key]['GoodsAttr'] = D('GoodsAttr')->where(array('attr_id'=>$val['id'],'goods_id'=>$info['id']))->select();
		}
		D("Goods")->where(array('id'=>$info['id']))->setInc('shownum');
		$info['Attr'] = $goodsType;
		$comment = D("GoodsComment")->where(array('goods_id'=>$info['id'],'status'=>1))->order('time DESC')->limit(5)->select();
		foreach($comment as $key => $val){
			$comment[$key]['User'] = D("User")->where(array('id'=>$val['user_id']))->field('username,id,nickname')->find();
		}
		$info['GoodsA'] = D("GoodsA")->where(array('id'=>$_GET['id']))->find();
        $info['Supplier'] = M("Supplier")->where(array('id'=>$info['supplier']))->find();
        $member_fee = unserialize($info['member_fee']);
        $info['member_fee'] = $member_fee[$this->config['User']['user_rank']];
		$this->assign('comment',$comment);
		$this->assign('info',$info);
		$this->assign('navt',$info['title']);
		$this->display();		
    }
	public function info(){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};	
		$goods_type = D("Goods")->field('goods_type,title')->where(array('id'=>$_GET['id']))->find();
		$info = D("GoodsA")->where(array('id'=>$_GET['id']))->find();
		$goodsType = D("GoodsType")->where(array('pid'=>$goods_type['goods_type']))->select();
		foreach($goodsType as $key => $val){
			$goodsType[$key]['GoodsAttr'] = D('GoodsAttr')->where(array('attr_id'=>$val['id'],'goods_id'=>$info['id']))->select();
		}
		$info['Attr'] = $goodsType;
		$this->assign('navt',$goods_type['title']);
		$this->assign('info',$info);
		$this->display();		
    }
	public function comment(){
		if($this->is_weixin()){
			$this->authentication();
		};
		$map = array(
			'goods_id' => $_GET['id'],
			'status'=>1
		);
		
		$pageconfig['count'] = D("GoodsComment")->where($map)->count();
		$pageconfig['pagesize'] = 10;
		$Page = new  \NewsLib\PageAjax($pageconfig['count'], $pageconfig['pagesize']);
		$show       = $Page->show();
		$comment = D("GoodsComment")->where($map)->order('time DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
		foreach($comment as $key => $val){
			$comment[$key]['User'] = D("User")->where(array('id'=>$val['user_id']))->field('username,id,nickname')->find();
		}
		$this->assign('comment',$comment);
		$this->assign('page',$show);
		$this->display();	
	}	
	
	public function popularize(){
		if($this->is_weixin()){
			$this->authentication();
		};
		$uid = $this->uid;
		if($uid){
			$user = M("User")->where(array('id'=>$uid))->find();
			$this->assign("user", $user);
		}
		$this->assign('navt',"分享推荐活动介绍");
		$this->display();
	}
}