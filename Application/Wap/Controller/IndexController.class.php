<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class IndexController extends CommonController {
    public function index(){  	  	
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
//		$dateO = new \Org\Util\Date();
		$GoodsListNew = D('Goods')->limit(20)->where(array('status' => 1, 'is_recycle'=>0,'is_putaway'=>1))->order("id DESC")->select();
        foreach ($GoodsListNew as $key => &$value){
            $member_fee = unserialize($value['member_fee']);
            $value['member_fee'] = $member_fee[$this->config['User']['user_rank']];
        }

 		$GoodsListHot = D('Goods')->where(array('status' => 1, 'is_hot'=>'1', 'is_recycle'=>0, 'is_putaway'=>1))->limit(20)->order("salesnum DESC")->select();
        foreach ($GoodsListHot as $key => &$value){
            $member_fee = unserialize($value['member_fee']);
            $value['member_fee'] = $member_fee[$this->config['User']['user_rank']];
        }

		$wappage = D('WapPage')->where(array('name'=>'index'))->find();
        $M = M("Nav");
        $nav = $M->order("sort ASC")->select();
        $News = M("News");
        $news = $News->where(array('type' => 0))->limit(5)->select();
        $this->assign('news_list',$news);
        $this->assign('nav',$nav);
		$this->assign('wappage',$wappage);
 		$this->assign('GoodsListHot',$GoodsListHot);
		$this->assign('GoodsListNew',$GoodsListNew);
		$this->display();
	}
	public function cpage(){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
		$wappage = D('WapPage')->where(array('name'=>$_GET['pageid']))->find();
		$this->assign('wappage',$wappage);
		$this->display();
	}
	public function siteswitch(){
		header("Content-Type:text/html; charset=utf-8");
		echo $this->config['siteConfig']['system_basis']['sys_site_cause'];	
	}
	//获取用户推广信息 /Wap/Index/popularize/type/u/user/1
    public function popularize(){
        cookie('popularize_u',null);
        cookie('popularize_s',null);

        $user_id = (int)$_GET['user'];
        //$user = M("User")->where(array('id' => $user_id))->find();
        //if ($user['user_rank'] > 0) {
            if ($_GET['type'] == 'u') {
                cookie('popularize_u', $user_id);
            } elseif ($_GET['type'] == 's') {
                cookie('popularize_s', $user_id);
            }
        //}
        if ($_GET['url']) {
            $url = base64_decode($_GET['url']);
        } else {
            $url = U("Index/index");
        }
        cookie('jump_url',$url);
        redirect($url);
    }


   	/*public function uploadImg(){
        header('Content-type: text/html; charset=UTF-8');
      	
        $this->authentication();
        $upload = new \Think\Upload(C('Upload'));// 实例化上传类
        $upload->savePath = './image/';
        $info = $upload->upload();
        if(!$info){
            $data['error']=1;
            $data['message']=$upload->getError();
            exit(json_encode($data));
        }
      //exit(__ROOT__.str_replace('./','/','/Uploads'.$info['file']['savepath']).'thumb_'.$info['file']['savename']);
        $data=array(
            'url'=>str_replace('./','/','/Uploads'.$info['file']['savepath']).$info['file']['savename'],
            'error'=>0,
        );
        if($_GET['thumb'] == 1){
            $image = new \Think\Image();
            $image->open('./'.$data['url']);// 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.jpg
            $image->thumb(150, 150)->save(".".str_replace('./','/','/Uploads'.$info['file']['savepath']).'thumb_'.$info['file']['savename']);
            $data['thumb'] = str_replace('./','/','/Uploads'.$info['file']['savepath']).'thumb_'.$info['file']['savename'];
        }
        exit(json_encode($data));
    }*/
}