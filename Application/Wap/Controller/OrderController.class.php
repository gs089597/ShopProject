<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class OrderController extends CommonController {
    public function myIntegralGoods(){
        $this->authentication();
        $uid = $this->uid;
        $M = M('Orders');
        $where = array(
            'O.pay_status' => 1,
            'O.user_id' => $uid,
            'G.type' => 2
        );
        $pageSize = 10;
        $count = $M->alias('O')
            ->join('JOIN __ORDER_GOODS__ OG ON OG.order_id = O.id')
            ->join('JOIN __GOODS__ G ON G.id = OG.goodsId')
            ->where($where)->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pageSize);
        //order_status
        //order_goods
        $list = $M->alias('O')
            ->field("O.order_status, O.pay_status, O.addtime, OG.*, G.integral_price")
            ->join('JOIN __ORDER_GOODS__ OG ON OG.order_id = O.id')
            ->join('JOIN __GOODS__ G ON G.id = OG.goodsId')
            ->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('O.addtime DESC')->select();
        foreach ($list as $key => &$value){
            $value['attr'] = json_decode($value['attr'], true);
        }
        $show = $Page->show();
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display();
    }
	public function index(){
		$this->authentication();
		$uid = $this->uid;
		$U = D('User');
		$cart = session("W_cart");
		$cart = json_decode($cart,true);
		$goodsPrice = 0;
        $goodsIntegralPrice = 0;
		$payGoods = array();
		foreach($cart as $key => $val){
			if($val['is_pay_c'] == 1){
				$val['goods'] = $Goods_or = D('Goods')->where(array('id'=>$val['goods']))->find();
				$orderGoods[] = array(
					'goodsthumb' => $Goods_or['thumb'] ? $Goods_or['thumb'] : $Goods_or['img'],
					'goodsprice' => $Goods_or['is_promotion'] ? $Goods_or['promotion_price'] : $Goods_or['price'],
					'goodstitle' => $Goods_or['title'],
					'goodsnum' => $Goods_or['goodsnum'],
					'cartId' => $val['cartId'],
					'goodsId' => $Goods_or['id'],
					'attr' => json_encode($val['attr']),
					'number' => $val['number'],
					'zPrice' => $val['zPrice'],
                    'zIntegralPrice' => $val['zIntegralPrice'] ? $val['zIntegralPrice'] : 0
				);
				$payGoods[] = $val;
				$goodsPrice += $val['zPrice'];
                $goodsIntegralPrice += $val['zIntegralPrice'];
			}
		}
		if(empty($orderGoods)){
			$this->error('没有选择商品哦！亲',U("Order/cart"),3);	
		}
		$user = $U->where(array('id'=>$uid))->find();
		//获取默认收货地址
		$city = D('UserAddress')->where(array('id'=>$user['default_address']))->find();
		$city['province'] = D('City')->where(array('var'=>$city['province']))->find();
		$city['city'] = D('City')->where(array('var'=>$city['city']))->find();
		$city['district'] = D('City')->where(array('var'=>$city['district']))->find();
		$user['default_address'] = $city;
		//获取默认支付方式
		$pay = D('Payment')->where(array('id'=>$user['default_pay']))->find();
		$user['default_pay'] = $pay;
		//获取默认物流
		/*$shipping = D('Shipping')->where(array('id'=>$user['default_shipping']))->find();
		$user['default_shipping'] = $shipping;
		
		$shippingPrice = $this->getSpPrice($payGoods,$user['default_shipping']['id']);
		$zPrice = $goodsPrice + $shippingPrice;*/
 		$zPrice = $goodsPrice;
		//print_r($payGoods);
		if(IS_POST){
		    $post = I('post.');
		    if(!empty($post['default_pay'])){
                $pay = D('Payment')->where(array('id'=>$post['default_pay']))->find();
                $user['default_pay'] = $pay;
            }
			$rand = rand(1000,9999);
			$ordernum = date("YmdHis").$rand;
			if($user['default_address']['id'] == ''){
				$this->error('请选择您的收货地址',U("Order/index"),3);	
			}
			if($user['default_pay']['id'] == ''){
				$this->error('请先选择支付方式',U("Order/index"),3);	
			}
			/*if($user['default_shipping']['id'] == ''){
				$this->error('请选择物流方式',U("Order/index"),3);	
			}*/
			if(empty($orderGoods)){
				$this->error('没有商品哦！亲',U("Order/cart"),3);	
			}
			$array = array(
				'user_id' => $uid,
				'type' => 1,
				'source' => 'Wap',
				'ordernum' => $ordernum,	
				'addtime' => time(),
				'address' => $user['default_address']['id'],
				'pay_type' => $user['default_pay']['id'],
				'ordersize' => $zPrice,
				'integral_size' => $goodsIntegralPrice,
				'goods_amount' => $goodsPrice,
				'address_a' => $user['default_address']['province']['city'].$user['default_address']['city']['city'].$user['default_address']['district']['city'].$user['default_address']['address'],
				'zipcode'=>$user['default_address']['zipcode'],
				'mobile'=>$user['default_address']['mobile'],
				'consignee'=>$user['default_address']['consignee'],
				//'shipping_fee' => $shippingPrice,
				//'shipping' => $user['default_shipping']['id'],
				'remarks' => $post['remarks'],
			);
			if($orderid = D('Orders')->add($array)){
				foreach($orderGoods as $k=>$v){
					unset($orderGoods[$k]['cartId']);
					$orderGoods[$k]['order_id']	= $orderid;
				}
				foreach($cart as $key => $val){
					if($val['is_pay_c'] == 1){												
						unset($cart[$key]);
					}
				}
				$str = json_encode($cart);
				session("W_cart",$str);
				if(D('OrderGoods')->addAll($orderGoods)){
					$msarr = array(
							array(
									'title' => "订单确认提醒",
									'description' => '您的订单号为：'.$ordernum.'的订单，已经提交成功，请尽快付款！'.$tixstr." \n <a href=\"http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Order/orderInfo',array('id'=>$orderid)) .'">查看详情</a>',
									'url' => "http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Order/orderInfo',array('id'=>$orderid)),
									'picurl' => "http://" . $_SERVER['HTTP_HOST'].$this->config['siteConfig']['system_basis']['sys_weix_img1'],
							)
					);
					setUserWeixinMessage($uid,$msarr[0]['description']);
					setAdminWeixinMessage("有用户提交了新商品订单，订单号：".$ordernum."，还未付款，请到后台查看");


                    if($zPrice == 0){
                        if($goodsIntegralPrice > $user['score']){
                            $this->error('您的积分不够支付');
                          	exit;
                        }
                        $this->ordersave($ordernum,$ordernum,time());
                        $this->error('支付成功',U("Order/all_order",array('all_order'=>1)),3);
                        exit;
                    }

					redirect(U("Payment/index",array('order'=>$orderid,'payId'=>$array['pay_type'])));	
				}else{
					D('Orders')->where(array('id'=>$orderid))->delete();	
					$this->error('订单生成失败',U("Order/index"),3);
				}; 
			}else{
				$this->error('订单生成失败',U("Order/index"),3);
			};
		}else{
            $Payment = D('Payment')->select();
            $this->assign('list',$Payment);
			//$this->assign('shippingPrice',$shippingPrice);
			$this->assign('zPrice',$zPrice);
            $this->assign('zIntegralPrice',$goodsIntegralPrice);
			$this->assign('goodsPrice',$goodsPrice);
			$this->assign('payGoods',$payGoods);
			$this->assign('user',$user);
			$this->display();
		}
    }
	public function getSpPrice($payGoods,$shippingId){
		$this->authentication();
		$shipping = M('Shipping')->where(array('id'=>$shippingId))->find();
		//计算物流金额
		if($shipping['fee_type'] == 1){
			$weight = 0;
			foreach($payGoods as $key=>$val){
				$weight += $val['goods']['weight'] * $val['number'];
			}
			$shippingPrice = $shipping['base_fee'];
			$weight = $weight - 500;
			$xShippingPrice = ceil($weight/500) * $shipping['step_fee'];
			$shippingPrice = $shippingPrice + $xShippingPrice;
		}elseif($shipping['fee_type'] == 2){
			$shippingPrice = $shipping['item_fee'];
		}
		return $shippingPrice;
	}
	public function orderSh(){
		$this->authentication();
		$uid = $this->uid;
		$O = D('Orders');
		$orderid = $_GET['order'];
		$order = $O->where(array('id'=>$orderid))->find();
		if($order['shipping_status'] == 1){
			$shipping_status = 2;
			$status = 1;
		}elseif($order['shipping_status'] == 3){
			$shipping_status = 4;
			$status = 3;
		}else{
			$this->error('非法操作！！');
		}
		if(D("OrderShipping")->where(array('user_id'=>$uid,'id'=>$_GET['shipping_order']))->save(array('status'=>1,'stime'=>time()))){		
			if($O->where(array('user_id'=>$uid,'shipping_status'=>$status,'id'=>$orderid))->save(array('shipping_status'=>$shipping_status,'shipping_time'=>time()))){
				setAdminWeixinMessage("订单号为 : ".$order['ordernum']."，已经成功收货，请前往确认");
				M('OrderGoods')->where(array('order_id'=>$orderid))->save(array('status'=>1));
				$this->success('操作成功');
			}else{
				$this->error('操作失败');
			}
		}else{
			$this->error('操作失败');
		}
	}
	public function orderQx(){
		$this->authentication();
		$uid = $this->uid;
		$O = D('Orders');
		$orders = M("Orders")->where(array('user_id'=>$uid,'order_status'=>0,'id'=>$_GET['order']))->find();
		
		$OrderGoods = M("OrderGoods")->where(array('order_id'=>$orders['id']))->select();
		foreach ($OrderGoods as $k => $v){
			if(M("Orders")->where(array('id'=>$orders['id'],'order_status'=>0,'user_id'=>$uid))->save(array('order_status'=>2))){
				$s = 1;
			}else{
				$s = 0;
			}
		}		
		if($s){
			$this->success('操作完成');
		}else{
			$this->error('操作失败');
		}
	}
	public function all_order(){
		$this->authentication();
		$uid = $this->uid;
		$O = D('Orders');
		$map = array('O.user_id'=>$uid);
		if((int)$_GET['type'] == 100){
			$map['O.type'] = array('in',array(3,4,5));
		}else if((int)$_GET['type'] < 10 && (int)$_GET['type'] <> ''){
			$map['O.type'] = (int)$_GET['type'];
		}
		$_GET['order_status'] <> '' ? $map['O.order_status'] = (int)$_GET['order_status'] : '';
		if($_GET['pay_status'] <> ''){
			$map['O.order_status'] = array("not in","2");
			$map['O.pay_status'] = (int)$_GET['pay_status'];
		}
		$shipping_status = (int)$_GET['shipping_status'];

		if($shipping_status == 1){
			$map['O.shipping_status']  = array("in","0,4");
			$map['O.order_status'] = array("not in","1,2");
			$map['O.pay_status'] = 1;
		}elseif($shipping_status == 2){
			$where['OS.status'] = 0;
			$where['O.shipping_status']  = array("in","1,3");
			$map['O.order_status'] = array("not in","2");
			$map['O.pay_status'] = array("not in","0");
			$where['_logic'] = 'or';
			$map['_complex'] = $where;
		}
		$count = M("")->table(C('DB_PREFIX')."orders as O")->join("LEFT JOIN ".C('DB_PREFIX').'order_shipping as OS ON OS.order_id = O.id')->field("O.*,OS.stime")->where($map)->count();
		$pagesize = 10;
		$Page = new  \NewsLib\wapPage($count, $pagesize);
		$show       = $Page->show();
		$orders = M("")->table(C('DB_PREFIX')."orders as O")->join("LEFT JOIN ".C('DB_PREFIX').'order_shipping as OS ON OS.order_id = O.id')->field("O.*,OS.stime,OS.status as shipping_order_status,OS.id as shipping_order_id,OS.touser as shipping_order_touser,OS.shipping_name as shipping_order_shipping_name,OS.shipping_order as shipping_order_shipping_oid")->order('O.addtime DESC')->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
		//echo M()->getLastSql();
		//$orders = $O->where($map)->order('addtime DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
// 		foreach($orders as $key=>$val){
// 			$orders[$key]["shipping_order"] = D("OrderShipping")->field("status,id")->where(array('order_id'=>$val['id'],'status'=>array('eq',0)))->order("id ASC")->find();
// 		}
// 		print_r($orders);
		$this->assign('list',$orders);
		$this->assign('page',$show );
		$this->display();	
	}
	public function orderInfo(){
		$this->authentication();
		$uid = $this->uid;
		$id = (int)$_GET['id'];
		$O = D('Orders');
		$orders = $O->where(array('user_id'=>$uid,'id'=>$id))->order('addtime DESC')->find();
		$city = D('UserAddress')->where(array('id'=>$orders['address']))->find();
		$city['province'] = D('City')->where(array('var'=>$city['province']))->find();
		$city['city'] = D('City')->where(array('var'=>$city['city']))->find();
		$city['district'] = D('City')->where(array('var'=>$city['district']))->find();
		$orders['address'] = $city;
		$pay = D('Payment')->where(array('id'=>$orders['pay_type']))->find();
		$orders['pay_type'] = $pay;
		$shipping = D('Shipping')->where(array('id'=>$orders['shipping']))->find();
		$orders['shipping'] = $shipping;
		$payGoods = D('OrderGoods')->where(array('order_id'=>$orders['id']))->select();
		$G = D('goods');
		$orders['shipping_time'];
		foreach($payGoods as $key=>$val){
			$goodsArr = $G->where(array('id'=>$val['goodsId']))->find();
			if($orders['type'] == 1){
				$payGoodsAttr = json_decode($val['attr'],true);
				$payGoods[$key]['attr'] = $payGoodsAttr;
			}
			$t = $this->config['siteConfig']['system_basis']['sys_exchanged'];
			$time = time() - $t * 3600 * 24 ;
			if($orders['shipping_time'] >= $time && $val['status'] < 2){
				$payGoods[$key]['is_exchanged'] = '<a class="uk-button uk-button-primary del_collect_1" href="'.
				U('Order/afterSale',array('goodsid'=>$payGoods[$key]['goodsId'],'id'=>$val['id'],'orderid'=>$orders['id'])).'">退换售后</a>';
			}elseif($orders['shipping_time'] >= $time && $val['status'] >= 2){
				$order_exchanged = M('order_exchanged')->where(array('ordergid'=>$val['id']))->find();
				if($order_exchanged['type'] == 2){
					$str = "换货";
				}elseif($order_exchanged['type'] == 3){
					$str = "退货";
				}
				if($order_exchanged['status'] == 0){
					$str_a = "等待审核";
				}elseif($order_exchanged['status'] == 1){
					$str_a = "进行中";
				}elseif($order_exchanged['status'] == 2){
					$str_a = "成功";
				}elseif($order_exchanged['status'] == 3){
					$str_a = "未通过审核";
				}	
				$payGoods[$key]['is_exchanged'] = '<a class="uk-button uk-button-primary del_collect_1" href="javascript:;">'.$str.$str_a.'</a>';
			}

            $payGoods[$key]['Supplier'] = M("Supplier")->where(array('id'=>$goodsArr['supplier']))->find();
		}

        $Payment = D('Payment')->select();
        $this->assign('list',$Payment);

		$shipping_orderarr = D("OrderShipping")->where(array('order_id'=>$orders['id']))->order("id DESC")->select();
		$this->assign('shipping_orderarr',$shipping_orderarr);
		$this->assign('payGoods',$payGoods);
		$this->assign('order',$orders);
		$this->display();		
	}
	//设置默认支付方式 
	public function payType(){
		$this->authentication();
		$uid = $this->uid;
		$M = D('User');
		if(IS_AJAX){
// 			$url = U('Order/index');
			$url = $_GET['type'] == 'rank' ? U('UserBuy/Buy') : U('Order/index');
			$data = array(
				'default_pay' => $_POST['default_pay']
			);
			if($M->where(array('id'=>$uid))->save($data)){
				$return = array('status' => 1, 'info' => "操作成功",'url'=>$url);
			}else{
				$return = array('status' => 0, 'info' => "操作失败",'url'=>$url);
			}
			$this->ajaxReturn($return);
		}else{
			$default = D('User')->where(array('id'=>$uid))->field('default_pay')->find();
			$Payment = D('Payment')->select();
			$this->assign('default',$default);
			$this->assign('list',$Payment);
			$this->display();
		}	
	}
	public function editPayType(){
		$this->authentication();
		$uid = $this->uid;
		$M = D('Orders');
		if(IS_AJAX){
			$data = array(
				'pay_type' => $_POST['default_pay']
			);
			if($M->where(array('id'=>$_POST['order'],'user_id'=>$uid))->save($data)){
				$return = array('status' => 1, 'info' => "操作成功",'url'=>U('Order/orderInfo',array('id'=>$_GET['order'])));
			}else{
				$return = array('status' => 0, 'info' => "操作失败",'url'=>U('Order/orderInfo',array('id'=>$_GET['order'])));
			}
			$this->ajaxReturn($return);
		}else{
			$order = $M->where(array('id'=>$_GET['order'],'user_id'=>$uid))->field('pay_type')->find();
			$default['default_pay'] = $order['pay_type'];
			$Payment = D('Payment')->select();
			$this->assign('default',$default);
			$this->assign('list',$Payment);
			$this->display('payType');
		}	
	}
	//设置默认物流
	public function shippingType(){
		$this->authentication();
		$uid = $this->uid;
		$M = D('User');
		if(IS_AJAX){
			$data = array(
				'default_shipping' => $_POST['default_shipping']
			);
// 			$url = U('Order/index');
			$url = $_GET['type'] == 'rank' ? U('UserBuy/Buy') : U('Order/index');
			if($M->where(array('id'=>$uid))->save($data)){
				$return = array('status' => 1, 'info' => "操作成功",'url'=>$url);
			}else{
				$return = array('status' => 0, 'info' => "操作失败",'url'=>$url);
			}
			$this->ajaxReturn($return);
		}else{
			$default = D('User')->where(array('id'=>$uid))->field('default_shipping')->find();
			$Payment = D('Shipping')->select();
			foreach($Payment as $key=>$val){
				$city_data = json_decode($val['city_data'],true);
				foreach($city_data as $k=>$v){
					$city[$key][] = D('City')->where(array('var'=>$v))->find();
				}
				$Payment[$key]['city_data'] = $city[$key];
			}
			$this->assign('default',$default);
			$this->assign('list',$Payment);
			$this->display();
		}	
	}
	public function editShippingType(){
		$this->authentication();
		$uid = $this->uid;
		$M = D('Orders');
		if(IS_AJAX){
			$payGoods = D('OrderGoods')->where(array('order_id'=>$_POST['order']))->select();
			foreach($payGoods as $key=>$val){
				$payGoods[$key]['goods'] = D('goods')->where(array('id'=>$val['goodsId']))->find();
			}
			$shippingPrice = $this->getSpPrice($payGoods,$_POST['default_shipping']);
			
			$data = array(
				'shipping' => $_POST['default_shipping'],
				'shipping_fee' => $shippingPrice
			);
			if($M->where(array('id'=>$_POST['order'],'user_id'=>$uid))->save($data)){
				$return = array('status' => 1, 'info' => "操作成功",'url'=>U('Order/orderInfo',array('id'=>$_GET['order'])));
			}else{
				$return = array('status' => 0, 'info' => "操作失败",'url'=>U('Order/orderInfo',array('id'=>$_GET['order'])));
			}
			$this->ajaxReturn($return);
		}else{
			$order = $M->where(array('id'=>$_GET['order'],'user_id'=>$uid))->field('shipping')->find();
			$default['default_shipping'] = $order['shipping'];
			$Payment = D('Shipping')->select();
			foreach($Payment as $key=>$val){
				$city_data = json_decode($val['city_data'],true);
				foreach($city_data as $k=>$v){
					$city[$key][] = D('City')->where(array('var'=>$v))->find();
				}
				$Payment[$key]['city_data'] = $city[$key];
			}
			$this->assign('default',$default);
			$this->assign('list',$Payment);
			$this->display('shippingType');
		}	
	}
	//设置默认配送地址
	public function address(){
		$this->authentication();
		$uid = $this->uid;
		$M = D('User');
		if(IS_AJAX){
			$data = array(
				'default_address' => $_POST['default_address']
			);
// 			$url = U('Order/index');
			$url = $_GET['type'] == 'rank' ? U('UserBuy/Buy') : U('Order/index');
			if($M->where(array('id'=>$uid))->save($data)){
				$return = array('status' => 1, 'info' => "操作成功",'url'=>$url);
			}else{
				$return = array('status' => 0, 'info' => "操作成功",'url'=>$url);
			}
			$this->ajaxReturn($return);
		}else{
			$default = D('User')->where(array('id'=>$uid))->field('default_address')->find();
			$addressArr = D('UserAddress')->where(array('user_id'=>$uid))->select();
			foreach($addressArr as $key=>$val){
				$addressArr[$key]['province'] = D('City')->where(array('var'=>$val['province']))->find();
				$addressArr[$key]['city'] = D('City')->where(array('var'=>$val['city']))->find();
				$addressArr[$key]['district'] = D('City')->where(array('var'=>$val['district']))->find();
			}
			if($addressArr == ''){
				$info['province'] = $this->getPid(array('mid'=>0));
				$this->assign('info',$info);
			}
			$this->assign('default',$default);
			$this->assign('list',$addressArr);
			$this->display();
		}
	}
	//添加收货地址
	public function addAddress(){
		$this->authentication();
		if (IS_POST) {
             $arr = D("Home")->addUserAddress();
             if($_POST['order'] <> ''){
             	$arr['status'] == 1 ? $this->success($arr['info'],U('Order/editAddress',array('order'=>$_POST['order'],'type'=>$_GET['type'])),3) : $this->error($arr['info']);
             }else{
             	$arr['status'] == 1 ? $this->success($arr['info'],U('Order/address',array('type'=>$_GET['type'])),3) : $this->error($arr['info']);
             }			
        }
	}
	public function editAddress(){
		$this->authentication();
		$uid = $this->uid;
		$M = D('Orders');
		if(IS_AJAX){
			$address_id = $_POST['default_address'];
			$city = D('UserAddress')->where(array('id'=>$address_id))->find();
			$city['province'] = D('City')->where(array('var'=>$city['province']))->find();
			$city['city'] = D('City')->where(array('var'=>$city['city']))->find();
			$city['district'] = D('City')->where(array('var'=>$city['district']))->find();
			$user['default_address'] = $city;
			$addarr_a = array(
					'address_a'=>$user['default_address']['province']['city'].$user['default_address']['city']['city'].$user['default_address']['district']['city'].$user['default_address']['address'],
					'zipcode' => $user['default_address']['zipcode'],
					'mobile'  => $user['default_address']['mobile'],
					'consignee' => 	$user['default_address']['consignee'],
					'address' => $address_id,
			);
			if($_GET['order']){
				if($M->where(array('id'=>$_POST['order'],'user_id'=>$uid))->save($addarr_a)){										
					$return = array('status' => 1, 'info' => "操作成功",'url'=>U('Order/orderInfo',array('id'=>$_GET['order'])));
				}else{
					$return = array('status' => 0, 'info' => "操作失败",'url'=>U('Order/orderInfo',array('id'=>$_GET['order'])));
				}
			}else{
			    if($_GET['type'] == "rank") {
                    $return = array('status' => 1, 'info' => "操作成功",'url'=>U('UserBuy/Buy'));
                }else{
                    $return = array('status' => 1, 'info' => "操作成功",'url'=>U('Order/index'));
                }
			}
			$this->ajaxReturn($return);
		}else{
			$order = $M->where(array('id'=>$_GET['order'],'user_id'=>$uid))->field('address')->find();
			$default['default_address'] = $order['address'];
			$addressArr = D('UserAddress')->where(array('user_id'=>$uid))->select();
			foreach($addressArr as $key=>$val){
				$addressArr[$key]['province'] = D('City')->where(array('var'=>$val['province']))->find();
				$addressArr[$key]['city'] = D('City')->where(array('var'=>$val['city']))->find();
				$addressArr[$key]['district'] = D('City')->where(array('var'=>$val['district']))->find();
			}
			$this->assign('default',$default);
			$this->assign('list',$addressArr);
			$this->display('address');
		}
	}
	//购物车
	public function cart(){
		cookie('jump_url',__SELF__);
        $cart = session("W_cart");
        $cart = json_decode($cart,true);

        $str = json_encode($cart);
        session("W_cart",$str);

		$zPrice = 0;
        $zIntegralPrice = 0;
		foreach($cart as $key => $val){
			$cart[$key]['goods'] = D('Goods')->where(array('id'=>$val['goods']))->find();
			if($val['is_pay_c'] == 1){
				$zPrice += $val['zPrice'];
				$zIntegralPrice += $val['zIntegralPrice'];
			}
		}
        $this->assign('zIntegralPrice',$zIntegralPrice);
		$this->assign('zPrice',$zPrice);
		$this->assign('info',$cart);
		$this->display();
	}
	//删除购物车物品
	public function delCart(){
        $this->authentication();
		if(IS_AJAX){
			$getcartId = $_GET['cartId'];
			$cart = session("W_cart");
			$cart = json_decode($cart,true);
			foreach($cart as $key => $val){
				if($getcartId == $val['cartId']){
					unset($cart[$key]);	
				}
			}
			$str = json_encode($cart);
			session("W_cart",$str);
			
			$zPrice = 0;
			foreach($cart as $key => $val){
				if($val['is_pay_c'] == 1){
					$zPrice += $val['zPrice'];
				}
			}
			$return = array(
				'zPrice' => $zPrice
			);
			if(count($cart) <= 0){
				$return['url'] = U('Order/cart');
			}
			$this->ajaxReturn($return);	
		}else{
			$this->error("非法操作");
		}
	}
	//ajax添加商品数量
	public function setCartNum(){
        $this->authentication();
		if(IS_AJAX){
			$is_pay_c = $_GET['is_pay_c'];
			$getcartId = $_GET['cartId'];
			$getnum = $_GET['num'];
			$cart = session("W_cart");
			$cart = json_decode($cart,true);
			$attrPrice = 0;
			foreach($cart as $key => $val){
				$Goods = D("Goods")->where(array('id'=>$val['goods']))->find();
				if($Goods['is_promotion'] == 1){
					$a_goodsprice = $Goods['promotion_price'];
				}else{
                    if($this->config['User']['user_rank'] > 0){
                        $member_fee = unserialize($Goods['member_fee']);
                        $a_goodsprice = $member_fee[$this->config['User']['user_rank']];
                    }else{
                        $a_goodsprice = $Goods['price'];
                    }
				}
				if($getcartId == $val['cartId']){
                    $number = $this->getgoodsnums($Goods, $getnum);
                    if($number > 0){
                        $getnum = $number;
                    }
					foreach($val['attr'] as $k => $v){
						$attrPrice += $v['attrPrice'];
					}
					$cart[$key]['is_pay_c'] = $is_pay_c;//是否结算
					$cart[$key]['number'] = $getnum;
					$cart[$key]['zPrice'] = $goodsPrice = ($attrPrice + $a_goodsprice) * $getnum;
                    if($Goods['type'] == 2) {
                        $cart[$key]['zIntegralPrice'] = $goodsIntegralPrice = $Goods['integral_price'] * $getnum;
                    }
				}
			}
			$str = json_encode($cart);
			session("W_cart",$str);
			$zPrice = 0;
			$zIntegralPrice = 0;
			foreach($cart as $key => $val){
				if($val['is_pay_c'] == 1){
					$zPrice += $val['zPrice'];
                    $zIntegralPrice += $val['zIntegralPrice'];
				}
			}
			$return = array(
                'zIntegralPrice' => $zIntegralPrice,
				'zPrice' => $zPrice,
				'number' => $getnum,
				'goodsIntegralPrice' => $goodsIntegralPrice,
				'goodsPrice' => $goodsPrice,
			);
			$this->ajaxReturn($return);
		}else{
			$this->error("非法操作");
		}	
	}
	//添加商品到购物车
	public function addCart(){
        $this->authentication();
		if(IS_AJAX){
			$is_g = true;
			$Goods = D("Goods")->where(array('id'=>$_POST['goodsid']))->find();
            if($Goods['user_id'] == $this->uid){
                $return = array('status' => 0, 'info' => "不可以购买您自己上传的商品");
                $this->ajaxReturn($return);
            }
            $price = $_POST['price'];
            $number = $_POST['number'];
            $number_1 = $this->getgoodsnums($Goods, $number);
            if($number_1 <= 0){
                $return = array('status' => 0, 'info' => "当前商品您已经超过限购数量，不可以再购买了咯");
                $this->ajaxReturn($return);
            }else{
                $number = $number_1;
            }

            if($Goods['is_promotion'] == 1){
                $a_goodsprice = $Goods['promotion_price'];
            }else{
                if($this->config['User']['user_rank'] > 0){
                    $member_fee = unserialize($Goods['member_fee']);
                    $a_goodsprice = $member_fee[$this->config['User']['user_rank']];
                }else{
                    $a_goodsprice = $Goods['price'];
                }
            }

            if($Goods['type'] == 2){
                $a_goods_integral_price = $Goods['integral_price'];
                $zIntegralPrice = $a_goods_integral_price * $number;
            }

			for($i=1;$i<20;$i++){
				if($_POST['attrid_'.$i]){
					$arr[] = array(
						'attrId' => $_POST['attrid_'.$i],
						'attr' => $_POST['attr_'.$i],
						'attrPrice' => $_POST['price_'.$i],
					);
				}else{
					continue;	
				}
			}
			$attrPrice = 0;
			foreach($arr as $key=>$val){
				$goodsAttr = D("GoodsAttr")->where(array('goods_attr_id'=>$val['attrId']))->find();
				if($goodsAttr['attr_price'] <> $val['attrPrice']){
					$return = array('status' => 0, 'info' => "非法操作");
					$is_g = false;
					break;
				}
				$attrPrice += $goodsAttr['attr_price'];
			}
			if((float)$price <> (float)$a_goodsprice){
				$return = array('status' => 0, 'info' => "非法操作");
				$is_g = false;
			}
			$zPrice = ($a_goodsprice + $attrPrice) * $number;
			if($is_g){
				$cart = session("W_cart");
				$cart = json_decode($cart,true);
				$count = count($cart);
                if($_POST['is_pay_c'] == 1){
                    foreach($cart as $key=>$val){
                        $cart[$key]['is_pay_c'] = 0;
                    }
                }
                $add_s = true;
                foreach($cart as $key=>$val){
                    if($val['goods'] == $_POST['goodsid'] && $arr == $val['attr']){
                        //$number_a = $this->getgoodsnums($Goods, $number + $val['number']);
                        $number_a = $this->getgoodsnums($Goods, $number);
                        if($number_a > 0){
                            $number = $number_a;
                        }
                        if($Goods['type'] == 2) {
                            $cart[$key]['zIntegralPrice'] = $a_goods_integral_price * $number;
                        }
                        $cart[$key]['zPrice'] = ($a_goodsprice + $attrPrice) * $number;
                        $cart[$key]['price'] = $a_goodsprice + $val['price'];
                        $cart[$key]['is_pay_c'] = $_POST['is_pay_c'];
                        $cart[$key]['number'] = $number;
                        $add_s = false;
                    }
                }
                if($add_s){
                    $cartgoods = array(
                        'cartId' => $count + 1,
                        'type' => $Goods['type'],
                        'goods' => $_POST['goodsid'],
                        'zPrice' => $zPrice,//加属性的总价格
                        'price' => $a_goodsprice,
                        'attr' => $arr,
                        'number' => $number,
                        'is_pay_c' => $_POST['is_pay_c'],//是否结算
                    );
                    if($Goods['type'] == 2) {
                        $cartgoods['zIntegralPrice'] = $zIntegralPrice;
                    }
                    $cart[] = $cartgoods;
                }
				$str = json_encode($cart);
				session("W_cart",$str);
				if($_POST['is_pay_c'] == 1){
                    $return = array('status' => 1, 'info' => "马上去结算",'url'=>U('Order/index'));
				}else{
					$return = array('status' => 1, 'info' => "添加成功");
				}
			}
			$this->ajaxReturn($return);
		}else{
			$this->error("非法操作");
		}
	}

	//商品评论
    public function comment(){
        $this->authentication();
        $M = M('GoodsComment');
        if(IS_POST){
            $_POST['time'] = time();
            $_POST['status'] = 1;
            $_POST['user_id'] = $this->uid;
            $M->create($_POST);
            if($M->add()){
                M('OrderGoods')->where(array('id'=>$_POST['order_goods_id']))->save(array('is_comment'=>1));
                $this->success('评论成功',U('Order/orderInfo',array('id'=>$_POST['orderid'])),3);
            }else{
                $this->error("操作失败");
            };
        }else{
            $this->display();
        }
    }
	//退换货
	public function afterSale(){
		$this->authentication();
		$M = M('OrderExchanged');
		if(IS_POST){
			$_POST['addtime'] = time();
			$_POST['status'] = 0;
			$_POST['user_id'] = $this->uid;
			$_POST['ordernum'] = date("YmdHis",time()).rand(1000,9999);
			$type = $_POST['type'];
			$M->create($_POST);
			if($M->add()){
				M('OrderGoods')->where(array('id'=>$_POST['ordergid']))->save(array('status'=>$type));	
				if($type == 3){
					M('ReferrerLog')->where(array('order_goods_id'=>$_POST['ordergid']))->save(array('status'=>1));
				}				
				$this->success('您的申请已经提交，客服人员会尽快和您取的联系',U('Order/orderInfo',array('id'=>$_POST['orderid'])),3);
			}else{
				$this->error("操作失败");
			};
		}else{
			$this->display();
		}
	}
	//退换货
	public function afterSaleList(){
		$this->authentication();
		$M = M ( 'OrderExchanged' );
		$uid = $this->uid;
		$map = array(
				'user_id' => $uid,
		);
		$count = $M->where($map)->count();
		$pagesize = 10;
		$Page = new  \NewsLib\wapPage($count, $pagesize);
		$show       = $Page->show();
		$tabs = $M->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('time DESC,is_look')->select();
		$map['is_look'] = 0;
		$M->where($map)->save(array('is_look'=>1));
		$this->assign("page", $show);
		$this->assign("list", $tabs);
		$this->display();
	}
	private function getPid($where,$mid) {
        $list = D('City')->where($where)->select();
        foreach ($list as $k => $v) {
            $selected = $v['var'] != $mid ? "" : ' selected="selected"';
            $option.='<option value="' . $v['var'] . '"' . $selected . '>' . $v['city'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }
}