<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class ActController extends CommonController {
    public function index(){
		cookie('jump_url',__SELF__);
		if($this->is_weixin()){
			$this->authentication();
		};
		$Goods = M('Goods');
		$wappage = D('WapActPage')->where(array('id'=>(int)$_GET['cid']))->find();
		
		$M = M('GoodsCategory');
		$Category = $M->where(array('id'=>$wappage['category']))->find();
		$Category['list'] = $this->getCategory($Category['id']);
		$Catid = $this->getCategoryId($Category['list']);
		$Catid[] = (int)$_GET['category'];
		
		$map = array(
			'category' => array('in',$Catid),
			'is_putaway' => array('eq',1),
		);
		//获取分页
		$pageconfig['count'] = $Goods->where($map)->count();
		$pageconfig['pagesize'] = 15;
		$Page = new  \NewsLib\Page($pageconfig['count'], $pageconfig['pagesize']);
		
		$list = $Goods->where($map)->order($orderby)->limit($Page->firstRow.','.$Page->listRows)->select();
		
		if(IS_AJAX){
			$this->ajaxReturn($list);
		}else{
			$this->assign('navt',"分类搜索");
			$this->assign('pageconfig',$pageconfig);
			$this->assign('list',$list);
			$this->assign('wappage',$wappage);
			$this->display($wappage['tpl']);
		}
	}
	
	protected $carid;
	//获取下级所有包含的分类
	protected function getCategoryId($arr){
		foreach($arr as $key => $val){
			$this->carid[] = $val['id'];
			if(is_array($val['list'])){
				$aaa = $this->getCategoryId($val['list']);
			}
		}
		return $this->carid;
	}
	
	//获取下级所有包含的分类
	protected function getCategory($id = 0){
		$M = M('GoodsCategory');
		$array = $M->where(array('pid'=>$id))->order('sort ASC')->select();
		if($array){
			foreach($array as $key => $val){
				$val['list'] = $aaaa = $this->getCategory($val['id']);
				$arr[] = $val; 
			}
			return $arr; 
		}
	}
	
}