<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
use  Common\Controller\SystemController;
class CommonController extends SystemController {
	public $uid;
	public function _initialize() {
        parent::_initialize();
		$this->config = $this->configArr();
		if($this->config['siteConfig']['system_basis']['sys_site_switch'] <> 1){
			if(ACTION_NAME <> 'siteswitch'){
				$this->error('站点升级中.......',U('Index/siteswitch'));
			}
		}
		$popularize_u = cookie('popularize_u');
		if($popularize_u){
			$user = M ( "User" )->where ( array ("id" => $popularize_u ) )->find ();
			$this->assign('popularize_info',$user);
		}
        if(I('get.is_app')){
            cookie("is_app", 1);
        }
		$this->is_weixin();
		$this->jsSdk();
		$this->assign('is_edit_pwd',$this->is_edit_pwd);
		$this->assign('config',$this->config);
	}
	//微信JSSDK调用
	public function jsSdk(){
		if($this->is_weixin()){
			$options = $this->config['siteConfig']['wechat'];
			$we_obj = new \NewsLib\Wechat($options);
			$auth = $we_obj->checkAuth();
			$js_ticket = $we_obj->getJsTicket();
			if (!$js_ticket) {
				echo "获取js_ticket失败！<br>";
				echo '错误码：'.$we_obj->errCode;
				echo ' 错误原因：'.ErrCode::getErrText($we_obj->errCode);
				exit;
			}
			$url = "http://".$_SERVER['HTTP_HOST'].__SELF__;
			$js_sign = $we_obj->getJsSign($url);
			$this->assign('js_sign',$js_sign);
		}
	}
	//判断是否是微信浏览器
	public function is_weixin(){
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) {
			$this->assign('is_weixin',"Y");
			return true;
		}else{
			$this->assign('is_weixin',"N");
			return false;
		}
	}

    //添加限购
    public function getgoodsnums($Goods, $number){
        $gnum = -1;
        if($Goods['u_xg'] > 0){
            $gnum = 0;
            $map = array(
                'O.type' => 1,
                'OG.goodsId' => $Goods['id'],
                'O.user_id' => $this->config['User']['id']
            );
            $where['O.order_status']  = 1;
            $where['O.pay_status']  = 1;
            $where['_logic'] = 'or';
            $map['_complex'] = $where;
            $number_1 = M("")->table(C('DB_PREFIX') . "orders as O")->join(C('DB_PREFIX') . 'order_goods as OG ON OG.order_id = O.id')->field("O.*,OS.stime")->where($map)->sum('OG.number');
            $gnum = $Goods['u_xg'] - $number_1;
        }
        if($Goods['t_xg'] > 0){
            $gnum = 0;
            $map = array(
                'O.type' => 1,
                'OG.goodsId' => $Goods['id'],
                'O.user_id' => $this->config['User']['id'],
                'O.addtime' => array(
                    array("gt", strtotime(date("Y-m-d", time()))),
                    array("lt", time()),
                ),
            );
            $where['O.order_status']  = 1;
            $where['O.pay_status']  = 1;
            $where['_logic'] = 'or';
            $map['_complex'] = $where;
            $number_1 = M("")->table(C('DB_PREFIX') . "orders as O")->join(C('DB_PREFIX') . 'order_goods as OG ON OG.order_id = O.id')->field("O.*,OS.stime")->where($map)->sum('OG.number');
            $gnum = $Goods['t_xg'] - $number_1;
        }
        $card = 0;
        if($gnum > 0){
            if($number >= $gnum){
                $card = $gnum;
            }else{
                $card = $number;
            }
        }
        if($Goods['u_xg'] <= 0 && $Goods['t_xg'] <= 0){
            $card = $number;
        }
        return $card;
    }
}