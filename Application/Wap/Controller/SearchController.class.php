<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Wap\Controller;
use Think\Controller;
class SearchController extends CommonController {
    public function index(){
        cookie('jump_url',__SELF__);
        if($this->is_weixin()){
            $this->authentication();
        };
        C('TOKEN_ON',false);
        $Goods = M('Goods');
        $category = I("get.category");
        if(!$category) $this->error("请选择分类");
        $get = I("get.");
        if($category){
            //获取搜索条件
            $M = M('GoodsCategory');
            $Category = $M->where(array('id'=>$category))->find();
            $Category['list'] = $this->getCategory($Category['id']);
            $Catid = $this->getCategoryId($Category['list']);
            $Catid[] = $category;

            $map = $jgqj = array(
                'category' => array('in',$Catid),
                'is_recycle'=>0, 'is_putaway'=>1, 'status' => 1
            );
        }else{
            $map = $jgqj = array(
                'is_recycle'=>0, 'is_putaway'=>1, 'status' => 1
            );
        };
        $get['user_id'] <> '' ? $map['user_id'] = $get['user_id'] : ''; //商品类型
        $get['G.type'] <> '' ? $map['G.type'] = $get['type'] : ''; //商品类型
        $get['brand'] <> '' ? $map['brand'] = $get['brand'] : ''; //品牌
        $price_min = $get['price_min'];
        $price_max = $get['price_max'];
        if($price_min == 0 && $price_max == 0){
        }elseif($price_min <> 0 && $price_max <> 0){
            $map['price'] = array(array('egt',$price_min),array('elt',$price_max));
        }elseif($price_min <> 0 && $price_max == 0){
            $map['price'] = array('egt',$price_min);
        }elseif($price_min == 0 && $price_max <> 0){
            $map['price'] = array(array('gt',$price_min),array('elt',$price_max));
        }
        $map_1['title'] = array('like','%'.$get['key'].'%');
        $map_1['goodsnum'] = array('like','%'.$get['key'].'%');
        $map_1['_logic'] = 'or';
        $map['_complex'] = $map_1;
        //获取排序
        if($get['sort'] == 1){
            $orderby['salesnum']='desc';
        }elseif($get['sort'] == 2){
            $orderby['price']='asc';
            $orderby['salesnum']='desc';
        }elseif($get['sort'] == 3){
            $orderby['price']='desc';
            $orderby['salesnum']='desc';
        }else{
            $orderby['salesnum']='desc';
            $orderby['price']='asc';
        }
        $GoodsType = [];
        if($Category['types_id']){
            $GoodsType = M('GoodsType')->where(array('inputtype' => 3, 'pid' => array('in', explode(',', $Category['types_id']))))->select();
            foreach ($GoodsType as $key => &$val){
                if($get['attr_'.$val['id']]){
                    $map['GA_'.$val['id'].'.attr_value'] = $get['attr_'.$val['id']];
                    $map['GA_'.$val['id'].'.attr_id'] = $val['id'];
                }
                $val['items'] = explode('|', $val['data']);
            }
            $this->assign('GoodsType', $GoodsType);
        }
        $goodscount = $Goods->alias('G');
        foreach ($GoodsType as $key => $value){
            if($get['attr_'.$value['id']]){
                $goodscount->join('__GOODS_ATTR__ GA_'.$value['id'].' ON G.id = GA_'.$value['id'].'.goods_id');
            }
        }
        $pageconfig['count'] = $goodscount->where($map)->count();
        $pageconfig['pagesize'] = 15;
        $Page = new  \NewsLib\Page($pageconfig['count'], $pageconfig['pagesize']);

        //获取价格区间
        $grade = $this->getGrade($Category,$jgqj);
        $this->assign('grade',$grade);
        $list = $Goods->alias('G');
        foreach ($GoodsType as $key => $value){
            if($get['attr_'.$value['id']]){
                $list->join('__GOODS_ATTR__ GA_'.$value['id'].' ON G.id = GA_'.$value['id'].'.goods_id');
            }
        }
        $list = $list->where($map)->order($orderby)->limit($Page->firstRow.','.$Page->listRows)->select();

        foreach ($list as $key => &$value){
            $member_fee = unserialize($value['member_fee']);
            $value['member_fee'] = $member_fee[$this->config['User']['user_rank']];
        }
        $this->assign('Brand',$this->getBrand($jgqj));

        $this->assign('pageconfig',$pageconfig);
        $this->assign('list',$list);

        if(IS_AJAX){
            $this->display("ajaxlist");
        }else{
            $this->assign('navt',"商品搜索");
            $this->display();
        }

    }
    //获取品牌
    protected function getBrand($map){
        $Goods = M('Goods');
        unset($map['brand']);
        $brand = $Goods->where($map)->field("brand")->group('brand')->select();
        $arr = array();
        foreach($brand as $k=>$v){
            $arr[] = $v['brand'];
        }
        $idarr['id']  = array('in',$arr);
        $brand = D("GoodsBrand")->where($idarr)->select();
        return $brand;
    }
    //获取价格区间
    protected function getGrade($cat,$map){
        if ($cat['grade'] == 0  && $cat['pid'] != 0){
            //如果当前分类级别为空，取最近的上级分类
            $M = M('GoodsCategory');
            $Category = $M->where(array('id'=>$cat['pid']))->find();
            $cat['grade'] = $Category['grade'];
        }
        if ($cat['grade'] > 1){
            //获得当前分类下商品价格的最大值、最小值
            $Goods = M('Goods');
            $row['max'] = $Goods->where($map)->max('price');
            $row['min'] = $Goods->where($map)->min('price');
            $array = setPriceRange($row['min'],$row['max'],0,$cat['grade']);
            return $array;
        }
    }

    protected $carid;
    //获取下级所有包含的分类
    protected function getCategoryId($arr){
        foreach($arr as $key => $val){
            $this->carid[] = $val['id'];
            if(is_array($val['list'])){
                $aaa = $this->getCategoryId($val['list']);
            }
        }
        return $this->carid;
    }

    //获取下级所有包含的分类
    protected function getCategory($id = 0){
        $M = M('GoodsCategory');
        $array = $M->where(array('pid'=>$id))->order('sort ASC')->select();
        if($array){
            foreach($array as $key => $val){
                $val['list'] = $aaaa = $this->getCategory($val['id']);
                $arr[] = $val;
            }
            return $arr;
        }
    }

}