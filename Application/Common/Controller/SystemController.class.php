<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Common\Controller;

use Think\Controller;

class SystemController extends Controller
{
    public function _initialize() {
        $this->configArr();
        $this->setPromotion();
        $this->dividendDispose();
        $this->jsRank();
    }

    //实现分销分成
    protected function dividendDispose()
    {
        $t = $this->config_a['siteConfig']['system_basis']['sys_profit_t'];
        $time = time() - $t * 3600 * 24;
        //$time = time();
        $map = array(
            //'shipping_time' => array('egt',$time),
            //'OS.stime' => array('elt',$time),
            //'OS.status' => array('eq',1),
//            'type' => 1,
            'dividend_status' => 0,
            'pay_status' => 1
        );
//        $orders_a = M("")->table(C('DB_PREFIX') . "orders as O")->join(C('DB_PREFIX') . 'order_shipping as OS ON OS.order_id = O.id')->field("O.*,OS.stime")->where($map)->select();
        $orders_a = M("orders")->where($map)->select();
        $map = array(
            'type' => array('in', array(3, 4, 5)),
            'dividend_status' => 0,
            'order_status' => 1
        );
        $orders_b = M("Orders")->where($map)->select();
        if (!empty($orders_a) && !empty($orders_b)) {
            $orders = array_merge($orders_a, $orders_b);
        } elseif (empty($orders_a) && !empty($orders_b)) {
            $orders = $orders_b;
        } elseif (!empty($orders_a) && empty($orders_b)) {
            $orders = $orders_a;
        }
        $U = M('User');
        $R = M('ReferrerLog');
        $timea = time();
        foreach ($orders as $key => $val) {
            $re = $R->where(array('oid' => $val['id'], 'status' => 2))->select();
            foreach ($re as $ka => $va) {
                $user = $U->where(array('id' => $va['uid']))->find();
                $scoreA['user_money'] = $user['user_money'] + $va['money'];
                $scoreA['referrer_money_l'] = $user['referrer_money_l'] + $va['money'];
                $U->where(array('id' => $user['id']))->save($scoreA);
                $R->where(array('id' => $va['id']))->save(array('status' => 3, 'time' => $timea));
                addUserAccount($user['username'], $va['money'], 1, '分销返利分成，订单编号：' . $val['ordernum'], '站内充值', $timea);
            }
            M("Orders")->where(array('id' => $val['id']))->save(array('dividend_status' => 1));
        }
    }
    //计算用户会员等级
	protected function jsRank(){
		$rank = M('UserRank')->order('rank ASC')->select();
		foreach($rank as $key=>$val){
			$arr = array(
				'order_amount'=>array('egt',$val['min_points']),
			);
			M('User')->where($arr)->save(array('user_rank'=>$val['id']));
		}
	}
    protected function setPromotion()
    {
        $map = array(
            'promotion_startime' => array('egt', time()),
            'promotion_endtime' => array('elt', time()),
            '_logic' => 'or',
        );
        D("Goods")->where($map)->save(array('is_promotion' => 0));
        $map = array(
            'promotion_startime' => array('elt', time()),
            'promotion_endtime' => array('egt', time()),
        );
        D("Goods")->where($map)->save(array('is_promotion' => 1));
    }

    //登陆验证
    protected function authentication()
    {
        $uid = session('W_uid');
        if (!empty($uid)) {
            $usershell = session('W_usershell');
            $username = session('W_username');
            $user = M('User');
            $condition = array(
                'id' => $uid,
                'username' => $username
            );
            $info = $user->where($condition)->find();
            if ($usershell == md5(md5($info['id'] . $info['username']) . $info['password'] . $info['salt'])) {
                $yzdl = true;
            } else {
                $yzdl = false;
            }
        } else {
            $yzdl = false;
        }
        if ($yzdl == false) {
            if (IS_AJAX) {
                $this->error("登陆超时！！请重新登陆", U('Public/login'));
            } else {
                $this->redirect('Public/login');
            }
        }
    }

    //初始化
    protected function configArr()
    {
        $this->uid = $uid = session('W_uid');
        $username = session('W_username');
        $user = M('User');
        $condition = array(
            'id' => $uid,
            'username' => $username
        );
        $rs = $user->where($condition)->field('password,salt', true)->find();
        $user_rank = M('UserRank')->where(array('id'=>$rs['user_rank']))->find();
		$this->is_edit_pwd = $rs['is_edit_pwd'];
        //$this->is_edit_pwd = 1;
        $array['User'] = $rs;
        $array['User']['user_rank_info'] = $user_rank;
        $site = M('System')->select();
        foreach ($site as $k => $v) {
            $siteConfig[$v['name']] = json_decode($v['data'], true);
        }
        $array['siteConfig'] = $siteConfig;
        $this->config = $array;
        return $array;
    }

    //获取菜单
    protected function getNav($id = 0)
    {
        $M = M('Nav');
        $array = $M->where(array('pid' => $id))->order('sort ASC')->select();
        if ($array) {
            foreach ($array as $key => $val) {
                $val['list'] = $this->getNav($val['id']);
                $arr[] = $val;
            }
            return $arr;
        }
    }

    //获取分类
    protected function getGoodsCategory($id = 0)
    {
        $M = M('GoodsCategory');
        $array = $M->where(array('pid' => $id))->order('sort ASC')->select();
        if ($array) {
            foreach ($array as $key => $val) {
                $val['list'] = $this->getGoodsCategory($val['id']);
                $arr[] = $val;
            }
            return $arr;
        }
    }

    protected function is_mobile()
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $mobile_agents = Array("240x320", "acer", "acoon", "acs-", "abacho", "ahong", "airness", "alcatel", "amoi", "android", "anywhereyougo.com", "applewebkit/525", "applewebkit/532", "asus", "audio", "au-mic", "avantogo", "becker", "benq", "bilbo", "bird", "blackberry", "blazer", "bleu", "cdm-", "compal", "coolpad", "danger", "dbtel", "dopod", "elaine", "eric", "etouch", "fly ", "fly_", "fly-", "go.web", "goodaccess", "gradiente", "grundig", "haier", "hedy", "hitachi", "htc", "huawei", "hutchison", "inno", "ipad", "ipaq", "ipod", "jbrowser", "kddi", "kgt", "kwc", "lenovo", "lg ", "lg2", "lg3", "lg4", "lg5", "lg7", "lg8", "lg9", "lg-", "lge-", "lge9", "longcos", "maemo", "mercator", "meridian", "micromax", "midp", "mini", "mitsu", "mmm", "mmp", "mobi", "mot-", "moto", "nec-", "netfront", "newgen", "nexian", "nf-browser", "nintendo", "nitro", "nokia", "nook", "novarra", "obigo", "palm", "panasonic", "pantech", "philips", "phone", "pg-", "playstation", "pocket", "pt-", "qc-", "qtek", "rover", "sagem", "sama", "samu", "sanyo", "samsung", "sch-", "scooter", "sec-", "sendo", "sgh-", "sharp", "siemens", "sie-", "softbank", "sony", "spice", "sprint", "spv", "symbian", "tablet", "talkabout", "tcl-", "teleca", "telit", "tianyu", "tim-", "toshiba", "tsm", "up.browser", "utec", "utstar", "verykool", "virgin", "vk-", "voda", "voxtel", "vx", "wap", "wellco", "wig browser", "wii", "windows ce", "wireless", "xda", "xde", "zte");
        $is_mobile = false;
        foreach ($mobile_agents as $device) {
            if (stristr($user_agent, $device)) {
                $is_mobile = true;
                break;
            }
        }
        return $is_mobile;
    }

    //支付功能的所有计算开始 $order['type'] 1 普通订单 2充值订单 3积分订单 4VIP订单
    public function ordersave($out_trade_no, $trade_no, $time)
    {
        $order = D('Orders')->where(array('ordernum' => $out_trade_no, 'pay_status' => 0))->find();
        $order_aa = D('Orders')->where(array('ordernum' => $out_trade_no))->find();
        if ($order_aa['is_dividend_status'] == 1) {
            return true;
        }
        if ($order['type'] == 1) {
            return $this->ordersGoods($out_trade_no, $trade_no, $time, $order);
        } elseif ($order['type'] == 2) {
            return $this->ordersRecharge($out_trade_no, $trade_no, $time, $order);
        }
    }

    public function ordersGoods($out_trade_no, $trade_no, $time, $order)
    {
        $U = M('User');
        $G = M('Goods');
        $user = $U->where(array('id' => $order['user_id']))->find();
//        $this->saveScore($user, $order);
        $orderGoods = D('OrderGoods')->where(array('order_id' => $order['id']))->select();
        //修改库存及销量
        foreach ($orderGoods as $key => $val) {
            $G->where(array('id' => $val['goodsId']))->setDec('warn_number', $val['number']);
            $G->where(array('id' => $val['goodsId']))->setInc('salesnum', $val['number']);
        }
        $order_aa = D('Orders')->where(array('ordernum' => $out_trade_no, 'is_dividend_status' => 0))->find();
        D('Orders')->where(array('id' => $order_aa['id']))->save(array('is_dividend_status' => 1));
        if ($order['is_dividend_status'] == 0 && $this->config['siteConfig']['popularize']['popularize_switch']) {
            $this->referrer_money($user, $order_aa); //分销反点
        }
        //修改订单付款状态
        $arr = array(
            'pay_status' => 1,
            'pay_ordernum' => $trade_no,
            'paytime' => $time,
        );

        $U->where(array('id' => $order['user_id']))->setInc("order_amount",$order['ordersize']);
        if($order['integral_size'] > 0) {
            $U->where(array('id' => $order['user_id']))->setDec("score", $order['integral_size']);
            M("UserScoreLog")->add(array(
                'user_id' => $order['user_id'],
                'type' => 4,
                'num' => $order['integral_size'],
                'time' => time()
            ));
        }
        if (D('Orders')->where(array('ordernum' => $out_trade_no, 'pay_status' => 0))->save($arr)) {
            $str = '您的订单号为：' . $order['ordernum'] . '的订单，已经成功付款，我们会尽快安排人员发货';
            setUserMessage($order['user_id'], $str);
            $mmm = $str . " \n <a href=\"http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Order/orderInfo', array('id' => $order['id'])) . '">查看详情</a>';
            setUserWeixinMessage($order['user_id'], $mmm);
            setAdminWeixinMessage("订单号为 : " . $order['ordernum'] . "，已经成功付款，请前往发货");
            return true;
        }
        return false;
    }

    private $referrer_u;
    private $referrer_ur_1;

    //计算推广提成
    public function referrer_money($user, $order)
    {
        $orderGoods = D('OrderGoods')->where(array('order_id' => $order['id']))->select();
        $scaleArr = $this->config['siteConfig']['popularize']['scale'];
//        $user_rank = M("UserRank")->find();
        foreach ($orderGoods as $key => $val) {
            $goods = D('Goods')->where(array('id' => $val['goodsId']))->find();
            $this->referrer_u = $this->referrer_ur_1 = $user['referrer_u'];
            /*
             * 按固定分销
             * */
            /*foreach ($scaleArr as $k => $v) {
                if($v['scale_a'] <= 0) continue;
                $user_r = M("User")->where(array('id' => $this->referrer_u))->find();

                $tic = $v['scale_a'];
                $tic = sprintf("%.2f", substr(sprintf("%.4f", $tic), 0, -2));
                if ($this->referrer_u > 0) {
                    $this->setUserR($this->referrer_u, $tic, $order['id'], $user['id'], $val['id']);
                    $this->referrer_u = $user_r['referrer_u'];
                }else
                    $this->referrer_u = 0;
            }*/

            $Zp = $goods['fx_ratio'] * $val['zPrice'] / 100;
          	if($Zp > 0){
              $i = 1;
              foreach ($scaleArr as $k=>$v){
                  if($v['scale_a'] <= 0) continue;
                  $user_r = M("User")->where(array('id' => $this->referrer_u))->find();
                  $scale = $v['scale_a'] / 100;
                  $tic = $Zp * $scale;
                  $tic = sprintf("%.2f",substr(sprintf("%.4f",$tic), 0, -2));
                  if ($this->referrer_u > 0) {
                      $this->setUserR($this->referrer_u,$tic,$order['id'],$user['id'],$val['id']);
                      $this->referrer_u = $user_r['referrer_u'];
                  }else
                      $this->referrer_u = 0;
                  $i++;
              }
            }
            //赠送积分
            if($goods['money_r'] > 0){
                $score = $val['number'] * $goods['money_r'];
                M("User")->where(array('id'=>$user['id']))->setInc("score",$score);
                M("User")->where(array('id'=>$user['id']))->setInc("score_l",$score);
                M("UserScoreLog")->add(array(
                    'user_id' => $user['id'],
                    'type' => 1,
                    'num' => $score,
                    'time' => time()
                ));
            }
        }
    }

    public function setUserB($uid, $tic, $oid, $auid, $order_goods_id)
    {
        $U = M('ReferrerLog');
        $arr = array(
            'oid' => $oid,
            'money' => $tic,
            'uid' => $uid,
            'auid' => $auid,
            'status' => 2,
            'order_goods_id' => $order_goods_id,
            'time' => time()
        );
        $user_1 = M("User")->where(array('id' => $auid))->find();
        $str = "您的好友 " . $user_1['nickname'] . " 下单付款成功，本订单您将获得￥" . $tic . "元的分成奖励，待确认不退货后，自动转入您的账号";
        setUserWeixinMessage($uid, $str);
        setUserMessage($uid, $str);
        $U->add($arr);
    }

    public function setUserR($uid, $tic, $oid, $auid, $order_goods_id)
    {
        $U = M('ReferrerLog');
        $arr = array(
            'oid' => $oid,
            'money' => $tic,
            'uid' => $uid,
            'auid' => $auid,
            'status' => 2,
            'order_goods_id' => $order_goods_id,
            'time' => time()
        );
        $user_1 = M("User")->where(array('id' => $auid))->find();
        $str = "您的好友 " . $user_1['nickname'] . " 下单付款成功，本订单您将获得￥" . $tic . "元的分成奖励，待确认不退货后，自动转入您的账号";
        setUserWeixinMessage($uid, $str);
        setUserMessage($uid, $str);
        $U->add($arr);
    }

    public function ordersRecharge($out_trade_no, $trade_no, $time, $order)
    {
        $U = M('User');
        $user = $U->where(array('id' => $order['user_id']))->find();
        $arr = array(
            'order_status' => 1,
            'pay_status' => 1,
            'shipping_status' => 2,
            'pay_ordernum' => $trade_no,
            'paytime' => $time,
        );
        $scoreA['user_money'] = $user['user_money'] + $order['ordersize'];
        $payment = M('Payment')->where(array('id' => $order['pay_type']))->find();
        if ($U->where(array('id' => $order['user_id']))->save($scoreA)) {
            if (D('Orders')->where(array('ordernum' => $out_trade_no))->save($arr)) {
                addUserAccount($user['username'], $order['ordersize'], 1, '用户自助充值，订单编号：' . $out_trade_no, $payment['cname'], time());
                $str = '您的订单号为：' . $order['ordernum'] . '的充值订单，已经成功充值，如有疑问请联系客服';
                setUserMessage($order['user_id'], $str);
                $mmm = $str . " \n <a href=\"http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Order/orderInfo', array('id' => $order['id'])) . '">查看详情</a>';
                setUserWeixinMessage($order['user_id'], $mmm);
                return true;
            }
            return false;
        };
        return false;
    }
}