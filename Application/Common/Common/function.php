<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

/**
 * +----------------------------------------------------------
 * 功能：添加用户资金记录
 * +----------------------------------------------------------
 */
function addUserAccount($user, $money, $type ,$admin_note,$payment,$paid_time) {
	$arr = array(
			'user_id' => $user,
			'admin_user' => 'admin',
			'money' => $money,
			'admin_note' => $admin_note,
			'payment' => $payment,
			'add_time' => $paid_time,
			'type' => $type,
			'paid_time' => $paid_time,
			'is_paid' => 1
	);
	M("UserAccount")->add($arr);
}

/**
 * +----------------------------------------------------------
 * 功能：计算文件大小
 * +----------------------------------------------------------
 *
 * @param int $bytes
 *        	+----------------------------------------------------------
 * @return string 转换后的字符串
 *         +----------------------------------------------------------
 */
function byteFormat($bytes) {
	$sizetext = array (
			" B",
			" KB",
			" MB",
			" GB",
			" TB",
			" PB",
			" EB",
			" ZB",
			" YB" 
	);
	return round ( $bytes / pow ( 1024, ($i = floor ( log ( $bytes, 1024 ) )) ), 2 ) . $sizetext [$i];
}
/**
 * +----------------------------------------------------------
 * 功能：系统邮件发送函数
 * +----------------------------------------------------------
 *
 * @param string $to
 *        	接收邮件者邮箱
 * @param string $name
 *        	接收邮件者名称
 * @param string $subject
 *        	邮件主题
 * @param string $body
 *        	邮件内容
 * @param string $attachment
 *        	附件列表
 *        	+----------------------------------------------------------
 * @return boolean +----------------------------------------------------------
 */
function send_mail($to, $name, $subject = '', $body = '', $attachment = null, $config = '') {
	if (! $config) {
		$array = M ( 'System' )->where ( array (
				'name' => 'email' 
		) )->find ();
		$config = json_decode ( $array ['data'], true );
	}
	// vendor('PHPMailer.phpmailer'); //从PHPMailer目录导class.phpmailer.php类文件
	// $mail = new \PHPMailer();
	$mail = new \NewsLib\PHPMailer\PHPMailer (); // PHPMailer对象
	$mail->CharSet = 'UTF-8'; // 设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
	$mail->IsSMTP (); // 设定使用SMTP服务
	                  // $mail->IsHTML(true);
	$mail->SMTPDebug = 0; // 关闭SMTP调试功能 1 = errors and messages2 = messages only
	$mail->SMTPAuth = true; // 启用 SMTP 验证功能
	if ($config ['smtp_port'] == 465)
		$mail->SMTPSecure = 'ssl'; // 使用安全协议
	$mail->Host = $config ['smtp_host']; // SMTP 服务器
	$mail->Port = $config ['smtp_port']; // SMTP服务器的端口号
	$mail->Username = $config ['smtp_user']; // SMTP服务器用户名
	$mail->Password = $config ['smtp_pass']; // SMTP服务器密码
	$mail->SetFrom ( $config ['from_email'], $config ['from_name'] );
	$replyEmail = $config ['reply_email'] ? $config ['reply_email'] : $config ['reply_email'];
	$replyName = $config ['reply_name'] ? $config ['reply_name'] : $config ['reply_name'];
	$mail->AddReplyTo ( $replyEmail, $replyName );
	$mail->Subject = $subject;
	$mail->MsgHTML ( $body );
	$mail->AddAddress ( $to, $name );
	if (is_array ( $attachment )) { // 添加附件
		foreach ( $attachment as $file ) {
			if (is_array ( $file )) {
				is_file ( $file ['path'] ) && $mail->AddAttachment ( $file ['path'], $file ['name'] );
			} else {
				is_file ( $file ) && $mail->AddAttachment ( $file );
			}
		}
	} else {
		is_file ( $attachment ) && $mail->AddAttachment ( $attachment );
	}
	return $mail->Send () ? true : $mail->ErrorInfo;
}
/**
 * +----------------------------------------------------------
 * 功能：系统短信发送函数
 * +----------------------------------------------------------
 *
 * @param string $to
 *        	接收短信者
 * @param string $content
 *        	短信内容
 * @param string $config
 *        	配置
 *        	+----------------------------------------------------------
 * @return boolean +----------------------------------------------------------
 */
function send_sms($to, $content, $config = '', $tpl_id) {
	if (! $config) {
		$array = M ( 'System' )->where ( array (
				'name' => 'sms' 
		) )->find ();
		$config = json_decode ( $array ['data'], true );
	}
    $SMS = "\\NewsLib\\".$config['sms_type'];
    if($config['sms_type'] == 'SmsAliyun'){
        $content = array('code' => $content);
    }
    if($config['sms_type'] == 'Smsjh'){
        $content = "#code#=" . $content;
    }
	$ab = new $SMS ( $to, $content, $config, $tpl_id, '', '', '');
	return $ab->ret;
}

/**
 * +----------------------------------------------------------
 * 功能：检测一个目录是否存在，不存在则创建它
 * +----------------------------------------------------------
 *
 * @param string $path
 *        	待检测的目录
 *        	+----------------------------------------------------------
 * @return boolean +----------------------------------------------------------
 */
function makeDir($path) {
	return is_dir ( $path ) or (makeDir ( dirname ( $path ) ) and @mkdir ( $path, 0777 ));
}

/**
 * +----------------------------------------------------------
 * 生成随机字符串
 * +----------------------------------------------------------
 *
 * @param int $length
 *        	要生成的随机字符串长度
 * @param string $type
 *        	随机码类型：0，数字+大写字母；1，数字；2，小写字母；3，大写字母；4，特殊字符；-1，数字+大小写字母+特殊字符
 *        	+----------------------------------------------------------
 * @return string +----------------------------------------------------------
 */
function randCode($length = 5, $type = 0) {
	$arr = array (
			1 => "0123456789",
			2 => "abcdefghijklmnopqrstuvwxyz",
			3 => "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
			4 => "~@#$%^&*(){}[]|" 
	);
	if ($type == 0) {
		array_pop ( $arr );
		$string = implode ( "", $arr );
	} else if ($type == "-1") {
		$string = implode ( "", $arr );
	} else {
		$string = $arr [$type];
	}
	$count = strlen ( $string ) - 1;
	for($i = 0; $i < $length; $i ++) {
		$str [$i] = $string [rand ( 0, $count )];
		$code .= $str [$i];
	}
	return $code;
}

/**
 * +-----------------------------------------------------------------------------------------
 * 删除目录及目录下所有文件或删除指定文件
 * +-----------------------------------------------------------------------------------------
 *
 * @param str $path
 *        	待删除目录路径
 * @param int $delDir
 *        	是否删除目录，1或true删除目录，0或false则只删除文件保留目录（包含子目录）
 *        	+-----------------------------------------------------------------------------------------
 * @return bool 返回删除状态
 *         +-----------------------------------------------------------------------------------------
 */
function delDirAndFile($path, $delDir = FALSE) {
	$handle = opendir ( $path );
	if ($handle) {
		while ( false !== ($item = readdir ( $handle )) ) {
			if ($item != "." && $item != "..")
				is_dir ( "$path/$item" ) ? delDirAndFile ( "$path/$item", $delDir ) : unlink ( "$path/$item" );
		}
		closedir ( $handle );
		if ($delDir)
			return rmdir ( $path );
	} else {
		if (file_exists ( $path )) {
			return unlink ( $path );
		} else {
			return FALSE;
		}
	}
}
/**
 * ***********
 * 获取广告位数据
 */
function ADinvoke($id, $num = 10) {
	$AD = M ( 'Ad' );
	$adposition = $AD->where ( array (
			'id' => $id 
	) )->find ();
	$map = array (
			'adpositionid' => array (
					'eq',
					$id 
			),
			'startdate' => array (
					'elt',
					time () 
			),
			'enddate' => array (
					'egt',
					time () 
			),
			'status' => 1 
	);
	$AdContent = M ( 'AdContent' );
	$adposition ['content'] = $AdContent->where ( $map )->order ( "sort ASC" )->limit ( $num )->select ();
	$adStatistics = M ( 'AdStatistics' );
	$date = strtotime ( date ( "Y-m-d", time () ) );
	foreach ( $adposition ['content'] as $k => $v ) {
		$sta = $adStatistics->where ( array (
				'time' => $date,
				'ad_content_id' => $v ['id'] 
		) )->find ();
		if ($sta != '') {
			$adStatistics->where ( array (
					'time' => $date,
					'ad_content_id' => $v ['id'] 
			) )->setInc ( 'shownum' );
		} else {
			$data = array (
					'shownum' => 1,
					'ad_content_id' => $v ['id'],
					'time' => $date 
			);
			$adStatistics->add ( $data );
		}
		$adposition ['content'] [$k] ['url'] = U ( 'Ad/content', array (
				'id' => $v ['id'] 
		) );
	}
	return $adposition;
}

/**
 * [setPriceRange 设置价格区间]
 *
 * @param [type] $mini
 *        	[最小数]
 * @param [type] $max
 *        	[最大数]
 * @param [type] $diff
 *        	[间隔区间]
 * @param integer $step
 *        	[间隔区间个数]
 */
function setPriceRange($mini, $max, $diff = '', $step = 8) {
	if ($max <= $mini) {
		return null;
	}
	// 取得价格分级最小单位级数，比如，千元商品最小以100为级数
	$price_grade = 0.0001;
	for($i = - 2; $i <= log10 ( $row ['max'] ); $i ++) {
		$price_grade *= 10;
	}
	$diff = ceil ( ($max - $mini) / ($step) / $price_grade ) * $price_grade;
	static $first_price;
	static $last_price;
	$first_price = floor ( $mini );
	for($i = 1; $i <= $step; $i ++) {
		$last_price = floor ( $first_price + $diff );
		// 当循环完,最后一个数值还大于最大值时,取后面所有的
		if ($i == $step) {
			$temp = $first_price . '以上';
			$a = array (
					'price_min' => $first_price,
					'price_max' => 0 
			);
		} else {
			// 当最后一个数值大于最大数值时,跳出循环
			if ($last_price > $max) {
				$temp = $first_price . '以上';
				$a = array (
						'price_min' => $first_price,
						'price_max' => 0 
				);
				continue;
			} else {
				$temp = $first_price . '-' . $last_price;
				$a = array (
						'price_min' => $first_price,
						'price_max' => $last_price 
				);
			}
		}
		$arr [$i] ['map'] = $a;
		$arr [$i] ['str'] = $temp;
		$first_price = $last_price + 1;
	}
	return $arr;
}
function setUserWeixinMessage($userid,$arr) {
	$site = M('System')->select();
	foreach($site as $k => $v){
		$siteConfig[$v['name']] = json_decode($v['data'],true);
	}
	$array['siteConfig'] = $siteConfig;
	$options = $array['siteConfig']['wechat'];
	$we_obj = new \NewsLib\Wechat($options);
	$M = M ( 'User' );
	$user = $M->where(array('id'=>$userid))->find();
	$array = array (
			'touser' => $user['wx_openid'],
			'msgtype' => "text",
			'text' => array(
				'content' => $arr
			),
	);
	$we_obj->sendCustomMessage ( $array );
}
function setAdminWeixinMessage($arr) {
	$site = M('System')->select();
	foreach($site as $k => $v){
		$siteConfig[$v['name']] = json_decode($v['data'],true);
	}
	$array['siteConfig'] = $siteConfig;
	$options = $array['siteConfig']['wechat'];
	$we_obj = new \NewsLib\Wechat($options);
	$M = M ( 'User' );
	$ids_str = $array['siteConfig']['system_basis']['sys_wei_admin'];
	$ids = explode("|",$ids_str);
	$users = $M->where(array('id'=>array('in',$ids)))->select();
	foreach ($users as $key=>$user){
		$array = array (
				'touser' => $user['wx_openid'],
				'msgtype' => "text",
				'text' => array(
						'content' => $arr
				),
		);
		$we_obj->sendCustomMessage ( $array );
	}
}
function setUserMessage($userid,$info) {
	$M = M ( 'UserMessage' );
	$array = array (
			'to_user' => $userid,
			'time' => time (),
			'content' => $info 
	);
	$M->add ( $array );
}
/**
 * [cut_str 字符串截取]
 *
 * @param [type] $string
 *        	[字符串]
 * @param integer $sublen
 *        	[长度]
 * @param integer $start
 *        	[从第几个开始]
 * @param integer $code
 *        	[编码]
 */
function cut_str($string, $sublen, $start = 0, $code = 'UTF-8') {
	if ($code == 'UTF-8') {
		$pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
		preg_match_all ( $pa, $string, $t_string );
		if (count ( $t_string [0] ) - $start > $sublen)
			return join ( '', array_slice ( $t_string [0], $start, $sublen ) );
		return join ( '', array_slice ( $t_string [0], $start, $sublen ) );
	} else {
		$start = $start * 2;
		$sublen = $sublen * 2;
		$strlen = strlen ( $string );
		$tmpstr = '';
		
		for($i = 0; $i < $strlen; $i ++) {
			if ($i >= $start && $i < ($start + $sublen)) {
				if (ord ( substr ( $string, $i, 1 ) ) > 129) {
					$tmpstr .= substr ( $string, $i, 2 );
				} else {
					$tmpstr .= substr ( $string, $i, 1 );
				}
			}
			if (ord ( substr ( $string, $i, 1 ) ) > 129)
				$i ++;
		}
		// if(strlen($tmpstr)< $strlen ) $tmpstr.= "...";
		return $tmpstr;
	}
}
