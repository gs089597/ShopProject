<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
class UserModel extends CommonModel {

	public function adminMd5($password){
		return md5(substr(md5(substr(md5($password),5,20).C("PSD_VERIFY")),2,28).C("PS_ENTERPRISE"));
	}
    public function getConfig(){
        $site = M('System')->select();
        foreach ($site as $k => $v) {
            $siteConfig[$v['name']] = json_decode($v['data'], true);
        }
        $array['siteConfig'] = $siteConfig;
        $this->config = $array;
    }
    public function addUser() {
        $this->getConfig();
        $datas = $_POST['user'];
        $user = D ( "User" )->order("id DESC")->find ();

        if($user['username'] == ''){
            $userid_a = 10000;
        }else{
            $userid_a = $user['username'];
        }
        $datas['username'] = $userid_a + 1;
        $M = M("User");
        $datas['email'] = trim($datas['email']);
		if ($M->where("`username`='" . $datas['username'] . "'")->count() > 0) {
            return array('status' => 0, 'info' => "该用户名已经存在");
        }
		if ($datas['password'] <> $datas['password_1']) {
            return array('status' => 0, 'info' => "两次密码不一致");
        }
		unset($datas['password_1']);
		$datas['portrait'] = '/Public/Images/big_user_0.jpg';
		$datas['salt'] = randCode(5);
        $datas['password'] = $this->adminMd5($datas['password'].$datas['salt']);
        $datas['regdate'] = time();
        $datas['status'] = 1;
        $datas['is_edit_pwd'] = 1;
        /*$u_rank = $this->config['siteConfig']['system_basis']['sys_mr_rank'];
        if($u_rank > 0){
            $user_rank = M("UserRank")->where(array('rank'=> $u_rank))->find();
            $datas['user_rank'] = $user_rank['id'];
        }*/

        if ($M->add($datas)) {
			setAdminLog('添加会员-'.$datas['username']);
            return array('status' => 1, 'info' => "添加成功", 'url' => U("User/index"));
        } else {
            return array('status' => 0, 'info' => "添加新账号失败，请重试");
        }
    }
    public function editUser() {
		$datas = $_POST['user'];
		$datas['id'] = $_POST['id'];
        $M = M("User");
        if (!empty($datas['password'])) {
			if ($datas['password'] <> $datas['password_1']) {
				return array('status' => 0, 'info' => "两次密码不一致");
			}
			$info = $M->where(array('id'=>$datas['id']))->find();
			$datas['password'] = $this->adminMd5($datas['password'].$info['salt']);
        } else {
            unset($datas['password']);
        }
		unset($datas['password_1']);
        if ($M->save($datas)) {
			setAdminLog('编辑会员-'.$datas['username']);
            return array('status' => 1, 'info' => "更新成功", 'url' => U("User/index"));
        } else {
            return array('status' => 0, 'info' => "更新失败");
        }
    }
	
	public function addRank() {
        $M = M("UserRank");
		$M->create($_POST);
		setAdminLog('添加会员等级-'.$_POST['rank_name']);
        if ($M->add()) {
            return array('status' => 1, 'info' => "添加成功", 'url' => U("User/listRank"));
        } else {
            return array('status' => 0, 'info' => "添加失败，请重试");
        }
    }
	public function editRank() {
		$M = M("UserRank");
		setAdminLog('编辑会员等级-'.$_POST['rank_name']);
		$M->create($_POST);
        if ($M->save()) {
            return array('status' => 1, 'info' => "更新成功", 'url' => U("User/listRank"));
        } else {
            return array('status' => 0, 'info' => "更新失败");
        }
    }
	
	public function addAccount() {
        $M = M("UserAccount");
		$data = $_POST;
		$data['admin_user'] = base64_decode(session('username'));
		$data['add_time'] = time();
		if($_POST['is_paid'] <> 0){
			$data['is_paid'] = 0;
		}
		if($data['type'] == 2){
			M('User')->where(array('username'=>$data['user_id']))->setInc('frozen_money',$data['money']); //冻结用户余额
		}
		$M->create($data);
        if ($id = $M->add()) {
			setAdminLog('添加充值提现-申请id:'.$id);
            return array('status' => 1, 'info' => "添加成功，等待审核", 'url' => U("User/account"));
        } else {
            return array('status' => 0, 'info' => "添加失败，请重试");
        }
    }
	public function editAccount() {
		$M = M("UserAccount");
		$data = $_POST;
		$data['admin_user'] = base64_decode(session('username'));
		$M->create($data);
        if ($id = $M->save()) {
			setAdminLog('添加充值提现-申请id:'.$id);
            return array('status' => 1, 'info' => "操作成功", 'url' => U("User/account"));
        } else {
            return array('status' => 0, 'info' => "操作失败");
        }
    }

    public function auditAccount() {
        $M = M("UserAccount");
        $data = $_POST;
        $data['admin_user'] = base64_decode(session('username'));
        $data['paid_time'] = time();
        $account = $M->where(array('id'=>$data['id']))->find();
        $user = M('User')->where(array('username'=>$account['user_id']))->find();
        $M->create($data);
        if($account['type'] == 2 && $data['is_paid'] == 1){
            if($user['user_money'] < $account['money'] || $user['frozen_money'] < $account['money']){
                //$M->where(array('is_paid'=>0,'id'=>$data['id']))->save();
                return array('status' => 0, 'info' => "用户账户中余额不足，无法完成操作");
            }
        }
        if($account['payment'] == "微信提现" && $data['is_paid'] == 1 && $account['type'] == 2){
            return array('status' => 1, 'info' => "马上跳转到微信支付", 'url' => U("Payment/index",array('paytype'=>'wechat','account'=>$account['id'])));
        }
        if ($id = $M->where(array('is_paid'=>0,'id'=>$data['id']))->save()) {
            if($data['is_paid'] == 1){
                if($account['type'] == 1){
                    $str_a = "充值";
                    $str_b = "后台充值";
                    $ss = M('User')->where(array('username'=>$account['user_id']))->setInc('user_money',$account['money']);
                }elseif($account['type'] == 2){
                    $str_a = "提现";
                    $ss = M('User')->where(array('username'=>$account['user_id']))->setDec('user_money',$account['money']);
                    M('User')->where(array('username'=>$account['user_id']))->setDec('frozen_money',$account['money']);
                }elseif($account['type'] == 3){
                    $str_a = "退款";
                    $ss = M('User')->where(array('username'=>$account['user_id']))->setInc('user_money',$account['money']);
                }
                if($ss){
                    $str = '尊敬的'.$user['nickname'].'，您的'.$str_a.$account['money'].'元操作，已经处理完成，请注意查收，如有疑问请联系客服.'.$str_b."客服电话：".$this->siteConfig['siteConfig']['system_basis']['sys_phone'];
                    if($user['phone'] <> "" && $account['type'] == 2){
//						send_sms($user['phone'],$str); //发送提现短信通知
                    }
                    setAdminLog('审核'.$str_a.'申请-申请id:'.$id.'-操作成功,充值或扣款也操作成功');
                    setUserWeixinMessage($user['id'],$str);
                    setUserMessage($user['id'],$str);
                    return array('status' => 1, 'info' => "操作成功，充值或扣款也操作成功", 'url' => U("User/account"));
                }else{
                    $str = '尊敬的'.$user['nickname'].'，您的'.$str_a.$account['money'].'元操作，申请未通过审核，如有疑问请联系客服，客服电话：'.$this->siteConfig['siteConfig']['system_basis']['sys_phone'];
                    setAdminLog('审核'.$str_a.'申请-申请id:'.$id.'-操作成功,充值或扣款没有成功');
                    setUserWeixinMessage($user['id'],$str);
                    setUserMessage($user['id'],$str);
                    return array('status' => 1, 'info' => "操作成功，充值或扣款没有成功", 'url' => U("User/account"));
                }
            }
            if($data['is_paid'] == 2){
                setAdminLog('审核充值提现申请-申请id:'.$id.'-成功取消了申请');
                if($account['type'] == 2){
                    M('User')->where(array('username'=>$account['user_id']))->setDec('frozen_money',$account['money']);
                }
                return array('status' => 1, 'info' => "成功取消了申请", 'url' => U("User/account"));
            }
        } else {
            return array('status' => 0, 'info' => "这个申请已经审核过了哦");
        }
    }
}

?>
