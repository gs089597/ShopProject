<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model\RelationModel;
class CommonModel extends RelationModel {
	public function _initialize() {
		$this->configArr();	
	}
	//初始化
	public function configArr(){
		$site = M('System')->select();
		foreach($site as $k => $v){
			$siteConfig[$v['name']] = json_decode($v['data'],true);
		}
		$array['siteConfig'] = $siteConfig;	
		$this->siteConfig = $array;
	}
	public function getCity(){
		$cat = new \Org\Util\Category('City', array('var', 'mid', 'city'));	
		return $cat->getList();
	}
}

?>
