<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class NewsModel extends CommonModel {
	protected $_link = array(        
		'NewsA'=>array(            
			'mapping_type'      => self::HAS_ONE,            
			'class_name'        => 'NewsA', 
			'foreign_key'       => 'id'    
	    ), 
		'NewsCategory'=>array(
			'mapping_type'      => self::BELONGS_TO,            
			'class_name'        => 'NewsCategory', 
			'foreign_key'       => 'category'  
		)   
	 );
	//添加新闻
	public function addNews(){
		$M = M("News");
		$MA = M("NewsA");
		$_POST['news']['addtime'] = time();
		$goods = $_POST['news'];
		$goodsa = $_POST['newsa'];
        if($id = $M->add($goods)){
			//添加附表信息
			$goodsa['id'] = $id;
			setAdminLog('添加文章-'.$goods['title']);
			return $MA->add($goodsa) ? array('status' => 1, 'id'=> $id, 'info' => '添加成功') : array('status' => 0, 'info' => '添加失败');	
		}else{
			return array('status' => 0, 'info' => '添加失败');
		} 
	}
	
	//编辑新闻
	public function editNews(){
		$M = M("News");
		$MA = M("NewsA");
		(int)$id = $_POST['id'];
		$goods = $_POST['news'];
		$goods['id'] = $id; 
		$goodsa = $_POST['newsa'];
		$info = D("News")->where(array('id'=>$id))->find();
		setAdminLog('编辑文章-'.$goods['title']);
        $a = $M->save($goods) ?　1 : 0;
		//编辑附表
		//$goodsa['id'] = $id;
		$b = $MA->where(array('id'=>$id))->save($goodsa) ? 1 : 0;
		return array('status' => 1, 'info' => '操作成功');
	}
	//分类列表
	public function getCategory() {
        $cat = new \Org\Util\Category('NewsCategory', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        return $list;
    }
	
	//添加分类
	public function addCategory() {
        $M = M("NewsCategory");
		setAdminLog('添加文章分类-'.$_POST['name']);
		$M->create($_POST);
        return $M->add() ? array('status' => 1, 'info' => '添加成功', 'url' => U('News/listCategory')) : array('status' => 0, 'info' => '添加失败');
    }
	
	//编辑分类
	public function editCategory() {
        $M = M("NewsCategory");
		setAdminLog('编辑文章分类-'.$_POST['name']);
		$M->create($_POST);
        return $M->save() ? array('status' => 1, 'info' => '更新成功', 'url' => U('News/listCategory')) : array('status' => 0, 'info' => '更新失败');
    }
}

?>
