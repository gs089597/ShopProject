<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class GoodsModel extends CommonModel {
    protected $_link = array(
        'GoodsA'=>array(
            'mapping_type'      => self::HAS_ONE,
            'class_name'        => 'GoodsA',
            'foreign_key'       => 'id'
        ),
        'GoodsAttr'=>array(
            'mapping_type'      => self::HAS_MANY,
            'class_name'        => 'GoodsAttr',
            'foreign_key'       => 'goods_id'
        ),
        'Supplier'=>array(
            'mapping_type'      => self::BELONGS_TO,
            'class_name'        => 'Supplier',
            'foreign_key'       => 'id',
            'parent_key' 		=> 'supplier'
        ),
    );
    //添加商品
    public function addGoods(){
        $M = M("Goods");
        $MA = M("GoodsA");

        $goods = $_POST['goods'];
        $goodsa = $_POST['goodsa'];
        $goods['promotion_startime'] = strtotime($_POST['goods']['promotion_startime']);
        $goods['promotion_endtime'] = strtotime($_POST['goods']['promotion_endtime']);
        $goods['member_fee'] = serialize($_POST['member_fee']);
        setAdminLog('添加商品-'.$goods['title']);

        $photos = array();
        foreach($_POST['photo'] as $key=>$val){
            $photos[$key]['photo'] = $_POST['photo'][$key];
            $photos[$key]['photo_thumb'] = $_POST['photo_thumb'][$key];
        }
        $goods['photos'] = json_encode($photos);

        if($id = $M->add($goods)){
            //添加附表信息
            $goodsa['id'] = $id;
            $goodsAttr = array();
            for($key=0;$key<200;$key++){
                if($_POST['GoodsTypeAttrArr_'.$key]){
                    $goodsAttr[$key]['goods_id'] = $id;
                    $goodsAttr[$key]['attr_id'] = $_POST['GoodsTypeIdArr_'.$key];
                    $goodsAttr[$key]['attr_value'] = $_POST['GoodsTypeAttrArr_'.$key];
                    if($_POST['GoodsTypePriceArr_'.$key]){
                        $goodsAttr[$key]['attr_price'] = $_POST['GoodsTypePriceArr_'.$key];
                    }
                }else{
                    continue;
                }
            }

            M('GoodsAttr')->addAll($goodsAttr);

            return $MA->add($goodsa) ? array('status' => 1, 'id'=> $id, 'info' => '添加成功') : array('status' => 0, 'info' => '添加失败');
        }else{
            return array('status' => 0, 'info' => '添加失败');
        }
    }

    public function editGoods(){
        $M = M("Goods");
        $MA = M("GoodsA");
        (int)$id = $_POST['id'];
        $goods = $_POST['goods'];
        $goods['promotion_startime'] = strtotime($_POST['goods']['promotion_startime']);
        $goods['promotion_endtime'] = strtotime($_POST['goods']['promotion_endtime']);
        $goods['id'] = $id;
        $goods['member_fee'] = serialize($_POST['member_fee']);
        $goodsa = $_POST['goodsa'];
        setAdminLog('编辑商品-'.$goods['title']);
        $info = D("Goods")->where(array('id'=>$id))->find();
        $photos = array();
        foreach($_POST['photo'] as $key=>$val){
            $photos[$key]['photo'] = $_POST['photo'][$key];
            $photos[$key]['photo_thumb'] = $_POST['photo_thumb'][$key];
        }
        $goods['photos'] = json_encode($photos);

        $a = $M->save($goods) ?　1 : 0;
        //编辑附表
        $goodsa['id'] = $id;
        $b = $MA->save($goodsa) ? 1 : 0;
        $goodsAttr = array();
        for($key=0;$key<200;$key++){
            if($_POST['GoodsTypeAttrArr_'.$key]){
                $goodsAttr[$key]['goods_id'] = $id;
                $goodsAttr[$key]['attr_id'] = $_POST['GoodsTypeIdArr_'.$key];
                $goodsAttr[$key]['attr_value'] = $_POST['GoodsTypeAttrArr_'.$key];
                if($_POST['GoodsTypePriceArr_'.$key] <> ''){
                    $goodsAttr[$key]['attr_price'] = $_POST['GoodsTypePriceArr_'.$key];
                }
                if($_POST['GoodsAttrIdArr_'.$key]){
                    $goodsAttr[$key]['goods_attr_id'] = $_POST['GoodsAttrIdArr_'.$key];
                }
            }else{
                continue;
            }
        }
        if($goods['goods_type'] == $info['goods_type']){
            foreach($goodsAttr as $k=>$val){
                if($val['goods_attr_id'] <> ''){
                    M('GoodsAttr')->where(array('goods_attr_id'=>$val['goods_attr_id']))->save($goodsAttr[$k]);
                    if($val['attr_value'] == ''){
                        M('GoodsAttr')->where(array('goods_attr_id'=>$val['goods_attr_id']))->delete();
                    }
                }else{
                    M('GoodsAttr')->add($goodsAttr[$k]);
                }
            }
        }else{
            foreach($goodsAttr as $k=>$val){
                if($val['attr_value'] == ''){
                    unset($goodsAttr[$k]);
                }
            }
            M('GoodsAttr')->where("`goods_id`=".$info['id'])->delete();
            M('GoodsAttr')->addAll($goodsAttr);
        }
        return array('status' => 1, 'info' => '操作成功');
    }

    //添加商品类型
    public function addGoodsType() {
        $M = M("GoodsType");
        setAdminLog('添加商品类型-'.$_POST['name']);
        $M->create($_POST);
        return $M->add() ? array('status' => 1, 'info' => '添加成功') : array('status' => 0, 'info' => '添加失败');
    }

    //编辑商品类型
    public function editGoodsType() {
        $M = M("GoodsType");
        setAdminLog('编辑商品类型-'.$_POST['name']);
        $M->create($_POST);
        return $M->save() ? array('status' => 1, 'info' => '更新成功') : array('status' => 0, 'info' => '更新失败');
    }

    //分类列表
    public function getCategory() {
        $cat = new \Org\Util\Category('GoodsCategory', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        return $list;
    }

    //添加分类
    public function addCategory() {
        $M = M("GoodsCategory");
        setAdminLog('添加商品分类-'.$_POST['name']);
        if(count($_POST['types_id'] > 0)){
            $_POST['types_id'] = implode(',', $_POST['types_id']);
        }
        $M->create($_POST);
        return $M->add() ? array('status' => 1, 'info' => '添加成功') : array('status' => 0, 'info' => '添加失败');
    }

    //编辑分类
    public function editCategory() {
        $M = M("GoodsCategory");
        setAdminLog('编辑商品分类-'.$_POST['name']);
        if(count($_POST['types_id'] > 0)){
            $_POST['types_id'] = implode(',', $_POST['types_id']);
        }
        $M->create($_POST);
        return $M->save() ? array('status' => 1, 'info' => '更新成功') : array('status' => 0, 'info' => '更新失败');
    }

    //添加品牌
    public function addBrand() {
        $M = M("GoodsBrand");
        setAdminLog('添加商品品牌-'.$_POST['name']);
        $M->create($_POST);
        return $M->add() ? array('status' => 1, 'info' => '添加成功', 'url' => U('Goods/addBrand')) : array('status' => 0, 'info' => '添加失败');
    }

    //编辑品牌
    public function editBrand() {
        $M = M("GoodsBrand");
        if($_POST['logo'] != ''){
        }else{
            unset($_POST['logo']);
        }
        unset($_POST['logo_old']);
        setAdminLog('编辑商品品牌-'.$_POST['name']);
        $M->create($_POST);
        return $M->save() ? array('status' => 1, 'info' => '更新成功', 'url' => U('Goods/listBrand')) : array('status' => 0, 'info' => '更新失败');
    }
}

?>
