<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class PublicModel extends CommonModel {
    protected $_validate = array(
		array('email','email','email有误！'),
		array('pwd','require','密码必须填写！'),
		array('verify','require','验证码错误！'),
	);
	protected $_auto = array ( 
		array('pwd','adminMd5',3,'callback',1), 
	);
	
	public function adminMd5($password){
		return md5(substr(md5(substr(md5($password),5,30).C("PSD_VERIFY")),2,25).C("PS_ENTERPRISE"));
	}
	public function login(){
		$datas = $_POST;
        if (!$this->check_verify($datas['verify'])) {
			return array('status' => 0, 'info' => "验证码错误！");
        }
		$M = M("Admin");
		$M->create($datas);
        if ($M->where("`email`='" . $datas['email'] . "'")->count() >= 1) {
            $info = $M->where("`email`='" . $datas["email"] . "'")->find();
            if ($info['status'] == 0) {
                return array('status' => 0, 'info' => "你的账号被禁用，有疑问联系管理员吧");
            }
            if ($info['pwd'] == $this->adminMd5($datas['pwd'])) {
				$session = array(
					'uid' => $info['aid'],
					'email' => $info['email'],
					'usershell' => md5(md5($info['aid'].$info['email']).$info['pwd']),
				);
				session("uid",$session['uid']);
				session("username",base64_encode($session['email']));
				session("usershell",$session['usershell']);
                return array('status' => 1, 'info' => "登录成功", 'url' => U("Index/index"));
            } else {
                return array('status' => 0, 'info' => "账号或密码错误");
            }
        } else {
            return array('status' => 0, 'info' => "不存在邮箱为：" . $datas["email"] . '的管理员账号！');
        }
	}
	public function check_verify($code, $id = ''){    
		$verify = new \Think\Verify();    
		return $verify->check($code, $id);
	}
}

?>
