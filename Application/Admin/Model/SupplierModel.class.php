<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class SupplierModel extends CommonModel {
	public function addSupplier(){
		$M = M("Supplier");
		$goods = $_POST['news'];
        if($id = $M->add($goods)){
			setAdminLog('添加供货商-'.$goods['name']);
			return array('status' => 1, 'info' => '添加成功');	
		}else{
			return array('status' => 0, 'info' => '添加失败');
		} 
	}

	public function editSupplier(){
		$M = M("Supplier");
		$goods = $_POST['news'];
		setAdminLog('编辑供货商-'.$goods['name']);
        return $M->where(array('id'=>$_POST['id']))->save($goods) ? array('status' => 1, 'info' => '更新成功') : array('status' => 0, 'info' => '更新失败');
	}
}

?>
