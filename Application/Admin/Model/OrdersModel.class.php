<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class OrdersModel extends CommonModel {
	protected $_link = array(        
		'UserAddress'=>array(            
			'mapping_type'      => self::BELONGS_TO,            
			'class_name'        => 'UserAddress', 
			'foreign_key'       => 'address',
	    ), 
		'Shipping'=>array(            
			'mapping_type'      => self::BELONGS_TO,            
			'class_name'        => 'Shipping', 
			'foreign_key'       => 'shipping',
	    ), 
		'Payment'=>array(            
			'mapping_type'      => self::BELONGS_TO,            
			'class_name'        => 'Payment', 
			'foreign_key'       => 'pay_type',
	    ),
		'OrderGoods'=>array(
				'mapping_type'      => self::HAS_MANY,
				'class_name'        => 'OrderGoods',
				'foreign_key'       => 'id',
				'parent_key'       => 'order_id',
		),
	 );
	
}

?>
