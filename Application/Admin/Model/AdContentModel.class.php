<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class AdContentModel extends CommonModel {
	protected $_link = array(        
		'Ad'=>array(            
			'mapping_type'      => self::BELONGS_TO,            
			'class_name'        => 'Ad', 
			'foreign_key'       => 'adpositionid'    
	    ),    
	 );
	//添加广告位
	public function addAdPositionId() {
        $M = M("Ad");
		setAdminLog('添加广告位-'.$_POST['name']);
		$M->create($_POST);
        return $M->add() ? array('status' => 1,'url'=>U('Ad/index'), 'info' => '添加成功') : array('status' => 0, 'info' => '添加失败');
    }
	
	//编辑广告位
	public function editAdPositionId() {
        $M = M("Ad");
		$M->create($_POST);
		setAdminLog('编辑广告位-'.$_POST['name']);
        return $M->save() ? array('status' => 1, 'info' => '更新成功') : array('status' => 0, 'info' => '更新失败');
    }
	
	//添加广告
	public function addAd(){
		$M = M("AdContent");
		$_POST['startdate'] = strtotime($_POST['startdate']);
		$_POST['enddate'] = strtotime($_POST['enddate']);
		$_POST['addtime'] = time();
		$_POST['status'] = 1;
		setAdminLog('添加广告-'.$_POST['name']);
		$M->create($_POST);
        return $M->add() ? array('status' => 1,'url'=>U('Ad/adList'), 'info' => '添加成功') : array('status' => 0, 'info' => '添加失败');
	}
	//编辑广告
	public function editAd(){
		$M = M("AdContent");
		$_POST['startdate'] = strtotime($_POST['startdate']);
		$_POST['enddate'] = strtotime($_POST['enddate']);
		setAdminLog('编辑广告-'.$_POST['name']);
		$M->create($_POST);
        return $M->save() ? array('status' => 1, 'info' => '更新成功') : array('status' => 0, 'info' => '更新失败');
	}
}

?>
