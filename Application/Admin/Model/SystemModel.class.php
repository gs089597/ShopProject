<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class SystemModel extends CommonModel {
    public function addData() {
        $M = M('System');
		$datas = array(
			'name' => $_POST['name'],
			'data' => json_encode($_POST['data'])
		);
		
		setAdminLog('修改系统配置');
		if($info = $M->where(array('name'=>$datas['name']))->find()){
			if ($M->where(array('name'=>$datas['name']))->save($datas)) {				
				return array('status' => 1, 'info' => "修改成功");
			} else {
				return array('status' => 0, 'info' => "修改失败");
			}
		}else{
			if ($M->add($datas)) {
				return array('status' => 1, 'info' => "添加成功");
			} else {
				return array('status' => 0, 'info' => "添加失败");
			}	
		}
    }
	
	public function addPayment() {
        $M = M('Payment');
		$datas = array(
			'cname' => $_POST['cname'],
			'name' => $_POST['name'],
			'config' => json_encode($_POST['data'])
		);
		
		if($M->where(array('name'=>$datas['name']))->find()){
			if ($M->where(array('name'=>$datas['name']))->save($datas)) {
				setAdminLog('修改支付方式-'.$datas['name']);
				return array('status' => 1, 'info' => "修改成功");
			} else {
				return array('status' => 0, 'info' => "修改失败");
			}
		}else{
			if ($M->add($datas)) {
				setAdminLog('添加支付方式-'.$datas['name']);
				return array('status' => 1, 'info' => "安装成功");
			} else {
				return array('status' => 0, 'info' => "安装失败");
			}	
		}
    }
	//添加菜单
	public function addNav() {
        $M = M("Nav");
		$M->create($_POST);
		setAdminLog('添加自定义菜单-'.$_POST['name']);
        return $M->add() ? array('status' => 1, 'info' => '添加成功', 'url' => U('System/nav')) : array('status' => 0, 'info' => '添加失败');
    }
	
	//编辑分类
	public function editNav() {
        $M = M("Nav");
		$M->create($_POST);
		setAdminLog('编辑自定义菜单-'.$_POST['name']);
        return $M->save() ? array('status' => 1, 'info' => '更新成功', 'url' => U('System/nav')) : array('status' => 0, 'info' => '更新失败');
    }

	
	//添加地区
	public function addCity() {
        $M = M("City");
		$M->create($_POST);
		setAdminLog('添加地区城市-'.$_POST['city']);
        return $M->add() ? array('status' => 1, 'info' => '添加成功', 'url' => U('System/city')) : array('status' => 0, 'info' => '添加失败');
    }
	
	//编辑地区
	public function editCity() {
        $M = M("City");
		setAdminLog('编辑地区城市-'.$_POST['city']);
		$M->create($_POST);
        return $M->save() ? array('status' => 1, 'info' => '更新成功', 'url' => U('System/city')) : array('status' => 0, 'info' => '更新失败');
    }
	
	public function addShipping(){
		$M = M("Shipping");
		$_POST['city_data'] = json_encode($_POST['city']);
		$M->create($_POST);
		if($_POST['id'] <> ''){
			setAdminLog('编辑配送方式-'.$_POST['name']);
			return $M->save() ? array('status' => 1, 'info' => '更新成功', 'url' => U('System/shipping')) : array('status' => 0, 'info' => '更新失败');
		}else{
			setAdminLog('添加配送方式-'.$_POST['name']);
        	return $M->add() ? array('status' => 1, 'info' => '添加成功', 'url' => U('System/shipping')) : array('status' => 0, 'info' => '添加失败');	
		}
	}
	
	public function paymentArray(){
		return array(
			array(
				'name' => "alipay",
				'title' => "支付宝",
				'description' => '支付宝网站(www.alipay.com) 是国内先进的网上支付平台。支付宝收款接口：在线即可开通，零预付，免年费，单笔阶梯费率，无流量限制。',
				'url' => 'https://b.alipay.com/newIndex.htm',
			),
			array(
				'name' => "wechat",
				'title' => "微信支付",
				'description' => '微信支付，是跟随微信发展又一个优秀产物，费率低，手机微信端支付的必备方式',
				'url' => 'https://mp.weixin.qq.com',
			),
			array(
				'name' => "balancepay",
				'title' => "余额支付",
				'description' => '你可以使用在本站的余额进行支付',
			),
            array(
                'name' => "offline",
                'title' => "线下支付",
                'description' => '使用汇款或转账等线下支付渠道',
            ),
		);	
	}
	public function shippingArray(){
		return array(
			array(
				'name' => "ems",
				'title' => "EMS快件",
				'description' => '中国邮政快递',
			),
		);	
	}
}

?>
