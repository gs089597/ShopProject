<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class MessageModel extends CommonModel {
	public function addMessage(){
		$M = M("adminMessage");
		$data['time'] = time();
		$data['form_admin'] = session("uid");
		$arr = $_POST['admin'];
		$data['content'] = $_POST['content'];
		foreach($arr as $k=>$v){
			$data['to_admin'] = $v;
			$M->add($data);
		}
       	return array('status' => 1, info => '操作成功');
	}
	public function getMessage(){
		$form_admin = $_GET['form_admin'];
		$M = M("adminMessage");
		$MA = M('Admin'); 
		$uid = session("uid");
		$arr = $M->where(array('to_admin'=>$uid))->field('form_admin')->select();
		foreach($arr as $k=>$v){
			$array[] = $v['form_admin'];
		}
		$array = array_unique($array);
		foreach($array as $k=>$v){
			$admin[$k] = $MA->where(array('aid'=>$v))->field('nickname,aid')->find();
			$admin[$k]['new'] = $M->where(array('to_admin'=>$uid,'form_admin'=>$v,'is_look'=>0))->count();
			if($k == 0){
				$map['to_admin'] = $uid;
				if($form_admin <> ''){
					$map['form_admin'] = $form_admin;
				}else{
					$map['form_admin'] = $v;
				}
				$admin[$k]['Message'] = $M->where($map)->order('time DESC')->limit(30)->select();
				$M->where($map)->save(array('is_look'=>1));
			}
		}
       	return $admin;
	}
}

?>
