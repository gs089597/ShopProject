<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class WeixinModel extends CommonModel {
	//分类列表
	public function getCategory() {
        $cat = new \Org\Util\Category('WeixNav', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        return $list;
    }
	//添加分类
	public function addCategory() {
        $M = M("WeixNav");
		setAdminLog('添加微信自定义菜单-'.$_POST['name']);
		$M->create($_POST);
        return $M->add() ? array('status' => 1, 'info' => '添加成功', 'url' => U('Weixin/listCategory')) : array('status' => 0, 'info' => '添加失败');
    }
	
	//编辑分类
	public function editCategory() {
        $M = M("WeixNav");
		setAdminLog('编辑微信自定义菜单-'.$_POST['name']);
		$M->create($_POST);
        return $M->save() ? array('status' => 1, 'info' => '更新成功', 'url' => U('Weixin/listCategory')) : array('status' => 0, 'info' => '更新失败');
    }
	public function getArr($arr,$type){
		if($type == 'text'){
			foreach($arr['content'] as $key=>$val){
				$array[$key]['content'] = $val;
			}
		}else{
			foreach($arr['Title'] as $key=>$val){
				$array[$key]['Title'] = $val;
				$array[$key]['Description'] = $arr['Description'][$key];
				$array[$key]['PicUrl'] = $arr['PicUrl'][$key];
				$array[$key]['Url'] = $arr['Url'][$key];
			}
		}
		return json_encode($array);
	}
	public function addReply() {
        $M = M("WeixReply");
		$_POST['data'] = $this->getArr($_POST['data'],$_POST['type']);
		setAdminLog('添加微信自动回复-'.$_POST['key']);
		$M->create($_POST);
        return $M->add() ? array('status' => 1, 'info' => '添加成功', 'url' => U('Weixin/reply')) : array('status' => 0, 'info' => '添加失败');
    }
	public function editReply() {
        $M = M("WeixReply");
		setAdminLog('编辑微信自动回复-'.$_POST['key']);
		$_POST['data'] = $this->getArr($_POST['data'],$_POST['type']);
		$M->create($_POST);
        return $M->save() ? array('status' => 1, 'info' => '更新成功', 'url' => U('Weixin/reply')) : array('status' => 0, 'info' => '更新失败');
    }
}

?>
