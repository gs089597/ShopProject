<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
class WapPageModel extends CommonModel {
	public function addPage(){
		$M = M("WapPage");
		$goods = $_POST['news'];
        if($id = $M->add($goods)){
			setAdminLog('添加自定义-'.$goods['title']);
			return array('status' => 1, 'info' => '添加成功');
		}else{
			return array('status' => 0, 'info' => '添加失败');
		} 
	}
	public function editPage(){
		$M = M("WapPage");
		(int)$id = $_POST['id'];
		$goods = $_POST['news'];
		$goods['id'] = $id; 
		$info = D("WapPage")->where(array('id'=>$id))->find();
		if($M->save($goods)){
			setAdminLog('编辑自定义-'.$goods['title']);
			return array('status' => 1, 'info' => '编辑成功');
		}else{
			return array('status' => 0, 'info' => '编辑失败');
		} 
	}
	
	
	public function addActPage(){
		$M = M("WapActPage");
		$goods = $_POST['news'];
        if($id = $M->add($goods)){
			setAdminLog('添加频道页-'.$goods['title']);
			return array('status' => 1, 'info' => '添加成功');
		}else{
			return array('status' => 0, 'info' => '添加失败');
		} 
	}
	public function editActPage(){
		$M = M("WapActPage");
		(int)$id = $_POST['id'];
		$goods = $_POST['news'];
		$goods['id'] = $id; 
		$info = D("WapActPage")->where(array('id'=>$id))->find();
		if($M->save($goods)){
			setAdminLog('编辑频道页-'.$goods['title']);
			return array('status' => 1, 'info' => '编辑成功');
		}else{
			return array('status' => 0, 'info' => '编辑失败');
		} 
	}
}

?>
