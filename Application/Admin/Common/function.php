<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

//添加管理员日志
function setAdminLog($info){
	$M = M('AdminLog'); 
	$uid = session('uid');
	$array = array(
		'admin_id' => $uid,
		'time' => time(),
		'model' => MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME,
		'info' => $info
	);
	$M->add($array);
}
function getPopuRankTpl($arr){
	$i = 0;
	$str = '';
	if(is_array($arr)){
		foreach($arr as $key=>$val){
			
			$str .= '<div class="controls"><label>级别<input type="text" name="scale_rank_'. $i .'" value="'. $key .'"></label>
						<label>提成： <input type="text" name="scale_a_'. $i .'" value="'. $val['scale_a'] .'"><span class="add-on">%</span></label>
						&nbsp;';						
						if($i == 0){
							$str .= '&nbsp; <span class="btn" onclick="addAttrS(this)">+</span>';
						}else{
							$str .= '&nbsp; <span class="btn" onclick="addAttrS(this)">+</span> &nbsp; <span class="btn" onclick="delAttrS(this)">-</span>';
						}
			$str .= '</div>';	
				
			$i++;							
		}	
	}else{
		$str .= '<div class="controls"><label>级别<input type="text" name="scale_rank_0" value="1"></label>
						<label>提成： <input type="text" name="scale_a_0" value="0"><span class="add-on">%</span></label> 
						&nbsp; <span onclick="addAttrS(this)" class="btn"> + </span>
					</div>';	
		$i++;
	}
	$str .= '<input type="hidden" value="'. $i .'" id="rank_num">';
	return $str;
}

function GoodsTypeTpl($arr){
	$i = 0;
	foreach($arr as $key=>$val){
		if($val['attr_type'] <> 0 && $val['inputtype'] == 3){
			$controlAa = 'controlAa';
		}
		$str .= '<div class="control-group '. $controlAa .'"><label class="control-label">'.$val['name'].' :</label>';
		if($val['GoodsAttr']){
			foreach($val['GoodsAttr'] as $k=>$v){
				//if($v['goods_id'] == (int)$_GET['GoodsId'] || $v['goods_id'] == (int)$_GET['id']){
					if($val['inputtype'] == 1){
						$str .= '<div class="controls">
									<input type="hidden" name="GoodsTypeIdArr_'.$i.'" value="'.$val['id'].'"/>
									<input type="text" name="GoodsTypeAttrArr_'.$i.'" value="'.$v['attr_value'].'" />
								 </div>';
					}
					if($val['inputtype'] == 2){
						$str .= '<div class="controls">
									<input type="hidden" name="GoodsTypeIdArr_'.$i.'" value="'.$val['id'].'"/>
									<textarea name="GoodsTypeAttrArr_'.$i.'">'.$v['attr_value'].'</textarea>
								 </div>';
					}
					if($val['attr_type'] <> 0 && $val['inputtype'] == 3){
						$str .= '<div class="controls">';
						$data = explode('|',$val['data']);
						$str .= '<label><select style="width:200px" name="GoodsTypeAttrArr_'.$i.'" class="span4">';
						foreach($data as $dk=>$dv){
							$select = $v['attr_value'] == $dv ? 'selected="selected"' : '';
							$str .= '<option '. $select .' value="'.$dv.'">'.$dv.'</option>';	
						}
						$str .= '</select></label>';
						$str .= '<label>属性价格： <input type="text" name="GoodsTypePriceArr_'.$i.'" value="'.$v['attr_price'].'"/></label>';
						if($k == 0){
							$str .= '&nbsp; <span class="btn" onclick="addAttrS(this)">+</span>';
						}else{
							$str .= '&nbsp; <span class="btn" onclick="addAttrS(this)">+</span> &nbsp; <span class="btn" onclick="javascript:if(popdel(\''.U('Goods/DelGoodsAttr',array('id'=>$v['goods_attr_id'])).'\',\'你确定要删除吗?\'));"> - </span>';
						}
						$str .= '<input type="hidden" class="TypeIdArr" name="GoodsTypeIdArr_'.$i.'" value="'.$val['id'].'"/>';
						$str .= '</div>';
						
					}elseif($val['attr_type'] == 0 && $val['inputtype'] == 3){
						$str .= '<div class="controls">';
						$data = explode('|',$val['data']);
						$str .= '<label><select style="width:200px" name="GoodsTypeAttrArr_'.$i.'" class="span4">';
						foreach($data as $dk=>$dv){
							$select = $v['attr_value'] == $dv ? 'selected="selected"' : '';
							$str .= '<option '. $select .' value="'.$dv.'">'.$dv.'</option>';	
						}
						$str .= '</select></label>';
						$str .= '<input type="hidden" name="GoodsTypeIdArr_'.$i.'" value="'.$val['id'].'"/>';
						$str .= '</div>';
					}
					$str .= '<input type="hidden" class="GoodsAttrId" name="GoodsAttrIdArr_'.$i.'" value="'.$v['goods_attr_id'].'"/>';
					$i++;
				}
			//}
		}else{
			if($val['inputtype'] == 1){
				$str .= '<div class="controls">
							<input type="hidden" name="GoodsTypeIdArr_'.$i.'" value="'.$val['id'].'"/>
							<input type="text" name="GoodsTypeAttrArr_'.$i.'" value="" />
						 </div>';
			}
			if($val['inputtype'] == 2){
				$str .= '<div class="controls">
							<input type="hidden" name="GoodsTypeIdArr_'.$i.'" value="'.$val['id'].'"/>
							<textarea name="GoodsTypeAttrArr_'.$i.'"></textarea>
						 </div>';
			}
			if($val['attr_type'] <> 0 && $val['inputtype'] == 3){
				$str .= '<div class="controls">';
				$data = explode('|',$val['data']);
				$str .= '<label><select style="width:200px" name="GoodsTypeAttrArr_'.$i.'" class="span4">';
				foreach($data as $dk=>$dv){
					$str .= '<option value="'.$dv.'">'.$dv.'</option>';	
				}
				$str .= '</select></label>';
				$str .= '<label>属性价格： <input type="text" name="GoodsTypePriceArr_'.$i.'" value=""/></label>';
				$str .= '&nbsp; <span class="btn" onclick="addAttrS(this)">+</span>';
				$str .= '<input type="hidden" class="TypeIdArr" name="GoodsTypeIdArr_'.$i.'" value="'.$val['id'].'"/>';
				$str .= '</div>';
				
			}elseif($val['attr_type'] == 0 && $val['inputtype'] == 3){
				$str .= '<div class="controls">';
				$data = explode('|',$val['data']);
				$str .= '<label><select style="width:200px" name="GoodsTypeAttrArr_'.$i.'" class="span4">';
				foreach($data as $dk=>$dv){
					$str .= '<option value="'.$dv.'">'.$dv.'</option>';	
				}
				$str .= '</select></label>';
				$str .= '<input type="hidden" name="GoodsTypeIdArr_'.$i.'" value="'.$val['id'].'"/>';
				$str .= '</div>';
			}
			$str .= '<input type="hidden" class="GoodsAttrId" name="GoodsAttrIdArr_'.$i.'" value=""/>';
			$i++;	
		}
		$str .= '</div>';
	}
	$str .= '<input type="hidden" id="attr_num" value="'.$i.'"/>';
	return $str;
}