<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
use  Common\Controller\SystemController;
class CommonController extends SystemController {
    public function _initialize() {
        parent::_initialize();

		if (C('USER_AUTH_ON') && !in_array(MODULE_NAME, explode(',', C('NOT_AUTH_MODULE')))) {
            $RBAC = new \Org\Util\Rbac();
			$RBAC->checkLogin();
            if (!$RBAC->AccessDecision()) {
                //检查认证识别号
                if (!$_SESSION[C('USER_AUTH_KEY')]) {
                    //跳转到认证网关
                    $this->redirect(C('USER_AUTH_GATEWAY'));
                }
				if (C('GUEST_AUTH_ON')) {
					$this->assign('jumpUrl', C('USER_AUTH_GATEWAY'));
				}
				$this->error(L('_VALID_ACCESS_'));
            }
        }
		$this->config = $this->configArrAdmin();
		$this->assign('configArr',$this->config);
		$this->assign('newMessageNum',$this->getMessageNum());
		$this->authentication();
	}
	//登陆验证
	protected function authentication(){
		$uid = session('uid');
		if(!empty($uid)){
			$usershell = session('usershell');
			$email = base64_decode(session('username'));
			$user=M('Admin');
			$condition=array(
				'aid'=>$uid,
				'email'=>$email
			);
			$rs=$user->where($condition)->find();
			if($usershell== md5(md5($rs['aid'].$rs['email']).$rs['pwd'])){
				$yzdl = true;	
			}else{
				$yzdl = false;
			}
		}else{
			$yzdl = false;
		}
		if($yzdl == false){
			$this->error("登陆超时！！请重新登陆",U('Public/index'));	
		}
	}
	//获取新通知条数
	protected function getMessageNum(){
		$M = M("adminMessage");
		$uid = session('uid');
		return $M->where(array('to_admin'=>$uid,'is_look'=>0))->count();
	}
	//初始化
	protected function configArrAdmin(){
		$uid = session('uid');
		$email = base64_decode(session('username'));
		$user=M('Admin');
		$condition=array(
			'aid'=>$uid,
			'email'=>$email
		);
		$rs=$user->where($condition)->find();
		$array['user']=$rs;
		$site = M('System')->select();
		foreach($site as $k => $v){
			$siteConfig[$v['name']] = json_decode($v['data'],true);
		}
		$array['siteConfig'] = $siteConfig;

		return $array;
	}
}