<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class PublicController extends Controller {
	public function verify_code(){
		$array = array(
			'imageW' => 150,
			'imageH' => 39,
			'length' => 4,
			'fontSize'=>16
		);
		$Verify = new \Think\Verify($array);
		$Verify->entry();	
	}
    public function index(){
//	    echo D("Public")->adminMd5("admin888");
		if(IS_POST){
            $returnLoginInfo = D("Public")->login();
            //生成认证条件
            if ($returnLoginInfo['status'] == 1) {
				$map = array();
                // 支持使用绑定帐号登录
                $map['email'] = $_POST['email'];
				$RBAC = new \Org\Util\Rbac();
                $authInfo = $RBAC->authenticate($map);
                $_SESSION[C('USER_AUTH_KEY')] = $authInfo['aid'];
                $_SESSION['email'] = $authInfo['email'];
                if ($authInfo['email'] == C('ADMIN_AUTH_KEY')) {
                    $_SESSION[C('ADMIN_AUTH_KEY')] = true;
                }
                // 缓存访问权限
                $RBAC->saveAccessList();
				
				$this->success($returnLoginInfo['info'],U('Index/index'),3);	
            }elseif($returnLoginInfo['status'] == 0){
				$this->error($returnLoginInfo['info']);	
			}
		}else{			
        	$this->display("login");
		}
    }
	public function logout(){
		session('uid',null);
		session('usershell',null);
		session('username',null);
		session(null);
		$this->success('成功退出！！',U('Public/index'),3);	
	}
}