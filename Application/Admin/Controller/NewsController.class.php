<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class NewsController extends CommonController {
	public function index() {
		$arr = D("News")->relation(true)->select();
		$this->assign("list", $arr);
        $this->display();
    }
	public function addNews(){
		if (IS_POST) {
			$arr = D("News")->addNews();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info['pidOption'] = $this->getPid(array('pid'=>''),0);
			$this->assign("info", $info);
            $this->display();
        }
	}
	public function editNews(){
		if (IS_POST) {
			$arr = D("News")->editNews();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info = $Goods = D('News')->where(array('id'=>(int)$_GET['id']))->relation(true)->find();
			$info['pidOption'] = $this->getPid(array('pid'=>$Goods['category']),0);
			$this->assign("info", $info);
            $this->display('addNews');
        }
	}
	public function delNews(){
		$info = $Goods = D('News')->where(array('id'=>$_GET['id']))->relation(true)->find();
		if(D("News")->where(array('id'=>(int)$_GET['id']))->delete()){
			setAdminLog('删除文章-'.$info['title']);
			D("NewsA")->where(array('id'=>(int)$_GET['id']))->delete();
			$this->success("操作成功");
		}else{
			$this->error("操作失败！！");
		}
	}
	
	/*************分类*****************/
	//分类列表
	public function listCategory(){
		$this->assign("list", D("News")->getCategory());
        $this->display();
	}
	//添加分类
	public function addCategory(){
		if (IS_POST) {
			$arr = D("News")->addCategory();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $this->assign("info", $this->getPid());
            $this->display();
        }
	}
	//编辑分类
	public function editCategory(){
		if (IS_POST) {
            $arr = D("News")->editCategory();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("NewsCategory");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该分类", U('News/listCategory'));
            }
            $this->assign("info", $this->getPid($info));
            $this->display('addCategory');
        }
	}
	//删除分类
	public function delCategory(){
		$M = M('NewsCategory');
		$info = $M->where("`id`=".(int)$_GET['id'])->find();
		if($M->where("`id`=".(int)$_GET['id'])->delete()){
			setAdminLog('删除文章分类-'.$info['name']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	/*************分类结束*****************/
	
	//获取分类
	private function getPid($info,$a=1) {
        $cat = new \Org\Util\Category('NewsCategory', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
		if($a==1){
        	$option = '<option value="0">一级分类</option>';
		}else{
			$option = '';
		}
        foreach ($list as $k => $v) {
            $selected = $v['id'] != $info['pid'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }
}