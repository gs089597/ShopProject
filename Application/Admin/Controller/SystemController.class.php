<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class SystemController extends CommonController {

 	//基础信息
    public function index() {
		if(IS_POST){
			$arr = D("System")->addData();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
		}else{
			$array = M('System')->where(array('name'=>'system_basis'))->find();
			$info = json_decode($array['data'],true);
			$this->assign("info", $info);
			$this->display();
		}
    }
	public function email() {
		if(IS_POST){
			$arr = D("System")->addData();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
		}else{
			$array = M('System')->where(array('name'=>'email'))->find();
			$info = json_decode($array['data'],true);
			$this->assign("info", $info);
			$this->display();
		}
    }
	public function sms() {
		if(IS_POST){
			$arr = D("System")->addData();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
		}else{
			$array = M('System')->where(array('name'=>'sms'))->find();
			$info = json_decode($array['data'],true);
			$this->assign("info", $info);
			$this->display();
		}
    }
	public function weixin() {
		if(IS_POST){
			$arr = D("System")->addData();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
		}else{
			$array = M('System')->where(array('name'=>'wechat'))->find();
			$info = json_decode($array['data'],true);
			$this->assign("info", $info);
			$this->display();
		}
    }
	public function city(){
		if($_GET['var']){
			$where = array('mid'=>$_GET['var']);
		}else{
			$where = array('mid'=>0);
		}
		$list = M('City')->where($where)->select();
        $this->assign("list", $list);
		$this->display();	
	}
	
	public function ajaxGetCity(){
		$where = array('mid'=>$_POST['id']);
		$list = M('City')->where($where)->select();
        echo json_encode($list);
	}
	//添加地区
	public function addCity(){
		if (IS_POST) {
			$arr = D("System")->addCity();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
			$where = array('var'=>$_GET['var']);
			$info = M('City')->where($where)->find();
			if(!$_GET['var']){
				$info['mid'] = 0;
				$info['city'] = '省级';
			}
			$this->assign("info",$info);
            $this->display();
        }
	}
	//编辑地区
	public function editCity(){
		if (IS_POST) {
            $arr = D("System")->editCity();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("City");
            $info = $M->where("var=" . (int) $_GET['var'])->find();
            if (empty($info['var'])) {
                $this->error("不存在该分类", U('System/City'));
            }
            $this->assign("info", $info);
            $this->display('addCity');
        }
	}
	//删除地区
	public function delCity(){
		$M = M('City');
		$info = $M->where("`var`=".(int)$_GET['var'])->delete();
		if($M->where("`var`=".(int)$_GET['var'])->delete()){
			setAdminLog('删除地区－'.$info['city']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	//支付方式设置
	public function payment(){
		$list = D('System')->paymentArray();
		$M = M('Payment');
		foreach($list as $key=>$val){
			$list[$key]['is_install'] = 0;
			if($M->where(array('name'=>$val['name']))->count()){
				$list[$key]['is_install'] = 1;
			}
		}
		$this->assign("list", $list);
        $this->display();	
	}
	//支付配置
	public function addPayment(){
		if(IS_POST){
			$arr = D("System")->addPayment();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
		}else{
			$array = M('Payment')->where(array('name'=>$_GET['type']))->find();
			$info = json_decode($array['config'],true);
			$this->assign("info", $info);
			$this->display('System/payment/'.$_GET['type']);	
		}
	}
	//删除支付配置
	public function delPayment(){
		if(M('Payment')->where(array('name'=>$_GET['type']))->delete()){
			setAdminLog('删除支付配置－'.$_GET['type']);
			$this->success("卸载成功");
		}else{
			$this->error("卸载失败");	
		}	
	}
	
	//配送方式设置
	public function shipping(){
		$list = D('System')->shippingArray();
		$M = M('Shipping');
		foreach($list as $key=>$val){
			$list[$key]['is_install'] = 0;
			if($M->where(array('type'=>$val['name']))->count()){
				$list[$key]['is_install'] = 1;
			}
		}
		
		$this->assign("list", $list);
        $this->display();	
	}
	public function shippingList(){
		$M = M('Shipping');
		$list = $M->where(array('type'=>$_GET['type']))->select();
		$this->assign("list", $list);
        $this->display();	
	}
	//配送配置
	public function addShipping(){
		if(IS_POST){
			$arr = D("System")->addShipping();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
		}else{
			$city = M('City')->where(array('mid'=>0))->select();
			$this->assign("city", $city);
			$info = M('Shipping')->where(array('type'=>$_GET['type'],'id'=>$_GET['id']))->find();
			$info['city_data'] = json_decode($info['city_data'],true);
			foreach($info['city_data'] as $k=>$v){
				$info['city_data'][$k] = M("City")->where(array('var'=>$v))->find();
			}
			$this->assign("info", $info);
			$this->display('System/shipping/'.$_GET['type']);	
		}
	}
	//删除配送配置
	public function delShipping(){
		if(M('Shipping')->where(array('type'=>$_GET['type']))->delete()){
			setAdminLog('删除配送配置－'.$_GET['type']);
			$this->success("卸载成功");
		}else{
			$this->error("卸载失败");	
		}	
	}
	 
	/*************自定义菜单*****************/
	//自定义菜单列表
	public function nav(){
		$cat = new \Org\Util\Category('nav', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList(); 
		$this->assign("list", $list);
        $this->display();
	}
	//添加自定义菜单
	public function addNav(){
		if (IS_POST) {
			$arr = D("System")->addNav();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $this->assign("info", $this->getPid());
            $this->display();
        }
	}
	//编辑自定义菜单
	public function editNav(){
		if (IS_POST) {
            $arr = D("System")->editNav();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("Nav");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该菜单", U('System/nav'));
            }
            $this->assign("info", $this->getPid($info));
            $this->display('addNav');
        }
	}
	//删除自定义菜单
	public function delNav(){
		$info = M('Nav')->where("`id`=".(int)$_GET['id'])->find();
		if(M('Nav')->where("`id`=".(int)$_GET['id'])->delete()){
			setAdminLog('删除自定义导航－'.$info['name']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	/*************商品分类结束*****************/
	
	
	public function adminLogList() {		
		$count = D("AdminLog")->where($map)->count();
		$pagesize = 15;
		$Page = new  \NewsLib\adminPage($count, $pagesize);
		$show       = $Page->show();
		
		$arr = M("AdminLog")->where($map)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
		foreach ($arr as $key=>$val){
			$arr[$key]['Admin'] = M("Admin")->where(array('aid'=>$val['admin_id']))->find();
		}
		$this->assign("list", $arr);
		$this->assign('page',$show );
        $this->display();
	}
	
	
	//测试邮箱配置
	public function testEmailConfig() {
        $return = send_mail($_POST['test_email'], "", "测试配置是否正确", "这是一封测试邮件，如果收到了说明配置没有问题", "", $_POST);
        if ($return == 1) {
            echo json_encode(array('status' => 1, 'info' => "测试邮件已经发往你的邮箱" . $_POST['test_email'] . "中，请注意查收"));
        } else {
            echo json_encode(array('status' => 0, 'info' => "$return"));
        }
    }
	//测试短信配置
	public function testSmsConfig() {
        $return = send_sms($_POST['sms_phont'],"你的验证码是 000000 如果收到信息说明配置成功", $_POST);
        if ($return == 100) {
            echo json_encode(array('status' => 1, 'info' => "测试已经成功发送到" . $_POST['sms_phont'] . "手机中，请注意查收"));
        } else {
            echo json_encode(array('status' => 0, 'info' => "$return"));
        }
    }
	//支付宝测试
	public function alipayapi(){
		header("Content-Type:text/html; charset=utf-8");
		//vendor('Alipay.Submit');
		$config = $_POST['data'];
		$alipay_config['partner']		= $config['partner'];
		$alipay_config['key']			= $config['key'];
		$alipay_config['sign_type']    = strtoupper('MD5');//签名方式 不需修改
		$alipay_config['input_charset']= strtolower('utf-8');//字符编码格式 目前支持 gbk 或 utf-8
		$alipay_config['transport']    = 'http';//访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
		$alipay_config['cacert']    = getcwd().'/ThinkPHP/Library/NewsLib/Alipay/cacert.pem';
        $payment_type = "1";//支付类型
        $notify_url = "http://".$_SERVER['HTTP_HOST']."/Center/notify";//服务器异步通知页面路径 
        $return_url = "http://".$_SERVER['HTTP_HOST']."/Center/alipayreturn";//页面跳转同步通知页面路径
        $seller_email = $config['email'];//卖家支付宝帐户
        $out_trade_no = time();//商户订单号
        $subject = '账户充值';//订单名称
        $quantity = "1";//商品数量
        $logistics_fee = "0.00";//物流费用
        $logistics_type = "EXPRESS";//物流类型
        $logistics_payment = "SELLER_PAY";//物流支付方式
		
		//构造要请求的参数数组，无需改动
		$parameter = array(
				//"service" => "trade_create_by_buyer",//双通道接口
				//"service" => "create_direct_pay_by_user",//及时到账接口
				//"service" => "create_partner_trade_by_buyer",//担保借款
				"service" => $config['payment_type'],
				"partner" => trim($alipay_config['partner']),
				"payment_type"	=> $payment_type,
				"notify_url"	=> $notify_url,
				"return_url"	=> $return_url,
				"seller_email"	=> $seller_email,
				"out_trade_no"	=> $out_trade_no,
				"subject"	=> $subject,
				"quantity"	=> $quantity,
				"logistics_fee"	=> $logistics_fee,
				"logistics_type"	=> $logistics_type,
				"logistics_payment"	=> $logistics_payment,
				"body"	=> $body,
				"show_url"	=> $show_url,
				"receive_name"	=> $receive_name,
				"receive_address"	=> $receive_address,
				"receive_zip"	=> $receive_zip,
				"receive_phone"	=> $receive_phone,
				"receive_mobile"	=> $receive_mobile,
				"_input_charset"	=> trim(strtolower($alipay_config['input_charset']))
		);
		if($config['payment_type'] == 'trade_create_by_buyer'){
			$parameter['price'] = 0.01;
		}elseif($config['payment_type'] == 'create_direct_pay_by_user'){
			$parameter['total_fee'] = 0.01;
		}elseif($config['payment_type'] == 'create_partner_trade_by_buyer'){
			$parameter['price'] = 0.01;
		}
		$alipaySubmit = new \NewsLib\Alipay\AlipaySubmit($alipay_config);
		echo $html_text = $alipaySubmit->buildRequestForm($parameter,"get", "确认");
		
	}
	
	//获取自定义菜单
	private function getPid($info,$a=1) {
        $cat = new \Org\Util\Category('Nav', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
		if($a==1){
        	$option = '<option value="0">一级菜单</option>';
		}else{
			$option = '';
		}
        foreach ($list as $k => $v) {
            $selected = $v['id'] != $info['pid'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }
    
}