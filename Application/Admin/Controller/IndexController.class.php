<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class IndexController extends CommonController {
  	public function getaaa(){
    	echo R('Wap/Fenxiao/getWeixinQRCodeWei', array('uid' => 3));
    }
    public function index(){
		$this->assign('goods_info', $this->getGoodsInfo());
		$this->assign('order_info', $this->getOrderInfo());
        $this->assign('server_info', $this->getServerInfo());
        $this->display();
    }
	protected function getGoodsInfo(){
		$info = array(
            '商品总数' => 0,
			'库存警告商品数' => 0
        );	
		return $info;	
	}
	protected function getOrderInfo(){
		$info = array(
            '待发货订单' => 0,
            '未确认订单' => 0,
			'待支付订单' => 0,
			'已成交订单数' => 0,
			'退款申请' => 0,
			'已取消订单' => 0,
        );	
		return $info;	
	}
	protected function getServerInfo(){
		if (function_exists('gd_info')) {
            $gd = gd_info();
            $gd = $gd['GD Version'];
        } else {
            $gd = "不支持";
        }
        $info = array(
            '操作系统' => PHP_OS,
            '主机名IP端口' => $_SERVER['SERVER_NAME'] . ' (' . $_SERVER['SERVER_ADDR'] . ':' . $_SERVER['SERVER_PORT'] . ')',
            '运行环境' => $_SERVER["SERVER_SOFTWARE"],
            'PHP运行方式' => php_sapi_name(),
            '程序目录' => WEB_ROOT,
			'PHP 版本'=> PHP_VERSION,
			'文件上传的最大大小'=>ini_get('upload_max_filesize'),
            'MYSQL版本' => function_exists("mysql_close") ? mysql_get_client_info() : '不支持',
            'GD库版本' => $gd,
//            'MYSQL版本' => mysql_get_server_info(),
            '上传附件限制' => ini_get('upload_max_filesize'),
            '执行时间限制' => ini_get('max_execution_time') . "秒",
            '剩余空间' => round((@disk_free_space(".") / (1024 * 1024)), 2) . 'M',
            '服务器时间' => date("Y年n月j日 H:i:s"),
            '北京时间' => gmdate("Y年n月j日 H:i:s", time() + 8 * 3600),
            '采集函数检测' => ini_get('allow_url_fopen') ? '支持' : '不支持',
            'register_globals' => get_cfg_var("register_globals") == "1" ? "ON" : "OFF",
            'magic_quotes_gpc' => (1 === get_magic_quotes_gpc()) ? 'YES' : 'NO',
            'magic_quotes_runtime' => (1 === get_magic_quotes_runtime()) ? 'YES' : 'NO',
        );	
		return $info;
	}
	
	
	public function message(){
		$arr = D("Message")->getMessage();
		$this->assign("arr", $arr);
        $this->display();
	}
	public function addMessage(){
		if (IS_POST) {
            $arr = D("Message")->addMessage();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("Admin");
			$uid = session("uid");
            $info = $M->select();
            $this->assign("info", $info);
            $this->display();
        }	
	}
	
	
	public function myinfo(){
        if (IS_POST) {
            $arr = D("Access")->editMyinfo();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("Admin");
			$uid = session("uid");
            $info = $M->where(array('aid'=>$uid))->find();
            $this->assign("info", $info);
            $this->display();
        }
    }
	
	
	public function uploadImg(){	
		header('Content-type: text/html; charset=UTF-8');
		$upload = new \Think\Upload(C('Upload'));// 实例化上传类
		$upload->savePath = './image/';
		$info = $upload->upload();
		if(!$info){
			$data['error']=1;
			$data['message']=$upload->getError();
			exit(json_encode($data));
		}
		$data=array(
			'url'=>str_replace('./','/','/Uploads'.$info['imgFile']['savepath']).'/'.$info['imgFile']['savename'],
			'error'=>0,
		);
		if($_GET['thumb'] == 1){
			$image = new \Think\Image(); 
			$image->open('./'.$data['url']);// 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.jpg
			$image->thumb(150, 150)->save(".".str_replace('./','/','/Uploads'.$info['imgFile']['savepath']).'thumb_'.$info['imgFile']['savename']);
			$data['thumb'] = str_replace('./','/','/Uploads'.$info['imgFile']['savepath']).'thumb_'.$info['imgFile']['savename'];
		}
		exit(json_encode($data));		
	}
}