<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class SupplierController extends CommonController {
	public function index() {
		$arr = D("Supplier")->relation(true)->select();
		$this->assign("list", $arr);
        $this->display();
    }
	public function addSupplier(){
		if (IS_POST) {
			$arr = D("Supplier")->addSupplier();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
            $this->display();
        }
	}
	public function editSupplier(){
		if (IS_POST) {
			$arr = D("Supplier")->editSupplier();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info = D('Supplier')->where(array('id'=>(int)$_GET['id']))->relation(true)->find();
			$this->assign("info", $info);
            $this->display('addSupplier');
        }
	}
	public function delSupplier(){
		$info = $Goods = D('Supplier')->where(array('id'=>$_GET['id']))->relation(true)->find();
		if(D("Supplier")->where(array('id'=>(int)$_GET['id']))->delete()){
			$this->success("操作成功");
		}else{
			$this->error("操作失败！！");
		}
	}
	public function getWeixinQRCode(){
		$weixconfig = $this->config['siteConfig']['wechat'];
		$weObj = new  \NewsLib\Wechat($weixconfig);
		$key = '商户'.$_GET['id'];
		$arr = $weObj->getQRCode($key,2);
		$data = array(
					0 => array(
						'content' => "<a href=\"http://" . $_SERVER['HTTP_HOST'].U('/Wap/Index/popularize',array('user' => $_GET['id'],'type' => 's'))."\">马上点击注册成为会员</a>",
					),
				);
		$data = json_encode($data);
		$array = array(
			'key' => $key,
			'type' => 'text',
			'data' => $data
			);
		$M = M("WeixReply")->add($array);
		D('Supplier')->where(array('id'=>(int)$_GET['id']))->save(array('weixin_qrcode'=>$weObj->getQRUrl($arr['ticket'])));
		$this->success("成功生成");
	}
	public function getQRCode(){
		$outUrl = "http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Index/popularize',array('type'=>'s', 'user'=>$_GET['id']));
		$img = new \NewsLib\myQRCode();
		$url = $img->getUrl($outUrl);
		if($url){
			D('Supplier')->where(array('id'=>(int)$_GET['id']))->save(array('referrer_qrcode'=>$url));
			$this->success("成功生成");
		}
	}
}