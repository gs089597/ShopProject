<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class OrderController extends CommonController {
    public function index() {
        $M = D("Orders");
        C('TOKEN_ON',false);
		$_GET['orderstatus'] <> 10 && $_GET['orderstatus'] <> '' ? $map['order_status'] = $_GET['orderstatus'] : '';
		$_GET['pay_status'] <> 10 && $_GET['pay_status'] <> '' ? $map['pay_status'] = $_GET['pay_status'] : '';
		$_GET['shipping_status'] <> 10 && $_GET['shipping_status'] <> '' ? $map['shipping_status'] = $_GET['shipping_status'] : '';
		$_GET['key'] <> '' ? $map['ordernum'] = array('like','%'.$_GET['key'].'%') : '';
		
		$_GET['startime'] <> '' && $_GET['endtime'] == ''? $map['addtime'] = array("egt",strtotime($_GET['startime'])) : '';
		$_GET['endtime'] <> '' && $_GET['startime'] == ''? $map['addtime'] = array("elt",strtotime($_GET['endtime'])) : '';
		$_GET['startime'] <> '' && $_GET['endtime'] <> '' ? $map['addtime'] = array(array("egt",strtotime($_GET['startime'])),array("elt",strtotime($_GET['endtime']))) : "";
		$_GET['user'] > 0 ? $map['user_id'] = $_GET['user'] : '';
		if($_GET['type'] == 100){
			$map['type'] = array('in',array(3,4,5));
		}else if($_GET['type'] < 10 && $_GET['type'] <> ''){
			$map['type'] = $_GET['type'];
		}		
		if($_GET['tguser']){
			$users = M('User')->where(array('referrer_u' => $_GET['tguser']))->field("id")->select();
			$uidarr = array();
			foreach($users as $key=>$val){
				$uidarr[] = $val['id'];
			}
			$map['user_id'] = array("in",$uidarr);
		}
		$count = $M->where($map)->count();
		$pagesize = 15;
		$Page = new  \NewsLib\adminPage($count, $pagesize);
		$show       = $Page->show();
		$this->assign('page',$show );
		
        $tabs = $M->where($map)->order('`addtime` DESC')->relation(true)->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign("list", $tabs);

        $this->display();
    }
	public function ShippingStatus(){
		$M = D("Orders");
		if($_GET['shipping_status']){
			$array = array(
				'shipping_status'=>$_GET['shipping_status'],
				'shipping_time' => time(),
			);
			if($M->where(array('id'=>$_GET['id']))->save($array)){
				if($_GET['shipping_status'] == 2){
					M('OrderGoods')->where(array('order_id'=>$_GET['id']))->save(array('status'=>1));
				}				
				$this->success("修改成功",U('Order/orderInfo',array("id"=>$_GET['id'])),3);
			}else{
				$this->error("修改成功失败", U('Order/orderInfo',array("id"=>$_GET['id'])));
			};
		}	
	}
	public function OrderStatus(){
		$M = D("Orders");
		$id = $_GET['id'];
		if($_GET['order_status']){
			$array = array(
				'order_status'=>$_GET['order_status']
			);
			if($array['order_status'] == 2){
				$orders = M("Orders")->where(array('order_status'=>0,'id'=>$id))->find();				
				$OrderGoods = M("OrderGoods")->where(array('order_id'=>$orders['id']))->select();
				if($orders['type'] == 1){
					$G = M("Goods");
				}
				foreach ($OrderGoods as $k => $v){
					if(M("Orders")->where(array('id'=>$orders['id'],'order_status'=>0))->save($array)){
						$s = 1;
					}else{
						$s = 0;
					}
				}
				if($s){
					$this->success("修改成功",U('Order/orderInfo',array("id"=>$id)),3);
				}else{
					$this->error("修改成功失败", U('Order/orderInfo',array("id"=>$id)));
				}
			}
			if($M->where(array('id'=>$id))->save($array)){
				$this->success("修改成功",U('Order/orderInfo',array("id"=>$id)),3);
			}else{
				$this->error("修改成功失败", U('Order/orderInfo',array("id"=>$id)));
			};
		}	
	}
	public function orderInfo() {
        $M = D("Orders");
        $info = $M->where(array("id"=>$_GET['id']))->relation(true)->find();
//         $info['UserAddress']['province'] = D('City')->where(array('var'=>$info['UserAddress']['province']))->find();
//         $info['UserAddress']['city'] = D('City')->where(array('var'=>$info['UserAddress']['city']))->find();
//         $info['UserAddress']['district'] = D('City')->where(array('var'=>$info['UserAddress']['district']))->find();
        if($info['type'] == 1){
        	$info['OrderGoods'] = $this->getOrderGoodsG($info['id']);
        }
        $info['User'] = D('User')->where(array('id'=>$info['user_id']))->find();
        $data = json_decode($info['User']['bank'],true);
        $this->assign("data", $data);
		if(IS_POST){
			if($_POST['type'] == 'deliver'){
				if(M("OrderShipping")->where(array('order_id'=>$info['id'],'status'=>array('eq',0)))->count() > 0){
					$this->error("发货失败");
				}
				$shipping_type = M('Shipping')->where(array('id'=>$_POST['shipping_id']))->find();
				$array = array(
					'shipping_order'=>$_POST['shipping_order'],
					'shipping_status'=> $_POST['shipping_status']
				);	
				$order_shipping = array(
						'address' => $info['address_a'],
						'zipcode' => $info['zipcode'],
						'mobile' => $info['mobile'],
						'consignee' => $info['consignee'],
						'shipping_order' => $_POST['shipping_order'],
						'shipping_name' => $shipping_type['name'],
						'addtime' => time(),
						'touser' => $_POST['touser'],
						'admininfo' => $_POST['admininfo'],
						'status' => 0,
						'user_id' => $info['user_id'],
						'order_id' => $info['id'],
						'shipping_id' => $shipping_type['id'],
				);
				M("OrderShipping")->add($order_shipping);
				$M->where(array('id'=>$_POST['orderId']))->save($array);
				setUserMessage($info['user_id'],'您的订单号为：'.$info['ordernum'].'的订单已经通过['.$shipping_type['name'].']发货，快递订单号为：'.$array['shipping_order'].'，请您注意查收');
				if($info['mobile'])
				//send_sms($info['mobile'],"您的订单{$info['ordernum']}已通过[{$shipping_type['name']}]发货，".$array['shipping_order']."，请及时确认收货。谢谢！");
// 				$msarr = array( 
// 								array(
// 								'title' => "发货提醒",
// 								'description' => '您的订单号为：'.$info['ordernum'].'的订单已经发货，快递订单号为： 请您注意查收\n<a href=\"http://" . $_SERVER['HTTP_HOST'] .U('/Wap/Order/orderInfo',array('id'=>$info['id'])) .'">查看详情</a>',
// 								'url' => "http://" . $_SERVER['HTTP_HOST'] . U('/Wap/Order/orderInfo',array('id'=>$info['id'])),
// 								'picurl' => "http://" . $_SERVER['HTTP_HOST'].$this->config['siteConfig']['system_basis']['sys_weix_img4'],
// 						)												
// 				); 
				$weixMessss = '尊敬的用户您好！您的订单号为'.$info['ordernum'].'的订单已经发货，快递订单号为：'.$_POST['shipping_name'].' ，'.$array['shipping_order']."，".$_POST['touser']." 请您收货后进入商城的订单中心确认收货。祝您生活愉快！\n<a href=\"http://" . $_SERVER['HTTP_HOST'] .U('/Wap/Order/orderInfo',array('id'=>$info['id'])) .'">查看详情</a>';
				setUserWeixinMessage($info['user_id'],$weixMessss);				
			}
			
			if($_POST['type'] == 'pay'){
				$order = D('Orders')->where(array('id'=>$_POST['orderId']))->find();
				$this->ordersave($order['ordernum'],$order['ordernum'],date('Y-m-d H:i:s',time()));
			}
			$this->success("操作成功","",3);
		}else{
			$order_fh = M("OrderShipping")->where(array('order_id'=>$info['id']))->order("id DESC")->select();
			$this->assign("order_fh", $order_fh);		
			$this->assign("info", $info);			
	        $this->display();
		}
    }
	public function getOrderGoodsG($order){
		$OrderGoods = D('OrderGoods')->where(array('order_id'=>$order))->select();
		foreach($OrderGoods as $key=>$val){
			$OrderGoods[$key]['Goods'] = $goods = M('Goods')->where(array('id'=>$val['goodsId']))->find();
			$attr = json_decode($val['attr'],true);
			$str = '';
			$attrArr = array();
			foreach ($attr as $k=>$v){
				$attrArr[] = M('GoodsAttr')->where(array('goods_attr_id'=>$v['attrId']))->find();
				$str .= $v['attr'].' | 属性加价：'.$v['attrPrice'].'<br/>';
			}
			$OrderGoods[$key]['attr_a'] = $str;
			$OrderGoods[$key]['attr_arr'] = $attrArr;
		}
		return $OrderGoods;
	}
	public function deliver(){
		$M = D("Orders");
		$info = $M->where(array("id"=>$_GET['Order']))->find();					
		if($info['type'] == 1){
			$info['OrderGoods'] = $this->getOrderGoodsG($info['id']);
		}
		$shipping_type = M('Shipping')->select();
		$this->assign("shipping_type", $shipping_type);
		$this->assign("info", $info);
		$this->display();	
	}
	public function pay(){
		$this->display();	
	}
	public function afterSaleOrder(){	
		if(IS_POST){
			$id = $_POST['id'];
			$order = M('OrderExchanged')->where(array('id'=>$id))->find();
			M('OrderExchanged')->where(array('id'=>$id))->save(array('admininfo'=>$_POST['admininfo'],'status'=>$_POST['status']));
			setAdminLog('审核退换货订单，单号'.$order['ordernum']);
		}		
		$M = D("OrderExchanged");
		$_GET['status'] <> '' ? $map['status'] = $_GET['status'] : '';
		$_GET['type'] <> '' ? $map['type'] = $_GET['type'] : '';
		$count = $M->where($map)->count();
		$pagesize = 15;
		$Page = new  \NewsLib\adminPage($count, $pagesize);
		$show       = $Page->show();
		$this->assign('page',$show );		
        $tabs = $M->where($map)->order('`addtime` DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach ($tabs as $key => $val){
        	$tabs[$key]['Order'] = M('Orders')->where(array('id'=>$val['orderid']))->find();
        	$tabs[$key]['User'] = M('User')->where(array('id'=>$val['user_id']))->find();
        	$tabs[$key]['OrderGoods'] = M('OrderGoods')->where(array('id'=>$val['ordergid']))->find();
        	if($tabs[$key]['Order']['type'] == 1){
        		$GoodsM = M('Goods');
        	}
        	$tabs[$key]['OrderGoods']['Goods'] = $GoodsM->where(array('id'=>$tabs[$key]['OrderGoods']['goodsId']))->find();
        }
        //print_r($tabs);
        $this->assign("list", $tabs);
		$this->display();

	}
	public function afterSaleOrderA(){
		$this->display();
	}
	//确认完成操作
	public function afterSaleOrderB(){
		$type = $_GET['type'];
		$id = $_GET['id'];
		$order = M('OrderExchanged')->where(array('id'=>$id))->find();	
		$timea = time();
		if(M('OrderExchanged')->where(array('id'=>$id))->save(array('status'=>$_GET['status']))){
			if($type == 3){
				$U = M('User');
				$order_goods = M('OrderGoods')->where(array('id'=>$order['ordergid']))->find();	
				$user = $U->where(array('id'=>$order['user_id']))->find();
				$scoreA['user_money'] = $user['user_money'] + $order_goods['zPrice'];
				$U->where(array('id'=>$user['id']))->save($scoreA);
				addUserAccount($user['username'],$order_goods['zPrice'],3,'退款订单编号：'.$order['ordernum'],'站内退款',$timea);
			}
			setAdminLog('完成退换货订单，单号'.$order['ordernum']);
			$this->success("操作成功");
		}else{
			$this->error("操作失败");	
		}	
	}
	public function comment(){
		$M = D("GoodsComment");
		$_GET['status'] <> '' ? $map['status'] = $_GET['status'] : '';
        $tabs = $M->where($map)->order('`time` DESC')->select();
		foreach($tabs as $key=>$val){
			$tabs[$key]['User'] = M('User')->where(array('id'=>$val['user_id']))->find();
			$tabs[$key]['Goods'] = M('Goods')->where(array('id'=>$val['goods_id']))->find();
		}
        $this->assign("list", $tabs);
        $this->display();
	}
	public function infoComment(){
		$M = D("GoodsComment");
		$map['id'] = $_GET['id'];
        $tabs = $M->where($map)->order('`time` DESC')->find();
		echo "评论分值：".$tabs['grade']."<br/>评论内容：".$tabs['content'];
	}
	public function editComment(){
		if(D("GoodsComment")->where(array('id'=>$_GET['id']))->save(array('status'=>$_GET['status']))){
			setAdminLog('审核评论');
			$this->success("操作成功");
		}else{
			$this->error("操作失败");
		}
	}
	public function orderSh(){
		$O = D('Orders');
		$orderid = $_GET['orderid'];
		$order = $O->where(array('id'=>$orderid))->find();
		if($order['shipping_status'] == 1){
			$shipping_status = 2;
			$status = 1;
		}elseif($order['shipping_status'] == 3){
			$shipping_status = 4;
			$status = 3;
		}else{
			$this->error('非法操作！！');
		}
		if(D("OrderShipping")->where(array('id'=>$_GET['shipping_order']))->save(array('status'=>1,'stime'=>time()))){
			if($O->where(array('shipping_status'=>$status,'id'=>$orderid))->save(array('shipping_status'=>$shipping_status,'shipping_time'=>time()))){
				setAdminWeixinMessage("订单号为 : ".$order['ordernum']."，已经成功收货，请前往确认");
				M('OrderGoods')->where(array('order_id'=>$orderid))->save(array('status'=>1));
				$this->success('操作成功');
			}else{
				$this->error('操作失败');
			}
		}else{
			$this->error('操作失败');
		}
	}
	
	public function getOrderGoodsGExcel($order){
		$OrderGoods = D('OrderGoods')->where(array('order_id'=>$order))->select();
		foreach($OrderGoods as $key=>$val){
			$OrderGoods[$key]['Goods'] = $goods = M('Goods')->where(array('id'=>$val['goodsId']))->find();
			$attr = json_decode($val['attr'],true);
			$str = array();
			$attrArr = array();
			foreach ($attr as $k=>$v){
				$attrArr[] = M('GoodsAttr')->where(array('goods_attr_id'=>$v['attrId']))->find();
				$str[] = array(
						'title' => $v['attr'],
						'goodsnum' => "属性附加价",
						'price' => $v['attrPrice'],
						'number' => $goods['number'],
				);
			}
			$OrderGoods[$key]['attr_a'] = $str;
		}
		return $OrderGoods;
	}
	public function getOrderExcel(){
		$M = D("Orders");
		C('TOKEN_ON',false);
		$_GET['orderstatus'] <> 10 && $_GET['orderstatus'] <> '' ? $map['order_status'] = $_GET['orderstatus'] : '';
		$_GET['pay_status'] <> 10 && $_GET['pay_status'] <> '' ? $map['pay_status'] = $_GET['pay_status'] : '';
		$_GET['shipping_status'] <> 10 && $_GET['shipping_status'] <> '' ? $map['shipping_status'] = $_GET['shipping_status'] : '';
		$_GET['key'] <> '' ? $map['ordernum'] = array('like','%'.$_GET['key'].'%') : '';
		$_GET['type'] > 10 && $_GET['type'] <> '' ? $map['type'] = $_GET['type'] : '';
		$_GET['startime'] <> '' & $_GET['endtime'] == ''? $map['addtime'] = array("egt",strtotime($_GET['startime'])) : '';
		$_GET['endtime'] <> '' & $_GET['startime'] == ''? $map['addtime'] = array("elt",strtotime($_GET['endtime'])) : '';
		$_GET['startime'] <> '' & $_GET['endtime'] <> '' ? $map['addtime'] = array(array("elt",strtotime($_GET['endtime'])),array("elt",strtotime($_GET['endtime']))) : "";
		$_GET['user'] > 0 ? $map['user_id'] = $_GET['user'] : '';
		if($_GET['tguser']){
			$users = M('User')->where(array('referrer_u' => $_GET['tguser']))->field("id")->select();
			$uidarr = array();
			foreach($users as $key=>$val){
				$uidarr[] = $val['id'];
			}
			$map['user_id'] = array("in",$uidarr);
		}
		
		$ordersArr = $M->where($map)->order('`addtime` DESC')->relation(true)->select();
		foreach ($ordersArr as $key=>$val){
			if($val['type'] == 1){
				$ordersArr[$key]['OrderGoods'] = $this->getOrderGoodsGExcel($val['id']);
			}
		}
		//print_r($ordersArr);
		$obj = new \NewsLib\myPHPExcel();
		$xlsName = "订单";
		$xlsCell = array(
				array('id','数据库ID'),
				array('type','订单类型'),
				array('user','订单用户'),
				array('ordernum','订单编号'),
				array('pay_type','支付方式'),
				array('pay_ordernum','支付平台订单号'),
				array('addtime','下单时间'),
				array('paytime','支付时间'),
				array('pay_status','支付状态'),				
				array('order_status','订单状态'),				
				array('ordersize','订单金额'),
				array('shipping_order','物流订单'),
				array('consignee','收货人名称'),
				array('mobile','收货电话'),
				array('address_a','收货地址'),
				array('zipcode','收货邮编'),
				array('goodsname','商品名称'),
//				array('goodsprice','商品价格（普通）'),
//				array('number','商品编号'),
//				array('goodsname_a','属性名称'),
//				array('goodsnum','属性编号'),
//				array('price','属性单价'),
		);
		$ki = 0;
		foreach ($ordersArr as $key_1=>$val_1){
			if($val_1['type'] == 1){
				$type = "商品购物订单";
			}elseif($val_1['type'] == 2){
				$type = "充值订单";
			}elseif($val_1['type'] == 3){
				$type = "会员购买订单";
			}
			if($val_1['pay_status'] == 0){
				$pay_status = "未支付";
			}elseif($val_1['pay_status'] == 1){
				$pay_status = "成功支付";
			}
			if($val_1['order_status'] == 0){
				$order_status = "未完成";
			}elseif($val_1['order_status'] == 1){
				$order_status = "已经完成";
			}elseif($val_1['order_status'] == 2){
				$order_status = "已经取消";
			}
				
			$user = M("User")->where(array("id"=>$val_1['user_id']))->find();
			foreach ($val_1['OrderGoods'] as $k=>$v){
				//foreach ($v['attr_a'] as $k_1=>$v_1){
					$xlsData[$ki]['id'] = $val_1['id'];
					$xlsData[$ki]['type'] = $type;
					$xlsData[$ki]['user'] = $user['nickname']."[".$user['username']."]";
					$xlsData[$ki]['ordernum'] = "/".$val_1['ordernum'];
					$xlsData[$ki]['pay_type'] = $val_1['Payment']['cname'];
					$xlsData[$ki]['pay_ordernum'] = "/".$val_1['pay_ordernum'];
					$xlsData[$ki]['addtime'] = date("Y-m-d H:i:s",$val_1['addtime']);
					$xlsData[$ki]['paytime'] = "/".$val_1['paytime'];
					$xlsData[$ki]['pay_status'] = $pay_status;
					$xlsData[$ki]['order_status'] = $order_status;
					$xlsData[$ki]['ordersize'] = $val_1['ordersize'];
					$xlsData[$ki]['shipping_order'] = $val_1['shipping_order'];
					$xlsData[$ki]['consignee'] = $val_1['consignee'];
					$xlsData[$ki]['mobile'] = $val_1['mobile'];
					$xlsData[$ki]['address_a'] = $val_1['address_a'];
					$xlsData[$ki]['zipcode'] = $val_1['zipcode'];
					$xlsData[$ki]['goodsname'] = $v['Goods']['title'];
//					$xlsData[$ki]['number'] = $v_1['number'];
//					$xlsData[$ki]['goodsname_a'] = $v_1['title'];
//					$xlsData[$ki]['goodsnum'] = $v_1['goodsnum'];
//					$xlsData[$ki]['goodsprice'] = $v['Goods']['price'];
//					$xlsData[$ki]['price'] = $v_1['price'];
					$ki ++;
				//}
			}
		}
		//print_r($xlsData);
		$obj->exportExcel($xlsName,$xlsCell,$xlsData);		
	}
	
	
}