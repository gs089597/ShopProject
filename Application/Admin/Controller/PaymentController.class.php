<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class PaymentController extends CommonController {	
	public function index(){
		$id = $_GET['account'];
		$paytype = $_GET['paytype'];
		
		$order = M("UserAccount")->where(array('id'=>$id))->find();
		if($order['is_paid'] == 1){
			return array('status' => 0, 'info' => "这个提现操作已经审核过了", 'url' => U("User/account"));
		}
		$payment = D('Payment')->where(array('name'=>$paytype))->find();
		$config = json_decode($payment['config'],true);	
		$user = D("User")->where(array('username'=>$order['user_id']))->find();
		if($user['wx_openid'] == ''){
			$this->error("当前用户 没有绑定微信，无法完成付款");
		}
		$array = array (
	    		'partner_trade_no' => $order['id'],
	    		'openid' => $user['wx_openid'],
	    		'check_name' => 'NO_CHECK',
	    		'amount' => $order ['money'] * 100,
	    		'desc' => "会员提现",
	    		're_user_name' => $user['nickname']
	    );		
		$this->$payment['name']($config,$array);
	}	
	//微信支付
	public function wechatConfig($config){
		return array(
			'APPID' => $config['appid'],
			'MCHID' => $config['mch_id'],
			'KEY' => $config['key'],//商户支付密钥Key。审核通过后，在微信发送的邮件中查看（新版本中需要自己填写）
			'APPSECRET' => $config['appsecret'],//JSAPI接口中获取openid，审核后在公众平台开启开发模式后可查看
			'CURL_TIMEOUT' => 30,//本例程通过curl使用HTTP POST方法，此处可修改其超时时间，默认为30秒
			//=======【证书路径设置】=====================================
			'SSLCERT_PATH' => __ROOT__.'ThinkPHP/Library/NewsLib/Wechat/cacert/apiclient_cert.pem',
			'SSLKEY_PATH' => __ROOT__.'ThinkPHP/Library/NewsLib/Wechat/cacert/apiclient_key.pem',
			'ROORCA' => __ROOT__.'ThinkPHP/Library/NewsLib/Wechat/cacert/rootca.pem',
		);
	}
	public function wechat($config,$data) {
		$wechatConfig = $this->wechatConfig($config);		
	    $unifiedOrder = new \NewsLib\Wechat\Qyfk($wechatConfig);   
	    $return = $unifiedOrder->QfykPay($data);
    	if($return['return_code'] == "SUCCESS"){
    		if($return['result_code'] == "SUCCESS"){
    			$this->upOrder($return);
    			$this->success("已经成功向用户付款！",U("User/account"),3);
    		}else{
    			$message = $return['err_code_des']."【".$return['err_code']."】";
    		}
    	}else{
    		$message = $return['return_msg'];
    	}
    	$this->error($message,U("User/auditAccount",array('id'=>$array['partner_trade_no'])));
    }
    /* Array
     (
     [return_code] => SUCCESS
     [return_msg] => Array
     (
     )
      
     [mch_appid] => wx96213cb43c85ee4e
     [mchid] => 1323697601
     [device_info] => Array
     (
     )
      
     [nonce_str] => BH92JOENR5VLJMOJKHSELBA9XPM1XX
     [result_code] => SUCCESS
     [partner_trade_no] => 12
     [payment_no] => 1000018301201606240695849115
     [payment_time] => 2016-06-24 16:48:25
     ) */
    public function upOrder($arr){ 
    	$account = M("UserAccount")->where(array('id'=>$arr['partner_trade_no']))->find();
    	$ss = M('User')->where(array('username'=>$account['user_id']))->setDec('user_money',$account['money']);
    	M('User')->where(array('username'=>$account['user_id']))->setDec('frozen_money',$account['money']);
    	$admin_note = $account['admin_note']." 微信支付信息：【微信订单号：{$arr['payment_no']}，微信支付时间：{$arr['payment_time']}】";
    	$array = array(
    			'admin_note'=>$admin_note,
    			'is_paid' => 1
    	);
    	M("UserAccount")->where(array('id'=>$arr['partner_trade_no']))->save($array);
    	setAdminLog('审核提现申请-申请id:'.$account['id'].'-操作成功,付款成功，用户余额扣款也操作成功');    	
    	$user = D("User")->where(array('username'=>$account['user_id']))->find();
    	$str = '尊敬的'.$user['nickname'].'，您的提现'.$account['money'].'元操作，已经支付到您的微信余额，请注意查收';
    	if($user['phone'] <> "" && $account['type'] == 2){
    		//send_sms($user['phone'],$str); //发送提现短信通知
    	}
    	setUserWeixinMessage($user['id'],$str);
    	setUserMessage($user['id'],$str);
    }
}