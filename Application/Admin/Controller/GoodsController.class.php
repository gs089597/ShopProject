<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class GoodsController extends CommonController {
    /*************商品编辑**************************/
    //商品列表
    public function index() {
        $info['SupplierOption'] = $this->getSupplier();
        $info['pidOption'] = $this->getPid();
        $info['BrandOption'] = $this->getBrand();
        $this->assign("info", $info);
        $map['is_recycle'] = 0;
        $_GET['key'] <> '' ? $map['title'] = array('like','%'.$_GET['key'].'%') : '';
        $_GET['supplier'] <> false ? $map['supplier'] = $_GET['supplier'] : '';
        $_GET['category'] <> false ? $map['category'] = $_GET['category'] : '';
        $_GET['brand'] <> false ? $map['brand'] = $_GET['brand'] : '';
        $_GET['user_id'] <> false ? $map['user_id'] = $_GET['user_id'] : '';
        if($_GET['type'] > 0){
            $type = $_GET['type'];
            if($type == 1){
                $map['is_new'] = 1;
            }
            if($type == 2){
                $map['is_hot'] = 1;
            }
        }
        $count = D("Goods")->where($map)->count();
        $pagesize = 15;
        $Page = new  \NewsLib\adminPage($count, $pagesize);
        $show       = $Page->show();

        $arr = D("Goods")->where($map)->order('id DESC')->relation(true)->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign("list", $arr);
        $this->assign('page',$show );
        $this->display();
    }
    //商品回收站列表
    public function goodsListRecycle() {
        $arr = D("Goods")->where(array('is_recycle'=>1))->relation(true)->select();
        $this->assign("list", $arr);
        $this->display();
    }
    //添加商品
    public function addGood(){
        if (IS_POST) {
            $arr = D("Goods")->addGoods();
            $arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
            $info['SupplierOption'] = $this->getSupplier();
            $info['pidOption'] = $this->getPid(array('pid'=>''),0);
            $info['BrandOption'] = $this->getBrand();
            $info['user_rank_fee'] = M('UserRank')->select();
            $GoodsType = M("GoodsType")->where(array('pid'=>0))->select();
            $this->assign("GoodsType", $GoodsType);
            $this->assign("info", $info);
            $this->display();
        }
    }
    //编辑商品
    public function editGood(){
        if (IS_POST) {
            $arr = D("Goods")->editGoods();
            $arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
            $info = $Goods = D('Goods')->where(array('id'=>$_GET['id']))->relation("GoodsA")->find();

            $str = '';
            foreach(json_decode($info['photos'],true) as $key => $val){
                $str .= '<li class="input-append">
							<div class="img"><img src="'. $val['photo'] .'"/></div>
								<p><input class="span10"  type="text" value="'. $val['photo'] .'" name="photo[]"/><input class="span10"  type="hidden" value="'. $val['photo_thumb'].'" name="photo_thumb[]"/><span onclick="delImg(this)" class="add-on"><i  class=" icon-trash delImg tip-bottom" data-original-title="删除图片"></i></span></p>
						</li>';
            }
            $info['photos'] = $str;
            $info['pidOption'] = $this->getPid(array('pid'=>$Goods['category']),0);
            $info['BrandOption'] = $this->getBrand(array('id'=>$Goods['brand']));
            $info['SupplierOption'] = $this->getSupplier(array('id'=>$Goods['supplier']));


            $GoodsTypeArr =  M("GoodsType")->where(array('pid'=>$info['goods_type']))->select();
            foreach($GoodsTypeArr as $key => $val){
                $GoodsTypeArr[$key]['GoodsAttr'] = D('GoodsAttr')->where(array('goods_id'=>$info['id'],'attr_id'=>$val['id']))->select();
            }
            $info['GoodsAttr'] = GoodsTypeTpl($GoodsTypeArr);
            $GoodsType = M("GoodsType")->where(array('pid'=>0))->select();
            $info['member_fee'] = unserialize($info['member_fee']);
            $info['user_rank_fee'] = M('UserRank')->select();
            $this->assign("GoodsType", $GoodsType);
            $this->assign("info", $info);
            $this->display('addGood');
        }
    }
    //删除商品
    public function delGood(){
        $info = $Goods = D('Goods')->where(array('id'=>$_GET['id']))->relation(true)->find();

        if(D("Goods")->where(array('id'=>(int)$_GET['id']))->delete()){
            setAdminLog('删除商品-'.$info['title']);
            D("GoodsAttr")->where(array('goods_id'=>(int)$_GET['id']))->delete();
            D("GoodsA")->where(array('id'=>(int)$_GET['id']))->delete();
            $this->success("操作成功");
        }else{
            $this->error("操作失败！！");
        }
    }
    //回收商品
    public function RecycleGood(){
        $info = $Goods = D('Goods')->where(array('id'=>$_GET['id']))->relation(true)->find();
        setAdminLog('回收商品-'.$info['title']);
        D("Goods")->where(array('id'=>(int)$_GET['id']))->save(array('is_recycle'=>(int)$_GET['is_recycle'])) ? $this->success("操作成功") : $this->error("操作失败！！");
    }
    //删除商品属性值
    public function DelGoodsAttr(){
        if(M('GoodsAttr')->where("`goods_attr_id`=".(int)$_GET['id'])->delete()){
            setAdminLog('删除商品属性值');
            $this->success("删除成功");
        }else{
            $this->error("删除失败");
        }
    }
    public function setGoodsTj(){
        $type = $_POST['type'];
        $ids = $_POST['goodsids'];
        $where['id'] = array('in',$ids);
        if($type == 1){
            $sql = array('is_putaway'=>1);	//上架
        }elseif($type == 2){
            $sql = array('is_putaway'=>0);	//下架
        }elseif($type == 3){
            $sql = array('is_new'=>1);	//新品推荐
        }elseif($type == 4){
            $sql = array('is_new'=>0);	//取消新品
        }elseif($type == 5){
            $sql = array('is_hot'=>1);	//热销推荐
        }elseif($type == 6){
            $sql = array('is_hot'=>0);	//取消热销
        }
        if(D('Goods')->where($where)->save($sql)){
            $this->success("操作成功");
        }else{
            $this->error("操作失败");
        }
    }
    /*************商品结束**************************/




    /*************商品类型属性*****************/
    //类型属性列表
    public function listGoodsType(){
        if($_GET['ma'] == 'type'){
            $this->assign("list",M('GoodsType')->where(array('pid'=>0))->select());
        }
        if($_GET['ma'] == 'attribute'){
            $this->assign("list",M('GoodsType')->where(array('pid'=>(int)$_GET['pid']))->select());
        }
        $this->display();
    }
    //添加商品属性
    public function addGoodsType(){
        if (IS_POST) {
            $arr = D("Goods")->addGoodsType();
            $arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            if($_GET['ma'] == 'attribute'){
                $cat = new \Org\Util\Category('GoodsType', array('id', 'pid', 'name', 'fullname'));
                $list = $cat->getList(array('pid'=>0)); //获取分类结构
                foreach ($list as $k => $v) {
                    $selected = $v['id'] != $info['pid'] ? "" : ' selected="selected"';
                    $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
                }
                $info['pidOption'] = $option;
            }
            $this->assign("info",$info);
            $this->display();
        }
    }
    //编辑商品属性
    public function editGoodsType(){
        if (IS_POST) {
            $arr = D("Goods")->editGoodsType();
            $arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $info = M('GoodsType')->where(array('id'=>(int)$_GET['id']))->find();
            if($_GET['ma'] == 'attribute'){
                $cat = new \Org\Util\Category('GoodsType', array('id', 'pid', 'name', 'fullname'));
                $list = $cat->getList(array('pid'=>0)); //获取分类结构
                foreach ($list as $k => $v) {
                    $selected = $v['id'] != $info['pid'] ? "" : ' selected="selected"';
                    $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
                }
                $info['pidOption'] = $option;
            }
            $this->assign("info",$info);
            $this->display('addGoodsType');
        }
    }
    //删除属性
    public function delGoodsType(){
        $info = M('GoodsType')->where("`id`=".(int)$_GET['id'])->find();
        if(M('GoodsType')->where("`id`=".(int)$_GET['id'])->delete()){
            setAdminLog('删除商品属性-'.$info['name']);
            $this->success("删除成功");
        }else{
            $this->error("删除失败");
        }
    }

    /*************商品属性结束*****************/


    /*************商品分类*****************/
    //分类列表
    public function listCategory(){
        $this->assign("list", D("Goods")->getCategory());
        $this->display();
    }
    //添加分类
    public function addCategory(){
        if (IS_POST) {
            $arr = D("Goods")->addCategory();
            $arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $this->assign("types", M('GoodsType')->where(array('pid' => 0))->select());
            $this->assign("info", $this->getPid());
            $this->display();
        }
    }
    //编辑分类
    public function editCategory(){
        if (IS_POST) {
            $arr = D("Goods")->editCategory();
            $arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("GoodsCategory");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该分类", U('Goods/listCategory'));
            }
            $info['types_id'] = explode(',', $info['types_id']);
            $this->assign("types", M('GoodsType')->where(array('pid' => 0))->select());
            $this->assign("info", $this->getPid($info));
            $this->display('addCategory');
        }
    }
    //删除分类
    public function delCategory(){
        $info = M('GoodsCategory')->where("`id`=".(int)$_GET['id'])->find();
        if(M('GoodsCategory')->where("`id`=".(int)$_GET['id'])->delete()){
            setAdminLog('删除商品分类-'.$info['name']);
            $this->success("删除成功");
        }else{
            $this->error("删除失败");
        }
    }
    /*************商品分类结束*****************/



    /*************商品品牌*****************/
    //商品品牌列表
    public function listBrand(){
        $this->assign("list", M('GoodsBrand')->where()->select());
        $this->display();
    }
    //添加商品品牌
    public function addBrand(){
        if (IS_POST) {
            $arr = D("Goods")->addBrand();
            $arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $this->display();
        }
    }
    //编辑商品品牌
    public function editBrand(){
        if (IS_POST) {
            $arr = D("Goods")->editBrand();
            $arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("GoodsBrand");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该品牌", U('Goods/listCategory'));
            }
            $this->assign("info", $info);
            $this->display('addBrand');
        }
    }
    //删除商品品牌
    public function delBrand(){
        $M = M("GoodsBrand");
        $info = $M->where("id=" . (int) $_GET['id'])->find();
        if($M->where("`id`=".(int)$_GET['id'])->delete()){
            setAdminLog('删除商品品牌-'.$info['name']);
            $this->success("删除成功");
        }else{
            $this->error("删除失败");
        }
    }
    /*************商品品牌结束*****************/

    //Ajax获取属性
    public function AjaxGetAtt(){
        $info = $Goods = D('Goods')->where(array('id'=>$_GET['GoodsId']))->relation(true)->find();
        //print_r($info);
        if($info['goods_type'] != $_GET['pid']){
            $GoodsType = M('GoodsType')->where(array('pid'=>$_GET['pid']))->select();
            $str = GoodsTypeTpl($GoodsType);
        }else{
            $GoodsTypeArr =  M("GoodsType")->where(array('pid'=>$info['goods_type']))->select();
            foreach($GoodsTypeArr as $key => $val){
                //$GoodsTypeArr[$key]['GoodsAttr'] = D('GoodsAttr')->where(array('attr_id'=>$val['id']))->select();
                $GoodsTypeArr[$key]['GoodsAttr'] = D('GoodsAttr')->where(array('goods_id'=>$_GET['GoodsId'],'attr_id'=>$val['id']))->select();
            }
            //print_r($GoodsTypeArr);
            $str = GoodsTypeTpl($GoodsTypeArr);
        }

        echo json_encode(array('str'=>$str,'status'=>1));
    }
    public function ajaxGetSupplier(){

    }
    //获取供货商
    private function getSupplier($info) {
        $cat = new \Org\Util\Category('Supplier', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        $option = '<option value="0">请选择</option>';
        foreach ($list as $k => $v) {
            $selected = $v['id'] != $info['id'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
        }
        return $option;
    }
    //获取品牌
    private function getBrand($info) {
        $cat = new \Org\Util\Category('GoodsBrand', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        $option = '<option value="0">请选择</option>';
        foreach ($list as $k => $v) {
            $selected = $v['id'] != $info['id'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
        }
        return $option;
    }
    //获取分类
    private function getPid($info,$a=1) {
        $cat = new \Org\Util\Category('GoodsCategory', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        if($a==1){
            $option = '<option value="0">一级分类</option>';
        }else{
            $option = '';
        }
        foreach ($list as $k => $v) {
            $selected = $v['id'] != $info['pid'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }
}