<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class AccessController extends CommonController {

    //管理员列表OK
    public function index() {
        $this->assign("list", D("Access")->adminList());
        $this->display();
    }
	//节点列表OK
    public function nodeList() {
        $this->assign("list", D("Access")->nodeList());
        $this->display();
    }
	public function editNode() {
        if (IS_POST) {
            $arr = D("Access")->editNode();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("Node");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该节点", U('Access/nodeList'));
            }
            $this->assign("info", $this->getPid($info));
            $this->display();
        }
    }

    public function addNode() {
        if (IS_POST) {
			$arr = D("Access")->addNode();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $this->assign("info", $this->getPid(array('level' => 1)));
            $this->display("editNode");
        }
    }
	public function delNode(){
		
		$info = M('Node')->where("`id`='".$_GET['id']."'")->find();
		if(M('Node')->where("`id`='".$_GET['id']."'")->delete()){
			setAdminLog('删除节点-'.$info['name']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	//角色列表OK
    public function roleList() {
        $this->assign("list", D("Access")->roleList());
        $this->display();
    }
	
    public function addRole() {
        if (IS_POST) {
			$arr = D("Access")->addRole();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $this->assign("info", $this->getRole());
            $this->display("editRole");
        }
    }

    public function editRole() {
        if (IS_POST) {
            $arr = D("Access")->editRole();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("Role");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该角色", U('Access/roleList'));
            }
            $this->assign("info", $this->getRole($info));
            $this->display();
        }
    }

    public function opNodeStatus() {
        echo json_encode(D("Access")->opStatus("Node"));
    }

    public function opRoleStatus() {
        echo json_encode(D("Access")->opStatus("Role"));
    }

    public function opSort() {
        $M = M("Node");
        $datas['id'] = (int) $_POST['id'];
        $datas['sort'] = (int) $_POST['sort'];
        if ($M->save($datas)) {
            echo json_encode(array('status' => 1, 'info' => "处理成功"));
        } else {
            echo json_encode(array('status' => 0, 'info' => "处理失败"));
        }
    }

    
    public function changeRole() {
        if (IS_POST) {
			$arr = D("Access")->changeRole();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("Node");
            $info = M("Role")->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该用户组", U('Access/roleList'));
            }
            $access = M("Access")->field("CONCAT(`node_id`,':',`level`,':',`pid`) as val")->where("`role_id`=" . $info['id'])->select();
            $info['access'] = count($access) > 0 ? json_encode($access) : json_encode(array());
            $this->assign("info", $info);
            $datas = $M->where("level=1")->select();
            foreach ($datas as $k => $v) {
                $map['level'] = 2;
                $map['pid'] = $v['id'];
                $datas[$k]['data'] = $M->where($map)->select();
                foreach ($datas[$k]['data'] as $k1 => $v1) {
                    $map['level'] = 3;
                    $map['pid'] = $v1['id'];
                    $datas[$k]['data'][$k1]['data'] = $M->where($map)->select();
                }
            }
            $this->assign("nodeList", $datas);
            $this->display();
        }
    }

    /**
      +----------------------------------------------------------
     * 添加管理员
      +----------------------------------------------------------
     */
    public function editAdmin() {
        if (IS_POST) {
			$arr = D("Access")->editAdmin();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("Admin");
            $aid = (int) $_GET['aid'];
            $pre = C("DB_PREFIX");
            $info = $M->where("`aid`=" . $aid)->join($pre . "role_user ON " . $pre . "admin.aid = " . $pre . "role_user.user_id")->find();
            if (empty($info['aid'])) {
                $this->error("不存在该管理员ID", U('Access/index'));
            }
            if ($info['email'] == C('ADMIN_AUTH_KEY')) {
                $this->error("超级管理员信息不允许操作", U("Access/index"));
                exit;
            }
            $this->assign("info", $this->getRoleListOption($info));
            $this->display("addAdmin");
        }
    }
    public function addAdmin() {
    	if (IS_POST) {
    		$arr = D("Access")->addAdmin();
    		$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
    	} else {
    		$this->assign("info", $this->getRoleListOption(array('role_id' => 0)));
    		$this->display();
    	}
    }
    public function delAdmin() {
    	$info = M('Admin')->where("`aid`='".$_GET['aid']."'")->find();
    	if(M('Admin')->where("`aid`='".$_GET['aid']."'")->delete()){
    		setAdminLog('删除管理员-'.$info['nickname']);
    		$this->success("删除成功");
    	}else{
    		$this->error("删除失败");
    	}
    }
    private function getRole($info = array()) {
        $cat = new \Org\Util\Category('Role', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        foreach ($list as $k => $v) {
            $disabled = $v['id'] == $info['id'] ? ' disabled="disabled"' : "";
            $selected = $v['id'] == $info['pid'] ? ' selected="selected"' : "";
            $info['pidOption'].='<option value="' . $v['id'] . '"' . $selected . $disabled . '>' . $v['fullname'] . '</option>';
        }
        return $info;
    }

    private function getRoleListOption($info = array()) {
        $cat = new \Org\Util\Category('Role', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        $info['roleOption'] = "";
        foreach ($list as $v) {
            $disabled = $v['id'] == 1 ? ' disabled="disabled"' : "";
            $selected = $v['id'] == $info['role_id'] ? ' selected="selected"' : "";
            $info['roleOption'].='<option value="' . $v['id'] . '"' . $selected . $disabled . '>' . $v['fullname'] . '</option>';
        }
        return $info;
    }

    private function getPid($info) {
        $arr = array("请选择", "项目", "模块", "操作");
        for ($i = 1; $i < 4; $i++) {
            $selected = $info['level'] == $i ? " selected='selected'" : "";
            $info['levelOption'].='<option value="' . $i . '" ' . $selected . '>' . $arr[$i] . '</option>';
        }
        $level = $info['level'] - 1;
        $cat = new \Org\Util\Category('Node', array('id', 'pid', 'title', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        $option = $level == 0 ? '<option value="0" level="-1">根节点</option>' : '<option value="0" disabled="disabled">根节点</option>';
        foreach ($list as $k => $v) {
            $disabled = $v['level'] == $level ? "" : ' disabled="disabled"';
            $selected = $v['id'] != $info['pid'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $disabled . $selected . '  level="' . $v['level'] . '">' . $v['fullname'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }

}