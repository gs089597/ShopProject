<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class WeixinController extends CommonController {
	public function listCategory(){
		$this->assign("list", D("Weixin")->getCategory());
        $this->display();
	}
	//添加分类
	public function addCategory(){
		if (IS_POST) {
			$arr = D("Weixin")->addCategory();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $this->assign("info", $this->getPid());
            $this->display();
        }
	}
	//编辑分类
	public function editCategory(){
		if (IS_POST) {
            $arr = D("Weixin")->editCategory();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("WeixNav");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该菜单", U('Weixin/listCategory'));
            }
            $this->assign("info", $this->getPid($info));
            $this->display('addCategory');
        }
	}
	//删除分类
	public function delCategory(){
		$M = M('WeixNav');
		$info = $M->where("`id`=".(int)$_GET['id'])->find();
		if($M->where("`id`=".(int)$_GET['id'])->delete()){
			setAdminLog('删除微信自定义菜单-'.$info['name']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	//提交生成菜单
	public function upNav(){
		$weixconfig = $this->config['siteConfig']['wechat'];
		$weObj = new  \NewsLib\Wechat($weixconfig);
		$M = M('WeixNav');
		$info = $M->where(array('pid'=>0))->order('sort ASC')->select();
		foreach($info as $k=>$v){
			$sub = $M->where(array('pid'=>$v['id']))->order('sort ASC')->select();
			if($sub){
				$but = array(
					'name' => $v['name']
				);
				$s_button = '';
				foreach($sub as $k_1=>$v_1){
					$sub_button = array(
						'type' => $v_1['type'],
						'name' => $v_1['name']
					);
					if($v_1['type'] == 'view'){
						$sub_button['url'] = $v_1['url'];
					}elseif($v_1['type'] == 'click'){
						$sub_button['key'] = $v_1['key'];
					}
					$s_button[] = $sub_button;
				}
				$but['sub_button'] = $s_button;
			}else{
				$but = array(
					'type' => $v['type'],
					'name' => $v['name']
				);
				if($v['type'] == 'view'){
					$but['url'] = $v['url'];
				}elseif($v['type'] == 'click'){
					$but['key'] = $v['key'];
				}
			}
			$array[] = $but;
		}
		$data['button'] = $array;	
		$str = $weObj->createMenu($data);
		if($str == false){
			$this->error("生成失败,错误代码：".$weObj->errCode."---".$weObj->errMsg);	
		}else{
			$this->success("生成成功");
		}
	}
	
	public function reply(){
		$arr = M("WeixReply")->select();
		$this->assign("list", $arr);
        $this->display();
		
	}
	public function addReply(){
		if (IS_POST) {
			$arr = D("Weixin")->addReply();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $this->display();
        }
	}
	public function editReply(){
		if (IS_POST) {
            $arr = D("Weixin")->editReply();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
        } else {
            $M = M("WeixReply");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
			$info['content'] = json_decode($info['data'],true);
            if (empty($info['id'])) {
                $this->error("不存在该回复", U('Weixin/reply'));
            }
            $this->assign("info", $this->getPid($info));
            $this->display('addReply');
        }
	}
	public function getInfo(){
		$M = M("WeixReply");
		$info = $M->where("id=" . (int) $_GET['id'])->find();
		$list = json_decode($info['data'],true);
		$this->assign("info", $list);
		$this->display($_GET['type']);	
	}
	//删除分类
	public function delReply(){
		$M = M('WeixReply');
		$info = $M->where("`id`=".(int)$_GET['id'])->find();
		if($M->where("`id`=".(int)$_GET['id'])->delete()){
			setAdminLog('删除微信自动回复-'.$info['key']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	
	
	//获取分类
	private function getPid($info,$a=1) {
        $cat = new \Org\Util\Category('WeixNav', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
		if($a==1){
        	$option = '<option value="0">一级分类</option>';
		}else{
			$option = '';
		}
        foreach ($list as $k => $v) {
            $selected = $v['id'] != $info['pid'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }
}