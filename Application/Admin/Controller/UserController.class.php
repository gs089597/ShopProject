<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class UserController extends CommonController {
    public function index() {
        $M = M("User");
        if($_GET['referrer_user']){
            $tarr = $M->where(array('phone'=>$_GET['referrer_user']))->find();
            if($tarr){
                $map['referrer_u'] = $tarr['id'];
            }
        }
        $_GET['is_artist'] <> '' && $_GET['is_artist'] < 999 ? $map['is_artist'] = $_GET['is_artist'] : '';
        $_GET['key'] <> '' ? $map['username'] = array('like','%'.$_GET['key'].'%') : '';
        $_GET['phone'] <> false ? $map['phone'] = array('like','%'.$_GET['phone'].'%') : '';
		$count = $M->where($map)->count();
		$pagesize = 20;
		$Page = new  \NewsLib\adminPage($count, $pagesize);
		$show       = $Page->show();
		$this->assign('page',$show );		
		$list = $M->where($map)->limit($Page->firstRow.','.$Page->listRows)->order("id DESC")->select();
		foreach($list as $key=>$val){
			if($val['referrer_u'] > 0){
				$list[$key]['referrer_u_arr'] = $M->where(array('id'=>$val['referrer_u']))->find();
			}
		}
		$this->assign("list", $list);
        $this->display();
    }
	
	public function addUser(){
		if(IS_POST){
			$arr = D("User")->addUser();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
		}else{
            $M = M("User");
            $info = $M->where(array('id'=>(int)$_GET['id']))->find();
            $info['district'] = $this->getPid(array('mid'=>$info['city']),$info['district']);
            $info['city'] = $this->getPid(array('mid'=>$info['province']),$info['city']);
            $info['province'] = $this->getPid(array('mid'=>0),$info['province']);
            $info['top_pic'] = json_decode($info['top_pic'], true);
            $this->assign("info", $info);
			$this->display();
		}
	}
	
	public function editUser(){
		if(IS_POST){
			$arr = D("User")->editUser();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
		}else{
			$M = M("User");
			$info = $M->where(array('id'=>(int)$_GET['id']))->find();
            $info['district'] = $this->getPid(array('mid'=>$info['city']),$info['district']);
            $info['city'] = $this->getPid(array('mid'=>$info['province']),$info['city']);
            $info['province'] = $this->getPid(array('mid'=>0),$info['province']);
            $info['top_pic'] = json_decode($info['top_pic'], true);
			$this->assign("info", $info);
			$this->display('addUser');
		}
	}
	
	//删除会员
	public function delUser(){
		$datas  =  M('User')->where("`id`=".(int)$_GET['id'])->find();
		if($id = M('User')->where("`id`=".(int)$_GET['id'])->delete()){
			setAdminLog('删除会员-'.$datas['username']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	
	//会员等级列表
	public function listRank() {
        $M = M("UserRank");
        $tabs = $M->select();
        $this->assign("list", $tabs);
        $this->display();
    }
	//添加会员等级
	public function addRank(){
		if(IS_POST){
			$arr = D("User")->addRank();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
		}else{
			$this->display();
		}
	}
	//编辑会员等级
	public function editRank(){
		if(IS_POST){
			$arr = D("User")->editRank();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
		}else{
			$M = M("UserRank");
			$info = $M->where(array('id'=>(int)$_GET['id']))->find();
			$this->assign("info", $info);
			$this->display('addRank');
		}
	}
	//删除会员等级
	public function delRank(){
		$datas =  M('UserRank')->where("`id`=".(int)$_GET['id'])->find();
		if(M('UserRank')->where("`id`=".(int)$_GET['id'])->delete()){
			setAdminLog('删除会员等级-'.$datas['rank_name']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	//会员操作提现申请
	public function account() {	
		$M = M("UserAccount");
		$count = $M->count();
		$pagesize = 20;
		$Page = new  \NewsLib\adminPage($count, $pagesize);
		$show       = $Page->show();
		$this->assign('page',$show );		
		$list = $M->limit($Page->firstRow.','.$Page->listRows)->order("id DESC")->select();
		$this->assign("list", $list);
		$this->display();	
    }
	public function addAccount(){
		if(IS_POST){
			$arr = D("User")->addAccount();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
		}else{
			$info['paymentArr'] = M('payment')->select();
			$this->assign("info", $info);
			$this->display();
		}
	}
	public function auditAccount(){
		if(IS_POST){					
			$arr = D("User")->auditAccount();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
		}else{
			$info = M('UserAccount')->where(array('id'=>(int)$_GET['id']))->find();
			$info['paymentArr'] = M('payment')->select();
			$data = json_decode($info['data'],true);
			$this->assign("data", $data);
			$this->assign("info", $info);
			$this->display('addAccount');
		}	
	}
	public function editAccount(){
		if(IS_POST){
			$arr = D("User")->editAccount();
			$arr['status'] == 1 ? $this->success($arr['info'],$arr['url'],3) : $this->error($arr['info']);
		}else{
			$info = M('UserAccount')->where(array('id'=>(int)$_GET['id']))->find();
			if($info['is_paid'] <> 0){
				$this->error("这个申请已经审核或取消不可以编辑",U('User/account'));
			}
			$info['paymentArr'] = M('payment')->select();
			$this->assign("info", $info);
			$this->display('addAccount');
		}
	}
	
	public function getUserExcel() {
		$M = M("User");
		$_GET['key'] <> '' ? $map['username'] = array('like','%'.$_GET['key'].'%') : '';
		$_GET['phone'] <> false ? $map['phone'] = array('like','%'.$_GET['phone'].'%') : '';
		$_GET['ks_id'] <> '' ? $map['id'] = array('egt',$_GET['ks_id']) : '';
		$_GET['js_id'] <> '' ? $map['id'] = array('elt',$_GET['js_id']) : '';
		$list = $M->where($map)->order("id DESC")->select();
		$obj = new \NewsLib\myPHPExcel();
		$xlsName = "用户";
		$xlsCell = array(
				array('id','数据库ID'),
				array('username','用户名'),
				array('nickname','昵称'),
				array('user_money','用户余额'),
				array('frozen_money','冻结金额'),
				array('score_l','累计积分'),
				array('score','积分'),
				array('regdate','注册时间'),
				array('email','E-mail'),
				array('phone','手机'),
				array('name','真实姓名'),
				array('address','住址'),
				array('sex','性别'),
				array('referrer_u','推荐用户'),
				array('is_subscribe','是否关注'),
		);
		foreach ($list as $key=>$val){
			if($val['referrer_u'] > 0){
				$referrer_u_arr = $M->field("username,nickname")->where(array('id'=>$val['referrer_u']))->find();
				$referrer_u = $referrer_u_arr['nickname']."[".$referrer_u_arr['username']."]";
			}else{
				$referrer_u = "自来用户";
			}
			$is_subscribe = $val['is_subscribe'] ? "是" : "否";
			$sex = $val['sex'] ? "男" : "女";
			$xlsData[$key]['id'] = $val['id'];
			$xlsData[$key]['username'] = $val['username'];
			$xlsData[$key]['nickname'] = $val['nickname'];
			$xlsData[$key]['user_money'] = $val['user_money'];
			$xlsData[$key]['frozen_money'] = $val['frozen_money'];
			$xlsData[$key]['score_l'] = $val['score_l'];
			$xlsData[$key]['score'] = $val['score'];
			$xlsData[$key]['regdate'] = date("Y-m-d H:i:s",$val['regdate']);
			$xlsData[$key]['email'] = $val['email'];
			$xlsData[$key]['phone'] = $val['phone'];
			$xlsData[$key]['name'] = $val['name'];
			$xlsData[$key]['address'] = $val['address'];
			$xlsData[$key]['sex'] = $sex;
			$xlsData[$key]['referrer_u'] = $referrer_u;
			$xlsData[$key]['is_subscribe'] = $is_subscribe;
		}
		//print_r($list);
		$obj->exportExcel($xlsName,$xlsCell,$xlsData);
	}


    private function getPid($where,$mid) {
        $list = D('City')->where($where)->select();
        $option.='<option value="0">请选择</option>';
        foreach ($list as $k => $v) {
            $selected = $v['var'] != $mid ? "" : ' selected="selected"';
            $option.='<option value="' . $v['var'] . '"' . $selected . '>' . $v['city'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }

    public function ajaxGetCity(){
        $where = array('mid'=>$_GET['id']);
        $data = $this->getPid($where);
        $this->ajaxReturn($data);
    }
}