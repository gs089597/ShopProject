<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class AdController extends CommonController {
    public function index(){
		$list = M('Ad')->select();
		$type = $this->config(); 
		foreach($list as $key => $val){
			$list[$key]['num'] = M('AdContent')->where(array('adpositionid'=>$val['id']))->count();
			foreach($type as $k => $v){
				if($val['type'] == $v['id']){
					$list[$key]['type'] = $v['name'];
				}
			}
		}
		$this->assign("list", $list);	
        $this->display();
    }
	public function addAdPositionId(){
		if (IS_POST) {
			$arr = D("AdContent")->addAdPositionId();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info['adOption'] = $this->getAdOption();
			$this->assign("info", $info);
            $this->display();
        }
    }
	public function editAdPositionId(){
		if (IS_POST) {
			$arr = D("AdContent")->editAdPositionId();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info = M('Ad')->where(array('id'=>(int)$_GET['id']))->find();
			$info['adOption'] = $this->getAdOption($info);
			$this->assign("info", $info);
            $this->display('addAdPositionId');
        }
    }
	//删除广告位
	public function delAdPositionId(){
		$M = M("Ad");
		$info = $M->where("`id`=".(int)$_GET['id'])->find();
		if($M->where("`id`=".(int)$_GET['id'])->delete()){
			setAdminLog('删除广告位-'.$info['name']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	//广告列表
	public function adList(){
		$list = D('AdContent')->where(array('adpositionid'=>(int)$_GET['wid']))->relation(true)->select();
		$M = M('AdStatistics');
		//print_r($list);
		foreach($list as $k=>$v){
			$list[$k]['shownum'] = $M->where(array('ad_content_id'=>$v['id']))->sum('shownum');
			$list[$k]['click'] = $M->where(array('ad_content_id'=>$v['id']))->sum('click');
		}
		$this->assign("list", $list);	
        $this->display();
	}
	//添加广告
	public function addAd(){
		if (IS_POST) {
			$arr = D("AdContent")->addAd();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$arr = M('Ad')->where(array('id'=>(int)$_GET['wid']))->find();
			$info['AdPositionId'] = $arr;
			foreach($this->config() as $k=>$v){
				if($v['id'] == $arr['type']){
					$info['AdPositionIdType'] = $v;	
				}
			}
			$this->assign("AdPositionId", $info);
            $this->display();
        }
	}
	//编辑广告
	public function editAd(){
		if (IS_POST) {
			$arr = D("AdContent")->editAd();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info = D('AdContent')->where(array('adpositionid'=>(int)$_GET['wid'],'id'=>(int)$_GET['id']),"OR")->find();
			$arr = M('Ad')->where(array('id'=>(int)$_GET['wid']))->find();
			$array['AdPositionId'] = $arr;
			foreach($this->config() as $k=>$v){
				if($v['id'] == $arr['type']){
					$array['AdPositionIdType'] = $v;	
				}
			}
			$this->assign("info", $info);
			$this->assign("AdPositionId", $array);
            $this->display('addAd');
        }
	}
	//删除广告
	public function delAd(){
		$M = M("AdContent");
		$info = $M->where("`id`=".(int)$_GET['id'])->find();
		if($M->where("`id`=".(int)$_GET['id'])->delete()){
			setAdminLog('删除广告-'.$info['name']);
			$this->success("删除成功");
		}else{
			$this->error("删除失败");	
		}
	}
	//状态改变
	public function AdStatus(){
		$M = M("AdContent");
		if($M->where("`id`=".(int)$_GET['id'])->save(array('status'=>$_GET['status']))){
			$this->success("操作成功");
		}else{
			$this->error("操作失败");	
		}
	}
	public function invoke(){
		$this->display();	
	}
	private function getAdOption($info){
        $list = $this->config();              //获取分类结构
        $option = '<option value="0">请选择类型</option>';
        foreach ($list as $k => $v) {
            $selected = $v['id'] != $info['type'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['name'] . '</option>';
        }
        return $option;
	}
	//广告媒介、版位类型
	private function config(){
		return array(
			array(
				'id' => 1,
				'name' => '矩形横幅'
			),
			array(
				'id' => 2,
				'name' => '图片列表'
			),
			array(
				'id' => 3,
				'name' => '滚动图片'
			),
			array(
				'id' => 4,
				'name' => '文字'
			),
			array(
				'id' => 5,
				'name' => '返回数据数组'
			),
		);	
	}
}