<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class ActivityController extends CommonController {
    public function popularize() {
		if(IS_POST){
			$scale = array();
			for($key=0;$key<20;$key++){
				if($_POST['scale_a_'.$key]){
					$scale[$_POST['scale_rank_'.$key]] = array(
                        "scale_a" => $_POST['scale_a_'.$key]
                    );
				}else{
					continue;
				}
			}	
			ksort($scale);
			$_POST['data']['scale'] = $scale;		
			$arr = D("System")->addData();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
		}else{
			$array = M('System')->where(array('name'=>'popularize'))->find();
			$info = json_decode($array['data'],true);	
			//print_r($info);		
			$this->assign("scale", getPopuRankTpl($info['scale']));
			$this->assign("info", $info);
			$this->display();
		}
    }
	
	public function referrerLog(){
		$M = M("ReferrerLog");
		$_GET['status'] <> 10 && $_GET['status'] <> '' ? $map['status'] = $_GET['status'] : '';
		$_GET['startime'] <> '' && $_GET['endtime'] == ''? $map['time'] = array("egt",strtotime($_GET['startime'])) : '';
		$_GET['endtime'] <> '' && $_GET['startime'] == ''? $map['time'] = array("elt",strtotime($_GET['endtime'])) : '';
		$_GET['startime'] <> '' && $_GET['endtime'] <> '' ? $map['time'] = array(array("egt",strtotime($_GET['startime'])),array("elt",strtotime($_GET['endtime']))) : "";
		
		$count = $M->where($map)->count();
		$pagesize = 20;
		$Page = new  \NewsLib\adminPage($count, $pagesize);
		$show       = $Page->show();
		$this->assign('page',$show );
		$list = $M->limit($Page->firstRow.','.$Page->listRows)->where($map)->order("id DESC")->select();
		
		foreach ($list as $key=>$val){
			$list[$key]['Order'] = M("Orders")->where(array('id'=>$val['oid']))->find();
			$list[$key]['User'] = M("User")->where(array('id'=>$val['uid']))->find();
			$list[$key]['Goods'] = M("Goods")->where(array('id'=>$val['order_goods_id']))->find();
		}
		$this->assign("list", $list);
		$this->display();
	}
}