<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class WapInfoController extends CommonController {
	public function index() {
		$arr = D("WapPage")->select();
		$this->assign("list", $arr);
        $this->display();
    }
	public function addPage(){
		if (IS_POST) {
			$arr = D("WapPage")->addPage();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
            $this->display();
        }
	}
	public function editPage(){
		if (IS_POST) {
			$arr = D("WapPage")->editPage();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info = D('WapPage')->where(array('id'=>(int)$_GET['id']))->find();
			$this->assign("info", $info);
            $this->display('addPage');
        }
	}
	public function delPage(){
		$info = $Goods = D('WapPage')->where(array('id'=>$_GET['id']))->find();
		if(D("WapPage")->where(array('id'=>(int)$_GET['id']))->delete()){
			setAdminLog('删除自定义-'.$info['title']);
			$this->success("操作成功");
		}else{
			$this->error("操作失败！！");
		}
	}
	
	
	public function Act() {
		$arr = D("WapActPage")->select();
		$this->assign("list", $arr);
        $this->display();
    }
	public function addAct(){
		if (IS_POST) {
			$arr = D("WapPage")->addActPage();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info['category'] = $this->getPid();
			$info['tplarr'] = $this->tplarr();
			$this->assign("info", $info);
            $this->display();
        }
	}
	public function editAct(){
		if (IS_POST) {
			$arr = D("WapPage")->editActPage();
			$arr['status'] == 1 ? $this->success($arr['info']) : $this->error($arr['info']);
        } else {
			$info = M('WapActPage')->where(array('id'=>(int)$_GET['id']))->find();
			$arr['pid'] = $info['category'];
			$info['category'] = $this->getPid($arr);
			$info['tplarr'] = $this->tplarr();
			$this->assign("info", $info);
            $this->display('addAct');
        }
	}
	public function delAct(){
		$info = $Goods = M('WapActPage')->where(array('id'=>$_GET['id']))->find();
		if(M("WapActPage")->where(array('id'=>(int)$_GET['id']))->delete()){
			setAdminLog('删除频道页-'.$info['title']);
			$this->success("操作成功");
		}else{
			$this->error("操作失败！！");
		}
	}
	
	
	//获取分类
	private function getPid($info) {
        $cat = new \Org\Util\Category('GoodsCategory', array('id', 'pid', 'name', 'fullname'));
        $list = $cat->getList();               //获取分类结构
        foreach ($list as $k => $v) {
            $selected = $v['id'] != $info['pid'] ? "" : ' selected="selected"';
            $option.='<option value="' . $v['id'] . '"' . $selected . '>' . $v['fullname'] . '</option>';
        }
        $info['pidOption'] = $option;
        return $info;
    }
	private function tplarr(){
		return array(
			'style_1'
		);	
	}
}