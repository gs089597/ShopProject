<?php

namespace NewsLib;
class SmsAliyun
{
    private $http;        //短信接口地址
    private $accessKeyId;        //短信接口
    private $accessKeySecret;        //用户账号
    private $SignName;         //签名
    private $security = 0;         //签名

    public function __construct($mobile,$content,$config, $tpl_id='', $mobileids, $time='',$mid='')
    {
        $this->accessKeyId = $config['aliyunaccesskeyid'];
        $this->accessKeySecret = $config['aliyunaccesskeysecret'];
        $this->SignName = $config['aliyun_qm'];
        $this->mobile = $mobile;
        $this->tpl_id = $tpl_id;
        $this->tpl_value = $content;
        $this->http = ($this->security ? 'https' : 'http') . "://dysmsapi.aliyuncs.com/";
        $this->sendSMS();
    }

    public function sendSMS()
    {
        $data = array
        (
            'PhoneNumbers' => $this->mobile,
            'TemplateCode' => $this->tpl_id,
            'TemplateParam' => $this->tpl_value
        );
        $re = $this->postSMS($data);
        $this->ret = $this->getStatus($re->Code);
        return $this->ret;
    }

    /**
     * 发送短信
     */
    public function postSMS($params = array())
    {
        // *** 需用户填写部分 ***
        $params["SignName"] = $this->SignName;
        // fixme 可选: 设置发送短信流水号
        //$params['OutId'] = "12345";

        // fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
        //$params['SmsUpExtendCode'] = "1234567";


        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
        if (!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
            $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
        }

        // 此处可能会抛出异常，注意catch
        $content = $this->request(
            array_merge($params, array(
                "RegionId" => "cn-hangzhou",
                "Action" => "SendSms",
                "Version" => "2017-05-25",
            ))
        // fixme 选填: 启用https
        // ,true
        );

        return $content;
    }

    /**
     * 生成签名并发起请求
     *
     * @param $accessKeyId string AccessKeyId (https://ak-console.aliyun.com/)
     * @param $accessKeySecret string AccessKeySecret
     * @param $domain string API接口所在域名
     * @param $params array API具体参数
     * @param $security boolean 使用https
     * @return bool|\stdClass 返回API接口调用结果，当发生错误时返回false
     */
    public function request($params)
    {
        $apiParams = array_merge(array(
            "SignatureMethod" => "HMAC-SHA1",
            "SignatureNonce" => uniqid(mt_rand(0, 0xffff), true),
            "SignatureVersion" => "1.0",
            "AccessKeyId" => $this->accessKeyId,
            "Timestamp" => gmdate("Y-m-d\TH:i:s\Z"),
            "Format" => "JSON",
        ), $params);
        ksort($apiParams);

        $sortedQueryStringTmp = "";
        foreach ($apiParams as $key => $value) {
            $sortedQueryStringTmp .= "&" . $this->encode($key) . "=" . $this->encode($value);
        }

        $stringToSign = "GET&%2F&" . $this->encode(substr($sortedQueryStringTmp, 1));

        $sign = base64_encode(hash_hmac("sha1", $stringToSign, $this->accessKeySecret . "&", true));

        $signature = $this->encode($sign);

        $url = $this->http . "?Signature={$signature}{$sortedQueryStringTmp}";

        try {
            $content = $this->fetchContent($url);
            return json_decode($content);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function encode($str)
    {
        $res = urlencode($str);
        $res = preg_replace("/\+/", "%20", $res);
        $res = preg_replace("/\*/", "%2A", $res);
        $res = preg_replace("/%7E/", "~", $res);
        return $res;
    }

    private function fetchContent($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "x-sdk-client" => "php/2.0.0"
        ));

        if (substr($url, 0, 5) == 'https') {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        $rtn = curl_exec($ch);

        if ($rtn === false) {
            trigger_error("[CURL_" . curl_errno($ch) . "]: " . curl_error($ch), E_USER_ERROR);
        }
        curl_close($ch);

        return $rtn;
    }


    public function getStatus($a)
    {
        $arr = [
            "0" => "发送成功",
            "OK" => "发送成功",
            "isp.RAM_PERMISSION_DENY" => "RAM权限DENY",
            "isv.OUT_OF_SERVICE" => "业务停机",
            "isv.PRODUCT_UN_SUBSCRIPT" => "未开通云通信产品的阿里云客户",
            "isv.PRODUCT_UNSUBSCRIBE" => "产品未开通",
            "isv.ACCOUNT_NOT_EXISTS" => "账户不存在",
            "isv.ACCOUNT_ABNORMAL" => "账户异常",
            "isv.SMS_TEMPLATE_ILLEGAL" => "短信模板不合法",
            "isv.SMS_SIGNATURE_ILLEGAL" => "短信签名不合法",
            "isv.INVALID_PARAMETERS" => "参数异常",
            "isp.SYSTEM_ERROR" => "系统错误",
            "isv.MOBILE_NUMBER_ILLEGAL" => "非法手机号",
            "isv.MOBILE_COUNT_OVER_LIMIT" => "手机号码数量超过限制",
            "isv.TEMPLATE_MISSING_PARAMETERS" => "模板缺少变量",
            "isv.BUSINESS_LIMIT_CONTROL" => "业务限流",
            "isv.INVALID_JSON_PARAM" => "JSON参数不合法，只接受字符串值",
            "isv.BLACK_KEY_CONTROL_LIMIT" => "黑名单管控",
            "isv.PARAM_LENGTH_LIMIT" => "参数超出长度限制",
            "isv.PARAM_NOT_SUPPORT_URL" => "不支持URL",
            "isv.AMOUNT_NOT_ENOUGH" => "账户余额不足",
        ];
        if($a == 'OK') $a = 0;
        return ["code" => $a, "msg" => $arr[$a]];
    }
}