<?php
//本系统支付类
namespace NewsLib;
class bPay {
	var $config;
	
	function __construct($config){
		$this->config = $config;
	}
    function AlipaySubmit($config) {
    	$this->__construct($config);
    }
	/**
	 * 	作用：格式化参数，签名过程需要使用
	 */
	function formatBizQueryParaMap($paraMap, $urlencode)
	{
		$buff = "";
		ksort($paraMap);
		foreach ($paraMap as $k => $v)
		{
		    if($urlencode)
		    {
			   $v = urlencode($v);
			}
			$buff .= $k . "=" . $v . "&";
		}
		$reqPar;
		if (strlen($buff) > 0) 
		{
			$reqPar = substr($buff, 0, strlen($buff)-1);
		}
		return $reqPar;
	}
	/**
	 * 	作用：生成签名
	 */
	public function getSign($Obj)
	{
		foreach ($Obj as $k => $v)
		{
			$Parameters[$k] = $v;
		}
		//签名步骤一：按字典序排序参数
		ksort($Parameters);
		$String = $this->formatBizQueryParaMap($Parameters, false);
		//echo '【string1】'.$String.'</br>';
		//签名步骤二：在string后加入KEY
		$String = $String."&key=".$this->config['KEY'];
		//echo "【string2】".$String."</br>";
		//签名步骤三：MD5加密
		$String = md5($String);
		//echo "【string3】 ".$String."</br>";
		//签名步骤四：所有字符转为大写
		$result_ = strtoupper($String);
		//echo "【result】 ".$result_."</br>";
		return $result_;
	}
	function buildRequestForm($para_temp, $method, $button_name) {
		$sHtml = "<form class='uk-form' action='".$this->config['URL']."' method='".$method."'>";
		while (list ($key, $val) = each ($para_temp)) {
            $sHtml.= "<input type='hidden' name='".$key."' value='".$val."'/>";
        }
		$sign = $this->getSign($para_temp);
		$sHtml.= "<input type='hidden' name='sign' value='".$sign."'/>";
		//$sHtml.= '<div class="uk-form-row"><input type="password" placeholder="请输入支付密码" name="password_pay" class="uk-width-1-1" value="" /></div>';
        $sHtml .= "<div class='uk-form-row'><input type='submit' class='uk-button-danger uk-button uk-width-1-1' value='".$button_name."'/></div></form>";		
		return $sHtml;
	}
	
}
?>