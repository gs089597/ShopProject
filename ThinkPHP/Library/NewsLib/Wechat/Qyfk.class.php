<?php
/**
 *********************************************************
 * @author : Ethan
 * @contact : touch_789@163.com
 * @time ：2015-3-10 13:26
 * @copyright : (c) 2015 Ethan All rights reserved.
 *********************************************************
 * @name : 
 * @function : 
 *
 */
//include_once './UnifiedOrder.class.php';
namespace NewsLib\Wechat;
class Qyfk extends UnifiedOrder
{	
	function __construct($config){
		$this->conf = $config;
		//设置接口链接
		$this->url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
		//设置curl超时时间
		$this->curl_timeout = $this->conf['CURL_TIMEOUT'];
	}
   	function QfykPay($arr){
   		$arr['mch_appid'] = $this->conf['APPID'];
   		$arr['mchid'] = $this->conf['MCHID'];;
   		$arr['nonce_str'] = strtoupper($this->createNoncestr(30));
   		$arr['spbill_create_ip'] = $_SERVER["REMOTE_ADDR"];
   		$sign = $this->getSign($arr);
   		$arr['sign'] = $sign;
   		$Xml = $this->arrayToXml($arr);
   		$return = $this->postXmlSSLCurl($Xml, $this->url);
   		$return = $this->xmlToArray($return);
   		return $return;
   	}
	
}


?>