<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace NewsLib;
class myPHPExcel {
	
	public function exportExcel($expTitle,$expCellName,$expTableData){
        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
        $fileName = $expTitle.date('_YmdHis');//or $xlsTitle 文件名称可根据自己情况设定
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);
		Vendor('PHPExcel.PHPExcel'); 
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("ctos")
			        ->setLastModifiedBy("ctos")
			        ->setTitle("Office 2007 XLSX Test Document")
			        ->setSubject("Office 2007 XLSX Test Document")
			        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			        ->setKeywords("office 2007 openxml php")
			        ->setCategory("Test result file");
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        
        $objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');//合并单元格
       // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle.'  Export time:'.date('Y-m-d H:i:s'));  
        for($i=0;$i<$cellNum;$i++){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'2', $expCellName[$i][1]); 
        } 
        for($i=0;$i<$dataNum;$i++){
          for($j=0;$j<$cellNum;$j++){
            $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+3), $expTableData[$i][$expCellName[$j][0]]);
          }             
        }          
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$xlsTitle.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
        $objWriter->save('php://output'); 
        exit;   
    }
 	
    
    function formatExcelArray($filePath='',$sheet=0){
    	Vendor('PHPExcel.PHPExcel');
    	Vendor('PHPExcel.PHPExcel.IOFactory');
    	if(empty($filePath) or !file_exists($filePath)){die('file not exists');}
    	$PHPReader = new \PHPExcel_Reader_Excel2007();        //建立reader对象
    	if(!$PHPReader->canRead($filePath)){
    		$PHPReader = new \PHPExcel_Reader_Excel5();
    		if(!$PHPReader->canRead($filePath)){
    			echo 'no Excel';
    			return ;
    		}
    	}
    	$PHPExcel = $PHPReader->load($filePath);        //建立excel对象
    	$currentSheet = $PHPExcel->getSheet($sheet);        //**读取excel文件中的指定工作表*/
    	$allColumn = $currentSheet->getHighestColumn();        //**取得最大的列号*/
    	$allRow = $currentSheet->getHighestRow();        //**取得一共有多少行*/
    	$data = array();
    	for($rowIndex=1;$rowIndex<=$allRow;$rowIndex++){        //循环读取每个单元格的内容。注意行从1开始，列从A开始
    		for($colIndex='A';$colIndex<=$allColumn;$colIndex++){
    			$addr = $colIndex.$rowIndex;
    			$cell = $currentSheet->getCell($addr)->getValue();
    			if($cell instanceof PHPExcel_RichText){ //富文本转换字符串
    				$cell = $cell->__toString();
    			}
    			$data[$rowIndex][$colIndex] = $cell;
    		}
    	}
    	return $data;
    }
}

?>