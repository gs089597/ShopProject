<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
namespace NewsLib;
class myQRCode {

    private $path;
    private $size;

    public function __construct($path, $size) {
        $this->path = empty($path) ? __ROOT__ . "/Uploads/QRCode/" : $path;
        $this->size = empty($size) ? 10 : $size;
    }
    /**
      +----------------------------------------------------------
     * 检测存储目录是否存在，不存在则创建该目录
      +----------------------------------------------------------
     */
    private function makeDir($path) {
        return is_dir($path) or ($this->makeDir(dirname($path)) and @mkdir($path, 0777));
    }

    /**
      +----------------------------------------------------------
     * 取得二维码地址
      +----------------------------------------------------------
     */
    public function getUrl($file = '',$url = "http://www.jiu97.com") {
		import('Vendor.phpqrcode.phpqrcode');
		$level = 'L';
		$savePath = $_SERVER['DOCUMENT_ROOT'] . $this->path;
        $this->makeDir($savePath);
        if($file){
            $imgPath = $savePath.$file . ".png";
            $fileName = $file . ".png";
        }else{
            $imgPath = $savePath.substr(md5("$url"), 8, 16) . "_" . $this->size . ".png";
            $fileName = substr(md5("$url"), 8, 16) . "_" . $this->size . ".png";
        }

		$img = new \QRcode();
		$inPath = $img->png($url, $imgPath, $level,$this->size,2);
		$savePath.=$fileName;
        $outUrl = "http://" . $_SERVER['HTTP_HOST'] . $this->path . $fileName;
        if (file_exists($savePath) && filesize($savePath) > 0) {
            return $outUrl;
        }
		return false;
    }
}

?>