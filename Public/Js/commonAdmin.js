// JavaScript Document
function popup(content,icon){
	art.dialog({
		lock: true,
		content: content,
		cancel: true
	});
}
function popdel(e,t){
	if(t == null){
		c = '您确定要删除此内容';
	}else{
		c = t;	
	}
	art.dialog({
		lock: true,
		title: '提示',
		content: c,
		ok: function () {
			window.location.href = e;
			return true;
		},
		cancelVal: '取消',
		cancel: true
	});
}
function popcoo(e,t){
    if(t == null){
        c = '您确定要就行此操作吗？';
    }else{
        c = t;
    }
    art.dialog({
        lock: true,
        title: '提示',
        content: c,
        ok: function () {
            window.location.href = e;
            return true;
        },
        cancelVal: '取消',
        cancel: true
    });
}
$(function(){
	var chn=function(cid,op){
		if(op=="show"){
			$("tr[pid='"+cid+"']").each(function(){
				$(this).removeAttr("status").show();
				chn($(this).attr("id"),"show");
			});
		}else{
			$("tr[pid='"+cid+"']").each(function(){
				$(this).attr("status",1).hide();
				chn($(this).attr("id"),"hide");
			});
		}
	}
	$(".tree").click(function(){
		if($(this).attr("status")!=1){
			chn($(this).parent().attr("id"),"hide");
			$(this).attr("status",1);
		}else{
			chn($(this).parent().attr("id"),"show");
			$(this).removeAttr("status");
		}
	});
});