$(function(){
	$(".category li a").click(function(){
		if($(this).closest('li').find(".uk-panel-box").is(".uk-hidden")){
			$(this).removeClass("open");
		}else{
			$(this).addClass("open")
		};
	})
    $(".add_fans").click(function(){
        var _this = $(this)
        var id = $(this).attr("data-id");
        commonAjaxSubmit($(this).attr("data-url"), "#user_id_"+id, 1000, function(data){
            if(data.status == 0) return false;
            if(data.type == "1"){
                _this.html("已关注")
            }
            if(data.type == "2"){
                _this.html("+ 关注")
            }
            $(_this).toggleClass("uk-button-success")
            $(_this).toggleClass("uk-button-danger")
            _this.closest(".goods").find(".fans_box").html(data.fans_num + "<br/>粉丝")
        });
    })

    $(".add_zan").click(function(){
        var _this = $(this)
        var id = $(this).attr("data-id");
        console.log(id)
        commonAjaxSubmit($(this).attr("data-url"), ".goods_id_"+id, 1000, function(data){
            console.log(data)
            if(data.status == "1"){
                _this.find("em").html(data.praise_num)
                $(".praise_box").html(data.user_praise_num + "<br/>赞")
            }
        });
    })
})
art.artDialog = function(e){
	art.dialog({
		time : 3,
		content:e,
		width: '75%',
		drag : false,
		//lock : true,		
	});	
}
/**
 * 通用AJAX提交
 * @param  {string} url    表单提交地址
 * @param  {string} formObj    待提交的表单对象或ID
 */
function commonAjaxSubmit(url,formObj,time, callback){
	var ti = time ? time : 1000;
    if(!formObj||formObj==''){
        var formObj="form";
    }
    if(!url||url==''){
        var url=document.URL;
    }
	/*
	art.dialog({
		id : "A1",
		title: '温馨提示',
		content:"服务器正在处理",
		drag : false,
		lock : true,
	});
	*/
    $(formObj).ajaxSubmit({
        url:url,
        type:"POST",
        success:function(data, st) {
			//art.dialog({id: 'A1'}).close()
			art.artDialog(data.info)
            if(data.url&&data.url!=''){
                setTimeout(function(){
                    top.window.location.href=data.url;
                },ti);
            }
            if(data.url==''){
                setTimeout(function(){
                    top.window.location.reload();
                },ti);
            }
            callback(data)
        }
    });
    return false;
}
/**
 * 检测字符串是否是电子邮件地址格式
 * @param  {string} value    待检测字符串
 */
function isEmail(value){
    var Reg =/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    return Reg.test(value);
}
function isPhone(value){
    var Reg =/^[0][1-9]{2,3}-[0-9]{5,10}$/;
	var Reg2 = /^[1][3][0-9]{9}$/;
	if(Reg.test(value)){
		return true;
	}else{
		if(Reg2.test(value)){
			return true;	
		}else{
			return false;
		}
	}
}
function pageBack(){
	history.go(-1);
}



function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
}

function lrzfun(_uploadFile) {
    return new Promise(function(resolve, reject){
        lrz(_uploadFile,{width: 1280, quality: 0.5})
            .then(function (rst, r) {
                file = dataURLtoFile(rst.base64, _uploadFile.name)
                file.exifdata = _uploadFile.exifdata
                file.iptcdata = _uploadFile.iptcdata
                resolve(file)
            })
            .catch(function (err) {
                console.log(err);
            })
            .always(function () {
                // 不管是成功失败，都会执行
            });
    })
}

/**
 * 异步上传图片,加图片压缩
 */
(function($) {

    $.fn.ajaxUploadImage = function(options) {
        var defaults = {
            url : '',
            data : {},
            start : function(){},     // 开始上传触发事件
            success : function(){}
        }
        var options = $.extend({}, defaults, options);
        var _uploadFile;
        function _checkFile() {
            //文件为空判断
            if (_uploadFile === null || _uploadFile === undefined) {
                alert("请选择您要上传的文件！");
                return false;
            }
            return true;
        };

        return this.each(function() {
            $(this).on('change', function(){
                var _element = $(this);
                _uploadFile = _element.prop('files')[0];
                if (!_checkFile()) return false;
                options.start.call('start', _element);
                try {
                    lrzfun(_uploadFile).then(function (resolve) {
                        _uploadFile = resolve
                        //执行上传操作
                        var xhr = new XMLHttpRequest();
                        xhr.open("post",options.url, true);
                        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState == 4) {
                                returnDate = $.parseJSON(xhr.responseText);
                                options.success.call('success', _element, returnDate);
                            };
                        };
                        //表单数据
                        var fd = new FormData();
                        for (k in options.data) {
                            fd.append(k, options.data[k]);
                        }
                        fd.append(_element.attr('name'), _uploadFile);
                        //执行发送
                        result = xhr.send(fd);
                    });
                } catch (e) {
                    console.log(e);
                    alert(e);
                }
            });
        });
    }

})(jQuery);
