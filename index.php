<?php
// +----------------------------------------------------------------------
// | PHP爱好者
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2025 All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

// 应用入口文件

// 检测PHP环境

if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',true);
//公共模块
//define('COMMON_PATH','./Common/');
// 定义应用目录
define('APP_PATH','./Application/');
define("WEB_ROOT", dirname(__FILE__) . "/");
define("DatabaseBackDir", WEB_ROOT . "Databases/"); //系统备份数据库文件存放目录
define('RUNTIME_PATH',   WEB_ROOT.'Runtime/');   // 系统运行时目录
define('WEB_CACHE_PATH', RUNTIME_PATH."Cache/");
define('HTML_PATH',      WEB_ROOT.'Html/'); // 应用静态目录
// 引入ThinkPHP入口文件
require './ThinkPHP/ThinkPHP.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单